#! /bin/bash

# /*
# *********************************************************************************
# *              start.sh : Robocup 3D Soccer Simulation Team Zigorat             *
# *                                                                               *
# *  Date: 01/24/2009                                                             *
# *  Author: Mahdi Hamdarsi                                                       *
# *  Comments: A not simple script to start number of agents on a remote host     *
# *                                                                               *
# *********************************************************************************
# */

# This script starts the Zigorat team. When player numbers are
# supplied along (optional) host-name and team-name arguments only
# these players are started, otherwise all players are started.
#
# Example: start.sh                            all players default host and name
# Example: start.sh -h machine                 all players on host 'machine'
# Example: start.sh -h localhost -t Zigorat    all players on localhost and name 'Zigorat'
# Example: start.sh -h 127.0.0.1 -t Zigorat    all players on 127.0.0.1  and name 'Zigorat'
# Example: start.sh 1 2 3 4                    players 1-4 on default host and name
# Example: start.sh 1 2 -h remote              players 1-2 on host 'remote's
# Example: start.sh 9 10 -h remote -t Zigorat  players 9-10 on host remote and name 'Zigorat'

wait=1
script_ver=2.0
host="localhost"
team="Zigorat"
dir="."
agent="${dir}/Player"
use_log="false"
log_levels="-l 1-100"
log_dir="${dir}/logs"
max_players=9

function greet()
{
  echo "*****************************************************************"
  echo "* Halcyon - Shariati technical college                          *"
  echo "*                                                               *"
  echo "* Created by:                                                   *"
  echo "*   Mahsa Bouriabaf                                             *"
  echo "*   Haideh Arabzadeh                                            *"
  echo "*   Elnaz Qasemi                                                *"
  echo "*   Mahboobeh Mohseni                                           *"
  echo "*   Pegah Motakefi                                              *"
  echo "*   Katayoun Niki Maleki                                        *"
  echo "*                                                               *"
  echo "* Research coordinator & Team Leader: Mahdi Hamdarsi            *"
  echo "*****************************************************************"
  echo "                                                                 "
  echo "*****************************************************************"
  echo "* Zigorat - Zigorat Robotic & AI reasearch group                *"
  echo "*                  <zigorat3d@gmail.com>                        *"
  echo "*                                                               *"
  echo "* Created by:                                                   *"
  echo "*   Amin  Mohammadi    <amin.mohammadi@gmail.com>               *"
  echo "*   Mahdi Hamdarsi     <hamdarsi@gmail.com>                     *"
  echo "*                                                               *"
  echo "* Research coordinator & Team Leader: Amin Mohammadi            *"
  echo "*****************************************************************"
}

function about()
{
  echo "Usage: start.sh [HOST] [TEAMNAME] [PLAYER NUMBERS TO RUN]               "
  echo "Start players for TEAMNAME on specified host address                    "
  echo "                                                                        "
  echo "Mandatory arguments to long options are mandatory for short options too."
  echo "  -h, --host HOST              remote address for the simulator         "
  echo "  -t, --team TEAMNAME          team name to assign to players           "
  echo "      --help     display this help and exit                             "
  echo "      --version  output version information and exit                    "
  echo "                                                                        "
  echo "Zigorat start script version ${script_ver} by Mahdi                     "
  echo "Report bugs to <hamdarsi@gmail.com>.                                    "
}

function read_options()
{
  while test "$1" != "" ; do
    case $1 in
      --help)
        about
        exit 0
      ;;

      --team|-t)
        shift
        team=$1
      ;;

      --host|-h)
        shift
        host=$1
      ;;

      --version|-v)
        echo "start.sh version ${version} - Mahdi"
        exit 0
      ;;

      *)
        case $1 in
          1|2|3|4|5|6|7|8|9|10|11)
            players[$1]="enabled"
          ;;

          *)
            echo "Unknown option $1, try --help for help"
            exit 1
          ;;
        esac
     ;;
    esac

  shift
  done
}

function prepare_players()
{
  mkdir -p ${log_dir}

  cnt=0
  i=1
  while test ${i} -le ${max_players}; do
    if test "${players[i]}" == "enabled"; then
      cnt=$[cnt+1]
    fi

    i=$[i+1]
  done

  if test ${cnt} -eq 0; then
    i=1;

    while test ${i} -le ${max_players}; do
      players[i]=enabled
      i=$[i+1]
    done
  fi
}

function run_players()
{
  i=1
  cd ${dir}
  first_sleep=1

  while test ${i} -le ${max_players}; do
    if test "${players[i]}" == "enabled"; then
      if test "${use_log}" == "true"; then
        log_options="-o ${log_dir}/${team}_${i}.log ${log_levels}"
      else
        log_options=""
      fi

      if test ${first_sleep} -eq 0; then
        sleep ${wait}
      else
        first_sleep=0
      fi

      ${agent} --number ${i} --host ${host} --team ${team} --dir ${dir} ${log_options} &
    fi

    i=$[i+1]
  done
}

# Execution starts here
greet
read_options $@
prepare_players
run_players
echo "Finished starting players."
# and ends here
