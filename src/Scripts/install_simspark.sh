#! /bin/bash

# /*
# *********************************************************************************
# *            distclean.sh : Robocup 3D Soccer Simulation Team Zigorat           *
# *                                                                               *
# *  Date: 01/24/2009                                                             *
# *  Author: Mahdi Hamdarsi                                                       *
# *  Comments: A script to install simspark from svn source                       *
# *                                                                               *
# *********************************************************************************
# */

script_ver="0.6.1"
job="nothing"

function install_simulator()
{
  # Get the path to the extracted simulator tarball
  ROOT_DIR=$1
  if test -z ${ROOT_DIR}; then
	echo -n "Enter the path to the extracted simulator: "
	read ROOT_DIR
  fi

  # Spark installation
  cd "${ROOT_DIR}/spark/"
  mkdir -p "build"
  cd build
  if cmake ..; then
	if make; then
	  echo "Successfully built spark"
	else
	  echo "Build error in spark"
	  exit 1
	fi
  else
	echo "CMake error in spark"
	exit 1
  fi

  if make install; then
	echo "Successfully installed spark"
  else
	echo "Could not install spark - check permissions"
  fi


  # rcssserver3D installation
  cd "${ROOT_DIR}/rcssserver3d/"
  mkdir -p "build"
  cd build
  if cmake ..; then
	if make; then
	  echo "Successfully built rcssserver3D"
	else
	  echo "Build error in rcssserver3D"
	  exit 1
	fi
  else
	echo "CMake error in rcssserver3D"
	exit 1
  fi

  if make install; then
	echo "Successfully installed rcssserver3D"
  else
	echo "Could not install rcssserver3D - check permissions"
  fi

  cd "${ROOT_DIR}/simspark-utilities/"
  mkdir -p "build"
  cd build
  if cmake ..; then
	if make; then
	  echo "Successfully built rcssserver3D stuff"
	else
	  echo "Build error in rcssserver3D stuff"
	  exit 1
	fi
  else
	echo "CMake error in rcssserver3D stuff"
	exit 1
  fi

  if make install; then
	echo "Successfully installed rcssserver3D stuff"
  else
	echo "Could not install rcssserver3D stuff - check permissions"
  fi


  # rsgedit installation
  echo -n "Do you wish to install rsgedit? [yes/no - default yes] "
  read answer
  if test -z ${answer}; then
    answer="yes"
  fi
  if test ${answer} = "no"; then
	echo "SimSpark has been installed successfully"
	exit 0;
  fi

  # rsgedit installation
  cd "${ROOT_DIR}/rsgedit/"
  mkdir -p "build"
  cd build
  if cmake ..; then
	if make; then
	  echo "Successfully built rsgedit"
	else
	  echo "Build error in rsgedit"
	  exit 1
	fi
  else
	echo "CMake error in rsgedit"
	exit 1
  fi

  if make install; then
	echo "Successfully installed rsgedit"
  else
	echo "Could not install rsgedit - check permissions"
  fi


  # Finished
  echo "SimSpark installed successfully."
}

function uninstall_simulator()
{
  # Get the path to the extracted simulator tarball
  ROOT_DIR=$1
  if test -z ${ROOT_DIR}; then
	echo -n "Enter the path to the extracted simulator: "
	read ROOT_DIR
  fi

  # rsgedit
  if test -d "${ROOT_DIR}/rsgedit/build"; then
    cd ${ROOT_DIR}/rsgedit/build
    if make uninstall; then
	  echo "rsgedit successfully uninstalled"
	else
	  echo "Uninstallation error in rsgedit"
	fi
  else
    echo "rsgedit is not built, skipping"
  fi

  # rcssserver3d stuff
  if cd ${ROOT_DIR}/simspark-utilities/build; then
    if make uninstall; then
	  echo "rcssserver3D stuff successfully uninstalled"
	else
	  echo "Uninstallation error in rcssserver3D stuff - Check permissions"
	  exit 1
	fi
  else
    echo "rcssserver3D stuff build directory does not exist"
	exit 1
  fi

  # rcssserver3D
  if cd ${ROOT_DIR}/rcssserver3d/build; then
    if make uninstall; then
	  echo "rcssserver3D successfully uninstalled"
	else
	  echo "Uninstallation error in rcssserver3D - Check permissions"
	  exit 1
	fi
  else
    echo "rcssserver3D build directory does not exist"
	exit 1
  fi

  # Spark
  if cd ${ROOT_DIR}/spark/build; then
    if make uninstall; then
	  echo "Spark successfully uninstalled"
	else
	  echo "Uninstallation error in spark - Check permissions"
	  exit 1
	fi
  else
    echo "Spark build directory does not exist"
	exit 1
  fi

  echo "SimSpark successfully uninstalled"
}

function about()
{
  echo "                                                                        "
  echo "Usage: simspark_insaller [install|uninstall] [path]                     "
  echo "install or uninstall a simspark simulator                               "
  echo "                                                                        "
  echo "Mandatory arguments to long options are mandatory for short options too."
  echo "  -i, --inistall, install [PATH]     install the simspark simulator     "
  echo "  -u, --uninstall, uninstall [PATH]  uninstall the simspark simulator   "
  echo "      --help                         display this help and exit         "
  echo "      --version                      output version information and exit"
  echo "                                                                        "
  echo "Zigorat simspark installer script version ${script_ver}                 "
  echo "Report bugs to <hamdarsi@gmail.com>                                     "
  echo "                                                                        "
}

function read_options()
{
  while test "$1" != "" ; do
    case $1 in
      --help)
        about
        exit 0
      ;;

      --install|-i|install)
        job="install"
      ;;

      --uninstall|-u|uninstall)
        job="uninstall"
      ;;

      --version|-v)
        echo "simspark-installer.sh version ${version}"
        exit 0
      ;;

      *)
        ROOT_DIR=$1
     ;;
    esac

    shift
  done
}

read_options $@

if test ${job} = "install"; then
  install_simulator ${ROOT_DIR}
elif test ${job} = "uninstall"; then
  uninstall_simulator ${ROOT_DIR}
else
  about
fi

# END
