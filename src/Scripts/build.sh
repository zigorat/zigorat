#! /bin/bash

# /*
# *********************************************************************************
# *             build.sh : Robocup 3D Soccer Simulation Team Zigorat              *
# *                                                                               *
# *  Date: 01/24/2009                                                             *
# *  Author: Mahdi Hamdarsi                                                       *
# *  Comments: Simple script to regenerate build files                            *
# *                                                                               *
# *********************************************************************************
# */

project_dir="@project_dir@"

if [ -d "@project_dir@" ]; then
  cd "${project_dir}/build"
  make -j2

else # shall configure with cmake first

# Get the desired build environment
  if [ -z $1 ]; then
    BUILD_ENV="KDevelop3"
  else
    BUILD_ENV=$1
  fi

  cd ../../

  mkdir -p build
  cd build
  if cmake -G ${BUILD_ENV} ..; then
    make -j2
  else
    echo "CMake build failed"
  fi
fi

echo "Build finished."
echo -n "Press enter to continue..."
read x

# END
