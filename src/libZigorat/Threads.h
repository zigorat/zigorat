/*
*********************************************************************************
*             Threads.h : Robocup 3D Sodccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 10/12/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Thread utilization abstract mechanism                              *
*                                                                               *
*********************************************************************************
*/

/*! \file Threads.h
<pre>
<b>File:</b>          Threads.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       10/12/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Thread utilization abstract mechanism
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
10/12/2008       Mahdi           Initial version created
</pre>
*/

#ifndef THREADS
#define THREADS

#include "PlatformMacros.h"

#if PLATFORM == PLATFORM_WINDOWS
  #include <windows.h>
#else
  #include <pthread.h>
#endif


namespace Klaus
{

  /*! This class handles thread executions in windows and linux platforms. All that
      should be done to uitlize threads is to create a child class from this class
      and define it's own execute function. After a call to startThread(), a new
      thread will start and execute the execute() function.  */
  class _DLLExportControl Thread
  {
    protected:
      bool         mTerminated; /*!< If set to true, the thread should exit */

      #ifdef WIN32
      HANDLE       mThread;     /*!< Thread Handle */
      #else
      pthread_t    mThread;     /*!< Thread Handle */
      #endif

    public:
                   Thread       (                  );
      virtual    ~ Thread       (                  );

      void         terminate    (                  );
      void         startThread  (                  );

      /**
       * The thread function shall be written in this method.
       * This method will be called when the thread is started
       */
      virtual void execute      (                  ) = 0;
  };



  /*! This class protects resources from coincident write of multiple threads within a program */
  template<class DataType = bool>
  class _DLLExportControl Mutex
  {
    private:
      int              mLocked; /*!< Whether the mutex is locked or not */
      DataType         mData;   /*!< Data to protect with a mutex */

      #ifdef WIN32
      CRITICAL_SECTION mMutex;  /*!< The mutex instance */
      #else
      pthread_mutex_t  mMutex;  /*!< The mutex instance */
      #endif

    public:

      /**
       * Constructor of Mutex class. Initializes the mutex (or CriticalSection in Windowns)
       */
      Mutex()
      {
        #ifdef WIN32
          InitializeCriticalSection(&mMutex);
        #else
          pthread_mutex_init(&mMutex, NULL);
          mLocked = false;
        #endif
      }

      /**
       * Destructor of the Mutex. Destroys the Mutex in Windows (R) environment
       */
      ~ Mutex()
      {
        #ifdef WIN32
          DeleteCriticalSection( &mMutex );
        #else
          pthread_mutex_destroy( &mMutex );
        #endif
      }

      /**
       * This method locks the mutex hence disallows any changes to the stored data from
       * other threads than the one who locked it.
       * @return The protected data instance
       */
      DataType & lock()
      {
        #ifdef WIN32
          EnterCriticalSection(&mMutex);
        #else
          pthread_mutex_lock(&mMutex);
        #endif

        mLocked++;
        return mData;
      }

      /**
       * This method unlocks the mutex hence allows other threads
       * to lock the mutex and use it's data
       */
      void unlock()
      {
        #ifdef WIN32
          LeaveCriticalSection( &mMutex );
        #else
          pthread_mutex_unlock( &mMutex );
        #endif

        mLocked--;
      }

      /**
       * This method returns if the mutex is locked or not
       * @return True if the mutex is locked, false otherwise
       */
      bool isLocked()
      {
        return mLocked > 0;
      }
  };

};

#endif // THREADS
