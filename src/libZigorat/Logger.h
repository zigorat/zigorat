/*
Copyright (c) 2000-2003, Jelle Kok, University of Amsterdam
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the University of Amsterdam nor the names of its
contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*! \file Logger.h
<pre>
<b>File:</b>          Logger.h
<b>Project:</b>       Robocup Soccer Simulation Team: UvA Trilearn
<b>Authors:</b>       Jelle Kok
<b>Created:</b>       3/3/2000
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains the class to log information about the
               system to any output stream. A range can be specified
               for which the received log information is printed.
               Furthermore it is possible to print the time since the
               timer of the Logger has been restarted.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
3/3/2001         Jelle Kok       Initial version created
</pre>
*/

#ifndef _LOGGER_
#define _LOGGER_

#include "Singleton.h"  // needed for Klaus::Singleton
#include <iostream>     // needed for ostream (logging to output stream)
#include <fstream>      // needed for fstream (logging to file)
#include <string>       // needed for string
#include <set>          // needed for set
#include <deque>
#include "Threads.h"    // needed for mutex

#if PLATFORM == PLATFORM_WINDOWS
  #include <windows.h>  // needed for DWORD and GetTickCount() function
  #include <time.h>     // needed for time_t
#else
  #include <sys/time.h> // needed for timeval
#endif


#if COMPILER == COMPILER_MSVC
#  pragma warning (disable : 4251)
#endif


namespace Klaus
{

  /*! This class holds a timer. This timer can be set (restartTime) and
      text can be printed with the elapsed time since the timer was restarted.
  */
  class _DLLExportControl Timing
  {
    private:
      #ifdef WIN32
      DWORD  time1;         /*!< the number of milliseconds that have
                                 elapsed since the Windows was started */
      #else
      struct timeval time1; /*!< Time the timer has last been restarted.*/
      #endif

    public:

      // methods to restart the timer, get the elapsed time and print messages
      #ifdef WIN32
      static double getTimeDifference( DWORD tv1, DWORD tv2 );
      #else
      static double getTimeDifference( struct   timeval t1,
                                       struct   timeval t2  );
      #endif
      double        getElapsedTime   ( double   iFactor = 1 );
      void          restartTime      (                      );

      static double now              (                      );
  };



  /**
    This struct holds logged messages and their log levels.
  */
  struct LogCache
  {
    std::string mLog;   /*!< The logged message */
    int         mLevel; /*!< level of the logged message */

    /** Cache is a double queue of log messages. */
    typedef std::deque<LogCache> Cache;
  };

  /*! This class makes it possible to log information on different abstraction
      levels. All messages are passed to the log method 'log' with a level
      indication. When it has been specified that this level should be logged
      using either the 'addLogLevel' or 'addLogRange' method the message is
      logged, otherwise it is ignored. This makes it possible to print only
      the information you are interested in. The class is using singleton
      pattern to ease access by all other classes in the project. It can be
      accessed via Logger::getSingleton method. Furthermore the Logger also
      contains a timer with makes it possible to print the time since the
      timer has been restarted. */
  class _DLLExportControl Logger : public Klaus::Singleton<Logger>
  {
    public:

    private:
      #define cHeaderSize   128            /*!< maximum size of the header         */
      #define cLogSize    10240            /*!< maximum size of a log message      */
      #define cLogLines     100            /*!< maximum size of cached logs        */

      Timing          mTiming;             /*!< timer to print timing information  */
      char            mBuf[cLogSize];      /*!< buffer needed by different methods */
      std::set<int>   mLogLevels;          /*!< set that contains all log levels   */

      char            mHeader[cHeaderSize];/*!< header string printed before msg   */
      std::ostream  * mOS;                 /*!< output stream to print messages to */
      LogCache::Cache mCachedLogs;
      unsigned        mCacheSize;          /*!< Size of the cache                  */

      Mutex<>         mLogMutex;           /*!< Mutex to lock output streams
                                                for multithreaded logging          */


      void            addToCache        ( const std::string & str,
                                          int iLevel             );

                      // Constructor and destructor are private,
                      // for the singleton pattern
                      Logger            (                        );
                    ~ Logger            (                        );

    public:
      static Logger * getSingleton      (                        );
      static void     freeSingleton     (                        );

      // different methods associated with logging messages
      bool            log               ( int iLevel,
                                          const std::string &str );
      bool            log               ( int iLevel,
                                          const char   *str,
                                          ...                    );
      bool            logWithTime       ( int iLevel,
                                          double dAccuracy,
                                          const char   *str,
                                          ...                    );

      void            restartTimer      (                        );
      Timing          getTiming         (                        );
      bool            isInLogLevel      ( int         iLevel     );

      void            clearLogLevels    (                        );
      bool            addLogLevel       ( int         iLevel     );
      bool            addLogRange       ( int         iMin,
                                          int iMax               );

      const char*     getHeader         (                        );
      bool            setHeader         ( const char  *str       );
      bool            setHeader         ( double      d          );
      bool            setHeader         ( double      d,
                                          int         i          );

      const LogCache::Cache & getCache  (                        );


      bool            setOutputStream   ( std::ostream *   os    );
      std::ostream&   getOutputStream   (                        );
      void            showLogLevels     ( std::ostream &   os    );
      void            flush             (                        );
  };

}; // end namespace Klaus

#endif // Header guard _LOGGER_
