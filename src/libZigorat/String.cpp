/*
*********************************************************************************
*            String.cpp : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 01/22/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: String class and utilities                                         *
*                                                                               *
*********************************************************************************
*/

/*! \file String.cpp
<pre>
<b>File:</b>          String.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/22/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      String class and utilities
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/22/2011       Mahdi           Initial version created
</pre>
*/

#include "String.h"

#include <cstring> // needed for strlen


namespace Klaus
{


/****************************************************************************/
/*****************************  String Class  *******************************/
/****************************************************************************/


String::String()
{
  // start with default capacity of 20 charactors
  allocate(20);
  mSize = 0;
}

String::String(const char* str)
{
  for(mSize = 0; str[mSize]; mSize++)
    ;

  allocate(mSize + 20);

  for(size_t i = 0; str[i]; i++)
    mString[i] = str[i];
}

String::String(const wchar_t* str)
{
  for(mSize = 0; str[mSize]; mSize++)
    ;

  allocate(mSize + 20);

  for(size_t i = 0; str[i]; i++)
    mString[i] = str[i];
}

String::String(const std::string& str)
{
  mSize = str.length();
  allocate(mSize + 20);

  for(size_t i = 0; i < mSize; i++)
    mString[i] = str[i];
}

String::String(const std::wstring& str)
{
  mSize = str.length();
  allocate(mSize + 20);

  for(size_t i = 0; i < mSize; i++)
    mString[i] = str[i];
}

String::~String()
{
  if(mString)
    delete [] mString;
}

void String::allocate(size_t new_size)
{
  mCapacity = new_size;
  mString = new wchar_t[mCapacity];
}

void String::grow(size_t new_size)
{
  if(mCapacity >= new_size)
    return; // no action needed

  // TODO: fix string grow
  wchar_t * temp = mString;
  mCapacity = new_size + 20;
  mString = new wchar_t[mCapacity];
  memcpy(mString, temp, sizeof(wchar_t) * mSize);
  delete [] temp;
}

std::string String::toStdString() const
{
  std::string str(mSize, 0);
  return str;
}

std::wstring String::toStdWString() const
{
  return std::wstring(mString);
}



}; // end namespace Klaus
