/*
*********************************************************************************
*           FileSystem.cpp : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 03/03/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: A handy class for interacting with filesystem                      *
*                                                                               *
*********************************************************************************
*/

/*! \file FileSystem.cpp
<pre>
<b>File:</b>          FileSystem.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/03/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      A handy class for interacting with filesystem
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/03/2011       Mahdi           Initial version created
</pre>
*/


#include "FileSystem.h"
#include <cstring>       // needed for strncpy
#include <cstdlib>       // needed for free
#include <sys/stat.h>    // needed for stat
#ifdef WIN32
  #include <io.h>
#else
  #include <dirent.h>    // needed for alphasort
#endif


namespace Klaus
{


/**
  This method returns the contents of the provided folder
  @param strFolder Folder to get it's contents
  @param contents List to write the contents to
  @return Number of items in the folder
*/
int FileSystem::getFolderContents( const std::string & strFolder, StringArray & contents )
{
  contents.clear();

#ifdef WIN32

  struct _finddata_t file;
  long hFile;

  // Find first file in current directory
  if( (hFile = _findfirst( (strFolder+"\\*").c_str(), &file )) == -1L )
    return 0;

  int number = 0;
  do
  {
    //skip if find . and ..
    if((strcmp(file.name, ".") == 0 ||  strcmp(file.name, "..") == 0))
      continue;

    contents.push_back(file.name);
    number++;
  } while(_findnext( hFile, &file ) == 0);

  _findclose( hFile );

  return number;

#else

  struct dirent **namelist;

  int num = scandir(strFolder.c_str(), &namelist, 0, alphasort), number;
  number = num;

  if(num >= 0)
  {
    while(num--)
    {
      //skip if find . and ..
      if((strcmp(namelist[num]->d_name, ".") == 0 ||  strcmp(namelist[num]->d_name, "..") == 0))
        continue;

      contents.push_back( namelist[num]->d_name );
      free(namelist[num]);
    }

    free(namelist);
  }

  return number;

#endif
}

/**
  This method checks whether a file with the given name exists or not
  @param strFileName name of the file to check
  @return True if the file exists, false otherwise
*/
bool FileSystem::fileExists(std::string strFileName)
{
  struct stat stFileInfo;

  // Attempt to get the file attributes
  if(stat(strFileName.c_str(), &stFileInfo) == 0)
  {
    // We were able to get the file attributes
    // so the file obviously exists.
    return true;
  }
  else
  {
    // We were not able to get the file attributes.
    // This may mean that we don't have permission to
    // access the folder which contains this file. If you
    // need to do that level of checking, lookup the
    // return values of stat which will give you
    // more details on why stat failed.
    return false;
  }
}


}; // end namespace Klaus
