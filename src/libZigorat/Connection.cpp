/*
*********************************************************************************
*          Connection.cpp : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Class definitions for DataPorter, Connection, SimSpark    *
*            and TCPThread utilities for network manipulation                   *
*                                                                               *
*********************************************************************************
*/

/** \file Connection.cpp
<pre>
<b>File:</b>          Connection.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Class definition for DataPorter, Connection, SimSpark
            and TCPThread utilities for network manipulation
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
10/12/2008       Mahdi           Added multi-threading support
</pre>
*/

#ifndef WIN32
  #include <netdb.h>
  #include <errno.h>
#endif

#include <cstring>   // needed for strlen, memset
#include <iostream>  // needed for cout
#include <cstdlib>   // needed for strerror
#include <sstream>   // needed for stringstream

#include "Connection.h"

using std::cout;
using std::cerr;
using std::endl;

namespace Klaus
{

#define MAX_CONN_QUEUE 5 /*!< Maximum connection waiting to be listened */

#ifdef WIN32
DWORD WINAPI SynchRecvCallback(LPVOID conn);
#else
void * SynchRecvCallback(void * conn);
#endif

void PrintLastNetworkError(std::string strHeader);
bool ShowData(std::string str);



/********************************************************************/
/************************  Class DataPorter  ************************/
/********************************************************************/

/**
  Constructor of DataPorter, initializes connection mode (UDP/TCP)
  and creates sockets to send/recv data
  @param conn ConnectionType connection mode: UDP/TCP
  @param socket_fd int pre-initialized socket descriptor
*/
DataPorter::DataPorter(ConnectionType conn, int socket_fd)
{
  #ifdef WIN32
    if(DataPorter::mWSAInstances == 0)
    {
      // Start the WS2_32.dll
      WORD wVersionRequested;
      WSADATA wsaData;
      int err;

      wVersionRequested = MAKEWORD(2, 2);

      err = WSAStartup(wVersionRequested, &wsaData);
      if(err != 0)
      {
        /* Tell the user that we could not find a usable */
        /* WinSock DLL.                                  */
        return;
      }

      /* Confirm that the WinSock DLL supports 2.2.*/
      /* Note that if the DLL supports versions greater    */
      /* than 2.2 in addition to 2.2, it will still return */
      /* 2.2 in wVersion since that is the version we      */
      /* requested.                                        */

      if(LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2)
      {
        /* Tell the user that we could not find a usable */
        /* WinSock DLL.                                  */
        WSACleanup();
        return;
      }
    }

    /* The WinSock DLL is acceptable. Proceed. */
    DataPorter::mWSAInstances++;
  #endif

  mSocketFD = -1;
  setSocketFD(socket_fd, conn);
}

/**
  Destructor of DataPorter, closes available connections
*/
DataPorter::~DataPorter()
{
  if(mSocketFD != -1)
  #ifdef WIN32
    closesocket(mSocketFD);
  #else
    close(mSocketFD);
  #endif

  #ifdef WIN32
    DataPorter::mWSAInstances--;
    if(DataPorter::mWSAInstances == 0)
      WSACleanup();
  #endif
}

/**
  This method returns connection mode: UDP/TCP
  @return ConnectionType connection mode
*/
ConnectionType DataPorter::getConnectionType() const
{
  return mConnectionType;
}

/**
  This methos returns socket descriptor
  @return socket descriptor of current socket conenction
*/
int DataPorter::getSocketFD() const
{
  return (int)mSocketFD;
}

/**
  This method sets the current socket descriptor to a pre-initialiazed
  socket holder, and sets the connection mode
  @param socket_fd pre-initialized socket descriptor
  @param conn Connection mode: TCP/UDP
  @return bool inidicating whether class initialized or not
*/
bool DataPorter::setSocketFD(int socket_fd, ConnectionType conn)
{
  if(active())
    return false;

  mConnectionType = conn;
  mSocketFD = socket_fd;
  return true;
}

/**
  This method sends a message through socket connection which is made
  @param msg message or data to send
  @param iLength length of message to send
  @return int number of bytes totally sent
*/
int DataPorter::sendMessage(const void * msg, int iLength) const
{
  if(!active())
    return -1;

  int iRet = -1;
  if(getConnectionType() == TCP)
  {
    #ifdef WIN32
      iRet = send(mSocketFD, (const char *)msg, iLength, 0);
    #else
      iRet = send(mSocketFD, msg, iLength, 0);
    #endif
  }
  else
  {
    cerr << "(DataPorter) attempted to send to a UDP packet via a TCP socket" << endl;
    return -1;
  }

  if(iRet == -1)
    PrintLastNetworkError("(DataPorter) ");

  return iRet;
}

/**
  This method sends a message through a UDP socket connection which is made
  @param msg message or data to send
  @param iLength length of message to send
  @param remote Socket descriptor of the data porter
  @return int number of bytes totally sent
*/
int DataPorter::sendMessageUDP(const void * msg, int iLength, const sockaddr_in * remote) const
{
  if(!active())
    return -1;

  int iRet = -1;
  if(getConnectionType() == UDP)
  {
    #ifdef WIN32
      iRet = sendto(mSocketFD, (const char *)msg, iLength, 0, (sockaddr*)remote, sizeof(*remote));
    #else
      iRet = sendto(mSocketFD, msg, iLength, 0, (sockaddr*)remote, sizeof(*remote));
    #endif
  }
  else
  {
    cerr << "(DataPorter) attempted to send to a TCP packet via a UDP socket" << endl;
    return 0;
  }

  if(iRet == -1)
    PrintLastNetworkError("(DataPorter) ");

  return iRet;
}

/**
  This method recieves any data which is ready in connection to read
  @param msg pointer to write recieved bytes to
  @param iMaxLength maximum length of bytes that can be stored in msg
  @param time_out Maximum tolerable time for data to be recieved
  @return int number of bytes totally recieved
*/
int DataPorter::recvMessage(void * msg, int iMaxLength, int time_out) const
{
  if(!active())
    return -1;

  // Wait for data if time_out is provided
  fd_set fds;
  int iReturn;
  if(time_out > 0)
  {
    iReturn = waitForData(fds, time_out);
    if(iReturn == -1)
    {
      PrintLastNetworkError("(DataPorter) ");
      return -1;
    }
    else if(iReturn == 0)
    {
      cerr << "(DataPorter) Wait for data timed out (" << time_out << " secs)" << endl;
      return 0;
    }
  }

  // If time_out is not provided, or it is provided and data is received then read the data
  if((time_out < 0) || (FD_ISSET(getSocketFD(), &fds)))
  {
    if(getConnectionType() == TCP)
    {
      #ifdef WIN32
        iReturn = recv(getSocketFD(), (char *)msg, iMaxLength, 0);
      #else
        iReturn = recv(getSocketFD(), msg, iMaxLength, 0);
      #endif
    }
    else
    {
      sockaddr remote;
      #ifdef WIN32
        int fromlen = sizeof(sockaddr_in);
        iReturn = recvfrom(getSocketFD(), (char *)msg, iMaxLength, 0, &remote, &fromlen);
      #else
        socklen_t fromlen = sizeof(sockaddr_in);
        iReturn = recvfrom(getSocketFD(), msg, iMaxLength, 0, &remote, &fromlen);
      #endif
    }
  }
  else
    return -1;

  if(iReturn == -1)
  {
    PrintLastNetworkError("(DataPorter) ");
    return -1;
  }
  else if(iReturn == 0)
    return 0;
  else
    (((char*)msg)[ iReturn ]) = '\0';

  return iReturn;
}

#ifndef WIN32

/**
  This method writes a message to its peer
  @param msg message to be written for peer
  @return int number of charactors sent
*/
int DataPorter::writeMessage(const char * msg) const
{
  return writeMessage((const void*)(msg), (int)strlen(msg));
}

/**
  This method writes a message to its peer
  @param msg message to be written for peer
  @param iLength message length
  @return int number of charactors sent
*/
int DataPorter::writeMessage(const void * msg, int iLength) const
{
  if(!active() || getConnectionType() != TCP)
    return -1;
  else
    return write(getSocketFD(), msg, iLength);
}

/**
  This method reads messages from a network connection
  @param msg buffer to write message to
  @param iLength Maximum length of buffer
  @return int number of bytes totally read
*/
int DataPorter::readMessage(void * msg, int iLength) const
{
  if(!active() || getConnectionType() != TCP)
    return -1;

  fd_set fds;
  FD_ZERO(&fds);
  FD_SET(getSocketFD(), &fds);

  ((char*)msg)[0] = '\0';

  if(select(getSocketFD() + 1, &fds, 0, 0, 0) > 0)
    return read(getSocketFD(), msg, iLength);
  else
    return -1;
}

#endif

/**
  This method determines whether this socket is active or not
  @return bool whether connection is open
*/
bool DataPorter::active() const
{
  return
    mSocketFD != -1;
}

/**
  This method waits for data  to arrive on socket, waits until data
  comes or a time-out event occures
  @param fds Internal variable to synch
  @param iSecs seconds to wait until data time-out occures
*/
int DataPorter::waitForData(fd_set & fds, int iSecs) const
{
  timeval tv;

  iSecs = iSecs > 0 ? iSecs : 0;
  tv.tv_sec = iSecs != -1 ? iSecs : 1000000;
  tv.tv_usec = 1000000;

  FD_ZERO(&fds);
  FD_SET(this->getSocketFD(), &fds);

  return select(getSocketFD() + 1, &fds, NULL, NULL, &tv) >= 0;
}

/**
  This method opens the socket for the first time & initializes
  the socket properties
  @return bool indicating update was successful
*/
bool DataPorter::open()
{
  if(active())
    return false;

  int conn = (mConnectionType == TCP ? SOCK_STREAM : SOCK_DGRAM);
  int yes = 1;

  if((mSocketFD = socket(PF_INET, conn, 0)) == -1)
  {
    PrintLastNetworkError("(DataPorter::open) ");
    return false;
  }

#ifdef WIN32
  BOOL bOptVal = TRUE;
  if(setsockopt(mSocketFD, SOL_SOCKET, SO_REUSEADDR, (char*)&bOptVal, sizeof(int)) == -1)
#else
  if(setsockopt(mSocketFD, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
#endif
  {
    PrintLastNetworkError("(DataPorter::open) ");
    return false;
  }

  return true;
}

/**
  This method re-initializes the socket connection
  @return bool indicating whether update was successful
*/
bool DataPorter::reOpen()
{
  closeAll();
  open();

  return true;
}

/**
  This method closes socket connections and clears internal fields
  @return bool inidicating whether socket closed normally or not
*/
bool DataPorter::closeAll()
{
  if(!active())
    return false;

  #ifdef WIN32
    closesocket(mSocketFD);
  #else
    close(mSocketFD);
  #endif
  mSocketFD = -1;

  return true;
}




/********************************************************************/
/************************  Class Connection  ************************/
/********************************************************************/

/**
  This is the constructor for Connection class which encapsulates
  connection routines under linux API, like listen(), accept() and
  others; Initializes with connection mode: TCP/UDP
  @param conn Connection mode to use: TCP/IP & UDP/IP are supported
  @param strHost hostname to connect to
  @param iPort Port to connect to on remote host
  @param iSocketFD socket descriptor if this connection should be on a an existing one
*/
Connection::Connection(ConnectionType conn, const char *strHost, int iPort, int iSocketFD)
{
  mThreadStarter               = NULL;
  mSynchRecvCallback           = NULL;
  mTimeout                     = -1;
  bContinue                     = false;

  mSelfAddress.sin_family      = AF_INET;        // host byte order
  mSelfAddress.sin_port        = htons(0);     // short, network byte order
  mSelfAddress.sin_addr.s_addr = INADDR_ANY;     // automatically fill with my IP
  memset(&(mSelfAddress.sin_zero), '\0', 8);   // zero the rest of the struct

  mSocket = new DataPorter(conn, iSocketFD);

  if(iSocketFD == -1 && strHost && iPort != -1)
    openConnection(strHost, iPort);
}

/**
  This is the destructor of Connection which closes available
  connections and frees up all memory used */
Connection::~Connection()
{
  closeConnection();

  delete mSocket;
};

/**
  This method returns whether connection is made or not
  @return bool whether connection is active
*/
bool Connection::connected() const
{
  return mSocket->active();
}

/**
  This method returns connection mode, ie. TCP/UDP
  @return bool indicating connection mode
*/
ConnectionType Connection::getConnectionType() const
{
  return mSocket->getConnectionType();
}

/**
  This method returns local port assigned to connection
  @return int current local port being used
*/
int Connection::getLocalPort() const
{
  return mSelfAddress.sin_port;
}

/**
  This method sets local port to use
  @param iPort port to use for connections
  @return bool whether update was successful
*/
bool Connection::setLocalPort(int iPort)
{
  if(connected())
    return false;

  mSelfAddress.sin_port = iPort;
  return true;
}

/**
  This method returns socket descriptor
  @return socket descriptor
*/
int Connection::getSocketFD() const
{
  return mSocket->getSocketFD();
}

/**
  This method makes a network connection to a server
  listening for connection
  @param str hostname to connect to
  @param iPort port to connect on remote machine
  @return bool whether connection is made or not
*/
bool Connection::openConnection(const char *str, int iPort)
{
  struct hostent *he;
  if((he = gethostbyname(str)) == NULL)
  {
    // get the host info
    PrintLastNetworkError("(Connection::openConnection) ");
    return false;
  }

  mSocket->reOpen();

  mRemote.sin_family = AF_INET;    // host byte order
  mRemote.sin_port   = htons(iPort); // short, network byte order
  mRemote.sin_addr   = *((struct in_addr *)he->h_addr);
  memset(&(mRemote.sin_zero), '\0', 8); // zero the rest of the struct
  if(getConnectionType() == TCP
      &&
      connect(mSocket->getSocketFD(), (struct sockaddr *)&mRemote, sizeof(struct sockaddr)) == -1)
  {
    PrintLastNetworkError("(Connection::openConnection) ");
    mSocket->closeAll();
    return false;
  }

  return true;
}

/**
  This method starts listening on port specified
  for any connection to be made
  @param iPort port to start listening on
  @return bool whether server started listening or not
*/
bool Connection::startServer(int iPort)
{
  if(connected())
    return false;

  mSocket->reOpen();

  mSelfAddress.sin_port = htons(iPort);     // short, network byte order
  if(bind(mSocket->getSocketFD(), (struct sockaddr *)&mSelfAddress, sizeof(struct sockaddr)) == -1)
  {
    PrintLastNetworkError("(Connection::startServer) ");
    return false;
  }

  if(getConnectionType() == TCP && listen(mSocket->getSocketFD(), MAX_CONN_QUEUE) == -1)
  {
    PrintLastNetworkError("(Connection::startServer) ");
    return false;
  }

  return true;
}

/**
  This method waits until a connection is made and a
  client asked for a connection
  @return bool if listening was successful
*/
bool Connection::waitForConnection()
{
  if(getConnectionType() != TCP)
    return false;

#ifdef WIN32
  SOCKET new_fd;
  int sin_size = sizeof(struct sockaddr_in);
#else
  int new_fd;
  socklen_t sin_size = sizeof(struct sockaddr_in);
#endif
  sockaddr_in their_addr;

  // This seems to need the listening part to be run on another thread
  // Any suggestions?
  while(1)
  {
    if((new_fd = accept(mSocket->getSocketFD(), (struct sockaddr *)&their_addr, &sin_size)) == -1)
    {
      PrintLastNetworkError("(Connection::waitForConnection) ");
      return false;
    }

    cout << "server: got connection from " << inet_ntoa(their_addr.sin_addr) << endl;
    if(mThreadStarter)
      mThreadStarter((int)new_fd);
    else
    {
      cerr << "No handler registered for handling incoming connection, Connection will be killed" << endl;

      #ifdef WIN32
        DataPorter sock(TCP, (int)new_fd);
        sock.closeAll();
      #else
        if(!fork())
        {                                                                // this is the child process
          mSocket->closeAll();                                        // child doesn’t need the listener
          DataPorter new_socket(TCP, new_fd);
          new_socket.sendMessage("Hello, world!\n", 14);
          new_socket.closeAll();

          exit(0);                                                       // Child exits
        }
      #endif
    }
  }

  return true;
}

/**
  This method closes connections on client side,
  or stops listening with servers
  @return bool whether any connection was closed
*/
bool Connection::closeConnection()
{
  if(!connected())
    return false;

  mSocket->closeAll();
  return true;
}

/**
  This method waits for data to be recieved to the socket
  @param time_out time in seconds for socket to determine packets are availabe
  @return bool indicating socket status
*/
bool Connection::waitForData(int time_out) const
{
  fd_set fds;

  return
    mSocket->waitForData(fds, time_out) >= 0;
}

/**
  This method sends a message via a network socket connection made
  @param strMsg message to send via network connection
  @return int On error returns -1 or 0, otherwise returns nr bytes sent
*/
int Connection::sendMessage(const char * strMsg) const
{
  return
    sendData((void*)strMsg, (int)strlen(strMsg));
}

/**
  This method sends a message via a network socket connection made
  @param data message to send via network connection
  @param iLength Length of the message
  @return int On error returns -1 or 0, otherwise returns nr bytes sent
*/
int Connection::sendData(const void * data, int iLength) const
{
  if(!connected())
    return -1;

  if(mSocket->getConnectionType() == TCP)
    return mSocket->sendMessage(data, iLength);
  else
    return mSocket->sendMessageUDP(data, iLength, &mRemote);
}

/**
  This method  attempts  to read written  data on the socket from the
  peer and  returns the data to the caller. In case more than timeout
  seconds passed from the attempt, the attempt will fail and function
  will return.
  This method does not attempt to read any specific amount of data so
  that all the derived classes would implement the feature if needed.
  @param strMsg Buffer holder of the recieved data
  @param iMaxLen The buffer's size
  @param time_out maximum time to wait for socket to recieve data
  @return int amount of recieved data in bytes
*/
int Connection::recvMessage(char * strMsg, int iMaxLen, int time_out) const
{
  if(!connected())
    return -1;
  else
    return mSocket->recvMessage(strMsg, iMaxLen - 1, time_out);
}

/**
  This method recieves message synchronically with all of application's threads
  and whenever a message is recived, it is passed on to  the provided function.
  Continual of  receiving is dependent on the passed functions return value. In
  case it returns true, listening for data continues
  Note that this method creates  a thread for synchronically getting  data, the
  main application should not finish when this thread exists, so no joint_thread
  is used
  @param callback The function to pass recieved data to
  @param time_out Nr of seconds to determine peer diconnection
*/
void Connection::recvMessage(bool (*callback)(std::string), int time_out)
{
  if(!connected())
    return;

  bContinue = true;

  mTimeout = time_out;
  mSynchRecvCallback = callback;
  #ifdef WIN32
    HANDLE listen_thread = CreateThread(NULL, 0, SynchRecvCallback, this, 0, NULL);

  #else

    pthread_t listen_thread;
    pthread_create(&listen_thread, NULL, SynchRecvCallback, this);
  #endif

  // Theads will be destroyed when connection is destroyed, closed or recving
  // cancelled outside thread
}

/**
  This method does the actual receiving of the data in synchroneos way
  It means that this function is called in a seperate thread.
*/
void Connection::synchRecv()
{
  const int  iMaxLen         = 4096;
        char strMsg[iMaxLen] = "";
        bool bContinue       = true;

  while(bContinue)
  {
    int iBytesRecieved = mSocket->recvMessage(strMsg, iMaxLen - 1, mTimeout);
    if(iBytesRecieved == -1)
      bContinue = false;
    else
    {
      strMsg[ iBytesRecieved ] = '\0';
      bContinue &= mSynchRecvCallback(strMsg);
      // &=, since it may change outside this thread
    }
  }
}

/**
  This method stops receiving data by setting the corresponding variable to false
*/
void Connection::stopReceiving()
{
  bContinue = false;
}

#ifndef WIN32

/**
  This method reads a message from the connection. When there is
  no message available, it does not wait for one.
  @param msg string in which message is stored
  @param iMaxLength Maximum length of the message
  @return bool indicating any message were read or not
*/
bool Connection::readMessage(void * msg, int iMaxLength) const
{
  return (mSocket->readMessage(msg, iMaxLength) > 0);
}

/**
  This method writes a message to its connected peer
  @param msg Message to be written for peer
  @return bool indicating whether message was written or not
*/
bool Connection::writeMessage(const char * msg) const
{
  return writeMessage((void*)msg, (int)strlen(msg));
}

/**
  This method writes a message to its connected peer
  @param msg Message to be written for peer
  @param iLength Message length
  @return bool indicating whether message was written or not
*/
bool Connection::writeMessage(const void * msg, int iLength) const
{
  return mSocket->writeMessage(msg, iLength) != 0;
}

#endif

/**
  This method sets the function to attempt to use a connection made with
  the server side. The server will pass the new client socket to provided
  function. Then the function can create a new thread to use the data.
  @param starter The thread to which is responsible to handle incoming connections
*/
void Connection::setConnectionHandler(void(*starter)(int))
{
  mThreadStarter = starter;
}




/***************************************************************************/
/****************************  Class TCPThread  ****************************/
/***************************************************************************/

/**
  This is the constructor of the TCPThread. It initializes with a socket
  descriptor value
  @param iSocket Socket descriptor value
*/
TCPThread::TCPThread(int iSocket)
{
  mConnection  = new Connection(TCP, "", -1, iSocket);
  mInstCreated = true;
}

/**
  This is the constructor of the TCPThread, It initializes with a
  Connection instance
  @param conn Connection instance
*/
TCPThread::TCPThread(Connection * conn)
{
  mConnection  = conn;
  mInstCreated = false;
}

/**
  This is the constructor of the TCPThread, It initializes with a
  socket descriptor
  @param port Socket descriptor instance
*/
TCPThread::TCPThread(DataPorter * port)
{
  mConnection  = new Connection(TCP, "", -1, port->getSocketFD());
  mInstCreated = true;
}

/**
  This is the destructor of the TCPThread, it cleans up sockets
  and connections if any were created
*/
TCPThread::~TCPThread()
{
  if(mInstCreated)
    delete mConnection;
}

/**
  This method executes the main loop of the thread, utilizing the connections
*/
void TCPThread::execute()
{
  mConnection->recvMessage(ShowData);
  while(mConnection->connected() && (mConnection->sendMessage("Hello World!\n") > 0))
    {}
}



/***************************************************************************/
/**************************  Utility functions  ****************************/
/***************************************************************************/

#ifdef WIN32
int DataPorter::mWSAInstances = 0; /** The definition of the controller on windows sockets */
#endif

/**
  This method creates a thread to handle recieving data independent
  of the applications main loop.
  @param conn The connection object to create a receive thread for it
  @return Always NULL
*/
#ifdef WIN32
DWORD WINAPI SynchRecvCallback(LPVOID conn)
{
  ((Connection*)conn)->synchRecv();

  return NULL;
}
#else
void * SynchRecvCallback(void * conn)
{
  static_cast<Connection*>(conn)->synchRecv();

  return NULL;
}
#endif

/**
  This method prints a network related error in human understandable text.
  @param strHeader String to insert before the actual error text
*/
void PrintLastNetworkError(std::string strHeader)
{
  std::stringstream ss;
  ss << strHeader;

  #ifdef WIN32
    /*
      Errors are handled by calling the WSAGetLastError routine which
      will return the last error as one of the following. As we develop
      this tutorial, we will go into much more detail on what they mean
      and what caused them.
    */

    switch (WSAGetLastError())
    {
      case WSANOTINITIALISED :
        ss << "Unable to initialise socket." << endl;
        break;

      case WSAEAFNOSUPPORT :
        ss << "The specified address family is not supported." << endl;
        break;

      case WSAEADDRNOTAVAIL :
        ss << "Specified address is not available from the local machine." << endl;
        break;

      case WSAECONNREFUSED :
        ss << "The attempt to connect was forcefully rejected." << endl;
        break;

      case WSAEDESTADDRREQ :
        ss << "Destination address is required." << endl;
        break;

      case WSAEFAULT :
        ss << "The namelen argument is incorrect." << endl;
        break;

      case WSAEINVAL :
        ss << "The socket is not already bound to an address." << endl;
        break;

      case WSAEISCONN :
        ss << "The socket is already connected." << endl;
        break;

      case WSAEADDRINUSE :
        ss << "The specified address is already in use." << endl;
        break;

      case WSAEMFILE :
        ss << "No more file descriptors are available." << endl;
        break;

      case WSAENOBUFS :
        ss << "No buffer space available. The socket cannot be created." << endl;
        break;

      case WSAEPROTONOSUPPORT :
        ss << "The specified protocol is not supported." << endl;
        break;

      case WSAEPROTOTYPE :
        ss << "The specified protocol is the wrong type for this socket." << endl;
        break;

      case WSAENETUNREACH :
        ss << "The network can't be reached from this host at this time." << endl;
        break;

      case WSAENOTSOCK :
        ss << "The descriptor is not a socket." << endl;
        break;

      case WSAETIMEDOUT :
        ss << "Attempt timed out without establishing a connection." << endl;
        break;

      case WSAESOCKTNOSUPPORT :
        ss << "Socket type is not supported in this address family." << endl;
        break;

      case WSAENETDOWN :
        ss << "Network subsystem failure." << endl;
        break;

      case WSAHOST_NOT_FOUND :
        ss << "Authoritative Answer Host not found." << endl;
        break;

      case WSATRY_AGAIN :
        ss << "Non-Authoritative Host not found or SERVERFAIL." << endl;
        break;

      case WSANO_RECOVERY :
        ss << "Non recoverable errors, FORMERR, REFUSED, NOTIMP." << endl;
        break;

      case WSANO_DATA :
        ss << "Valid name, no data record of requested type." << endl;
        break;

      case WSAEINPROGRESS :
        ss << "Windows Sockets operation is in progress." << endl;
        break;

      case WSAEINTR :
        ss << "The (blocking) call was canceled via WSACancelBlockingCall()." << endl;
        break;

      default :
        ss << "Unknown error." << endl;
        break;
    }

    cerr << ss.str() << endl;
  #else
    cerr << strHeader << strerror(errno) << endl;
  #endif
}

/**
  This method simply shows some data to standard output
  @param str Data to show
  @return Always true!
*/
bool ShowData(std::string str)
{
  cout << str << endl;
  cout.flush();

  return true;
}



};  // end namespace Klaus

