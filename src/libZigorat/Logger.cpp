/*
Copyright (c) 2000-2003, Jelle Kok, University of Amsterdam
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the University of Amsterdam nor the names of its
contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/** \file Logger.cpp
<pre>
<b>File:</b>          Logger.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: UvA Trilearn
<b>Authors:</b>       Jelle Kok
<b>Created:</b>       3/3/2000
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains the definitions for the class to log
               information about the system to any output stream. A
               range can be specified for which the received log
               information is printed. Furthermore it is possible to
               print the time since the timer of the Logger has been
               restarted.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
3/3/2001         Jelle Kok       Initial version created
</pre>
*/


#include "Logger.h"
#include "Parse.h"

#include <stdio.h>    // needed for vsprintf
#include <cstring>    // needed for strlen
#include <sstream>    // needed for std::stringstream
#include <stdarg.h>   // needed for va_list
#include <iomanip>    // needed for setprecision



/*! Logger singleton instance */
template<> _DLLExportControl Klaus::Logger* Klaus::Singleton<Klaus::Logger>::mInstance = 0;

namespace Klaus
{


/*****************************************************************************/
/********************************* CLASS TIMING ******************************/
/*****************************************************************************/

/**
  This method returns the difference between two timevals in seconds.
  @param tv1 first timeval
  @param tv2 second timeval
  @return double representing the difference between t1 and t2 in seconds
*/
#ifdef WIN32
double Timing::getTimeDifference( DWORD tv1, DWORD tv2 )
{
  return  ((double)(tv1 - tv2) / 1000.0) ;
}
#else
double Timing::getTimeDifference( struct timeval tv1, struct timeval tv2 )
{

  return  ((double)tv1.tv_sec + (double)tv1.tv_usec/1000000 ) -
          ((double)tv2.tv_sec + (double)tv2.tv_usec/1000000 ) ;
}
#endif

/**
  This method returns the time (in seconds) since the last time the timer was restarted.
  @param iFactor The Factor at which the result is divided. If for example
                 it is given 0.001, the result will be in milliseconds
  @return seconds that have passed since last restart of timer
*/
double Timing::getElapsedTime(double iFactor)
{
#ifdef WIN32
  DWORD time2 = GetTickCount();
#else
  struct timeval time2;
  gettimeofday(&time2, NULL);
#endif
  return getTimeDifference(time2, time1) / iFactor;
}

/**
  This method restarts the timer by setting it to the current time
*/
void Timing::restartTime( )
{
#ifdef WIN32
  time1 = GetTickCount();
#else
  gettimeofday( &time1, NULL );
#endif
}

/**
  This method returns current time information as a double value in seconds
  @return double Current time value as double
*/
double Timing::now()
{
  #ifdef WIN32
    DWORD time = GetTickCount();
    return (double)time / 1000.0;
  #else
    struct timeval time;
    gettimeofday( &time, NULL );
    return ((double)time.tv_sec + (double)time.tv_usec/1000000);
  #endif
}




/*****************************************************************************/
/********************************** LOGGER ***********************************/
/*****************************************************************************/

/**
  This is the constructor for the Logger. The output stream, the minimal and
  maximal log level can all be specified. The timer in this class is also
  restarted.
*/
Logger::Logger()
{
  mCacheSize = 0;
  mHeader[0] = 0;
  mTiming.restartTime();
  mOS = &std::cout;
}

/**
  This is the destructor of Logger which flushes any non written data to output
*/
Logger::~Logger()
{
  flush();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Logger * Logger::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new Logger;
    mInstance->log(1, "(Logger) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Logger::freeSingleton()
{
  if(!mInstance)
    std::cerr<< "(Logger) no singleton instance";
  else
  {
    mInstance->log(1, "(Logger) removed singleton");
    delete mInstance;
    mInstance = NULL;
  }
}

/**
  This method adds a given std::string to the cache of logger
  @param str String to be added to the logger cache
  @param iLevel The level to write the log to
*/
void Logger::addToCache(const std::string & str, int iLevel)
{
  LogCache l;
  l.mLog = str;
  l.mLevel = iLevel;

  mCachedLogs.push_back(l);

  if(mCacheSize < cLogLines)
    ++mCacheSize;
  else
    mCachedLogs.pop_front();
}

/**
  This method can be used to log information. Only when the specified
  level of the message is part of the set of logged levels the
  information is logged. This method receives a a normal std::string that
	is logged.
  @param iLevel level corresponding to this message
  @param str std::string that is logged when iLevel is a logging lvel.
  @return bool indicating whether the message was logged or not.
*/
bool Logger::log(int iLevel, const std::string & str)
{
  if(!isInLogLevel(iLevel))
    return false;

  // take away the output stream and caches
  mLogMutex.lock();

  if(mOS)
    *mOS << mHeader << str;

  addToCache(str, iLevel);

  // give it back
  mLogMutex.unlock();

  return true;
}

/**
  This method can be used to log information. Only when the specified
  level of the message is part of the set of logged values the
  information is logged. This method receives a character std::string that may
  contain format specifiers that are also available to 'printf' (like %d, %f,
  etc.). The remaining arguments are the variables that have to be filled in
  at the location of the specifiers.
  @param iLevel level corresponding to this message
  @param str character std::string with (possible) format specifiers
  @param ... variables that define the values of the specifiers.
  @return bool indicating whether the message was logged or not.
*/
bool Logger::log(int iLevel, const char *str, ...)
{
  if(!isInLogLevel(iLevel))
    return false;

  // take away the output stream and caches
  mLogMutex.lock();

  // create the std::string to log
  va_list ap;
#ifdef Solaris
  va_start(ap);
#else
  va_start(ap, str);
#endif
  if(vsnprintf(mBuf, cLogSize-1, str, ap) == -1)
    std::cerr << "(Logger::log) buffer is too small!\n";
  va_end(ap);
  std::string log_str = std::string(mHeader) + mBuf;

  // log it
  if(mOS)
    *mOS << log_str << std::endl;

  // cache it
  addToCache(log_str, iLevel);

  // give it back
  mLogMutex.unlock();

  return true;
}

/**
  This method can be used to log information. Only when the specified
  level of the message is an element in the set of logged levels the
  information is logged. This method receives a character string that may
  contain format specifiers that are also available to 'printf' (like %d, %f,
  etc.). The remaining arguments are the variables that have to be filled in
  at the location of the specifiers. Before the logged message the elapsed
  time since the timer has been restarted is printed.
  @param iLevel level corresponding to this message
  @param dAccuracy The accuracy of the decimal digits
  @param str character string with (possible) format specifiers
  @param ... variables that define the values of the specifiers.
  @return bool indicating whether the message was logged or not.
*/
bool Logger::logWithTime(int iLevel, double dAccuracy, const char *str, ... )
{
  if(!isInLogLevel(iLevel))
    return false;

  // take away the output stream and caches
  mLogMutex.lock();

  va_list ap;
  #ifdef Solaris
    va_start(ap);
  #else
    va_start(ap, str);
  #endif

  if(vsnprintf(mBuf, cLogSize-1, str, ap) == -1)
    std::cerr << "Logger::log, buffer is too small!" << "\n";
  va_end(ap);

  std::stringstream ss;
  ss << mTiming.getElapsedTime(dAccuracy) << ':' << mHeader << mBuf << std::endl;
  std::string s = ss.str();

  if(mOS)
    *mOS << s << std::endl;

  addToCache(s, iLevel);

  // give it back
  mLogMutex.unlock();

  return true;
}

/**
  This method restarts the timer associated with this Logger.
*/
void Logger::restartTimer()
{
  return mTiming.restartTime();
}

/**
  Return the instance of the timing class that denotes the time
  the counter is running.
*/
Timing Logger::getTiming( )
{
  return mTiming;
}

/**
  This method returns whether the supplied log level is recorded, thus
  within the interval [min_log_level..max_log_level] or equal to the
  extra log level.
  @param iLevel log level that should be checked
  @return bool indicating whether the supplied log level is logged.
*/
bool Logger::isInLogLevel( int iLevel )
{
  if(iLevel == 0)
    return true;

  return mLogLevels.find(iLevel) != mLogLevels.end() ;
}

/**
  This method removes all log levels.
*/
void Logger::clearLogLevels()
{
  mLogLevels.clear();
}

/**
  This method inserts the log level 'iLevel' to the set of logged levels.
  Information from this log level will be printed.
	@param iLevel level that will be added to the set
  @return bool indicating whether the update was successfull.
*/
bool Logger::addLogLevel( int iLevel )
{
  mLogLevels.insert(iLevel);
  return true;
}

/**
  This method inserts all the log levels in the interval [iMin..iMax] to
  the set of logged levels.
  @param iMin minimum log level that is added
  @param iMax maximum log level that is added
  @return bool indicating whether the update was successfull.
*/
bool Logger::addLogRange(int iMin, int iMax)
{
  bool bReturn = true;
  for(int i = iMin ; i <= iMax;  ++i)
    bReturn &= addLogLevel(i);
  return bReturn;
}

/**
  This method returns the current header that is written before
  the actual text that has to be logged.
  @return current header
*/
const char* Logger::getHeader( )
{
  return mHeader;
}

/**
  This method sets the header that is written before the actual logging text.
  @param str that represents the character std::string
  @return bool indicating whether the update was succesfull
*/
bool Logger::setHeader(const char *str)
{
  strcpy(mHeader, str);
  return true;
}

/**
  This method sets a predefined header that is written before the actual
  logging text. The header is represented by one integer which is followed by
  a semicolon (":") .
  @param d A number for log header
  @return bool indicating whether the update was succesfull
*/
bool Logger::setHeader(double d)
{
  sprintf(mHeader, "%2.2f: ", d);
  return true;
}

/**
  This method sets a predefined header that is written before the actual
  logging text. The header is represented by a double and an integers
  which are written between parentheses, i.e. (9.23, 2).
  @param d first integer
  @param i second integer
  @return bool indicating whether the update was succesfull
*/
bool Logger::setHeader(double d, int i)
{
  sprintf(mHeader, "(%2.2f, %d) ", d, i);
  return true;
}

/**
  This method returns the log cache deque instance.
  @return True if the given entry index existed, false otherwise
*/
const LogCache::Cache & Logger::getCache()
{
  return mCachedLogs;
}

/**
  This method sets the output stream to which the log information is written.
  This outputstream can be standard output or a reference to a file.
  @param o outputstream to which log information is printed.
  @return bool indicating whether update was succesfull.
*/
bool Logger::setOutputStream(std::ostream* o)
{
  mOS = o;
  return true;
}

/**
  This method returns the output stream to which the log information
  is written. This outputstream can be standard output or a
  reference to a file.
  @return outputstream to which log information is printed.
*/
std::ostream& Logger::getOutputStream()
{
  return *mOS;
}

/**
  This method outputs all the log levels that are logged to the output stream os.
  @param os output stream to which log levels are printed.
*/
void Logger::showLogLevels( std::ostream &os )
{
  std::set<int>::iterator itr;
  for (itr = mLogLevels.begin() ; itr != mLogLevels.end(); ++itr)
    os << *itr << " " ;
  os << "\n";
}

/**
  This method flushes the buffer of current output stream
*/
void Logger::flush()
{
  if(mOS)
    mOS->flush();
}


}; // end namespace Klaus

