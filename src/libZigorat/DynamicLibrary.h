/*
*********************************************************************************
*          DynamicLibrary.h : Robocup 3D Soccer Simulation Team Zigorat         *
*                                                                               *
*  Date: 09/06/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Wrapper for using DLLs in Linux and Windows                        *
*                                                                               *
*********************************************************************************
*/

/*! \file DynamicLibrary.h
<pre>
<b>File:</b>          DynamicLibrary.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       09/06/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Wrapper for using DLLs in Linux and Windows
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
09/06/2008       Mahdi           Initial version created
</pre>
*/

#ifndef DYNAMIC_LIBRARY
#define DYNAMIC_LIBRARY

#include <string>           // needed for std::string
#include <vector>           // needed for std::vector
#include "PlatformMacros.h"

#if PLATFORM == PLATFORM_WINDOWS
  #include <windows.h>
#endif

#if COMPILER == COMPILER_MSVC
#  pragma warning (disable : 4251)
#endif


namespace Klaus
{

  /*! This class manages dynamic library use in multiplatform mode, so that
      plugins and DLLs  could be written  and be  used in several platfroms.
  */
  class _DLLExportControl DynamicLibrary
  {
    private:

    #ifdef WIN32
      HINSTANCE     mHandle;           /*!< Handle of the library  */
    #else
      void        * mHandle;             /*!< Handle of the library  */
    #endif

      std::string   mLibraryName;        /*!< Name of the library    */

      void          showLastError        ( const char * strPrefix    );

    public:
                    DynamicLibrary       (                           );
                  ~ DynamicLibrary       (                           );

      bool          openLibrary          ( const std::string &strLibrary  );
      bool          closeLibrary         (                           );
      void        * getFunctionAddress   ( const std::string &strFunction,
                                           bool bSuppressOutput=false) const;

      std::string   getLibraryName       (                           ) const;
      bool          isOpen               (                           ) const;

      /*! Array is a dynamic array of DynamicLibraries */
      typedef std::vector<DynamicLibrary*> Array;
  };

}; // namespace Klaus


#endif // DYNAMIC_LIBRARY
