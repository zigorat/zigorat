/*
*********************************************************************************
*            FileSystem.h : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 03/03/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: A handy class for interacting with filesystem                      *
*                                                                               *
*********************************************************************************
*/

/*! \file FileSystem.h
<pre>
<b>File:</b>          FileSystem.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/03/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      A handy class for interacting with filesystem
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/03/2011       Mahdi           Initial version created
</pre>
*/

#ifndef FILE_SYSTEM_HELPER
#define FILE_SYSTEM_HELPER


#include "PlatformMacros.h" // needed to disable warnings
#include "Parse.h"          // needed for StringArray

namespace Klaus
{

  /*! This class implements serveral handy methods for
      interacting with file system. */
  class _DLLExportControl FileSystem
  {
    public:
      static int  getFolderContents( const std::string & strFolder,
                                     StringArray & contents         );
      static bool fileExists       ( std::string strFileName        );
  };

}; // end namespace Klaus


#endif // FILE_SYSTEM_HELPER

