/*
*********************************************************************************
*             String.h : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 01/22/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: String class and utilities                                         *
*                                                                               *
*********************************************************************************
*/

/*! \file String.h
<pre>
<b>File:</b>          String.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/22/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      String class and utilities
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/22/2011       Mahdi           Initial version created
</pre>
*/

#ifndef KLAUS_STRING
#define KLAUS_STRING

#include <string>

namespace Klaus
{

  /**
    This class represents a string object. It's reason of creation was heap
    problem in windows when accessing and using strings from a dll or in a
    executable. This String has unicode support and can be converted to
    std::string and std::wstring.
  */
  class String
  {
    private:
      wchar_t * mString;
      size_t    mSize;
      size_t    mCapacity;

      void allocate( size_t new_size );
      void grow( size_t new_size );
      
    public:
      String();
      String( const char * str );
      String( const wchar_t * str );
      String( const std::string & str );
      String( const std::wstring & str );
      ~String();

      std::string toStdString() const;
      std::wstring toStdWString() const;

      void clear();
  };

}; // end namespace Klaus

#endif // KLAUS_STRING
