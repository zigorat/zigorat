/*
*********************************************************************************
*            Threads.cpp : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 10/12/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Thread utilization abstract mechanism                              *
*                                                                               *
*********************************************************************************
*/

/*! \file Threads.cpp
<pre>
<b>File:</b>          Threads.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       10/12/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Thread utilization abstract mechanism
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
10/12/2008       Mahdi           Initial version created
</pre>
*/

#include "Threads.h"

namespace Klaus
{

/**
 * This function executes a thread class, by calling its execute() function, in another thread
 * @param thread A pointer to the thread class instance
 * @return Nothing
 */
#ifdef WIN32
  DWORD WINAPI threadCallBack(LPVOID thread)
  {
    Thread *t = (Thread*)(thread);
    t->execute();

    return NULL;
  }
#else
  void* threadCallBack(void * thread)
  {
    Thread *t = static_cast<Thread*>(thread);
    t->execute();

    return NULL;
  }
#endif

/*****************************************************************/
/***********************  Class Thread  *************************/
/*****************************************************************/

/**
 * The constructor, just sets that this thread is not finished yet!
 */
Thread::Thread()
{
  mTerminated = false;
}

/**
 * The destructor. This should exists, since this class has virtual
 * methods, so a virtual destructor shall exist
 */
Thread::~Thread()
{
}

/**
 * This method tells the thread to finish execution
 */
void Thread::terminate()
{
  mTerminated = true;
}

/**
 * This method starts the thread exection by a thread callback
 * See pthread manual for more information
 * On windows, see Threading manuals
 */
void Thread::startThread()
{
#ifdef WIN32
  mThread = CreateThread( NULL, 0, threadCallBack, this, 0, NULL );
#else
  pthread_create( &mThread, NULL, threadCallBack, this );
#endif
}


};  // end namespace Klaus
