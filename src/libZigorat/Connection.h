/*
*********************************************************************************
*           Connection.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Class decleration for DataPorter, Connection, RoboCupConnection    *
*            and TCPThread utilities for network manipulation                   *
*                                                                               *
*********************************************************************************
*/

/*! \file Connection.h
<pre>
<b>File:</b>          Connection.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Class decleration for DataPorter, Connection, RoboCupConnection
            and TCPThread utilities for network manipulation
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
10/12/2008       Mahdi           Added multi-threading support
</pre>
*/

#ifndef CONNECTION
#define CONNECTION

#ifdef WIN32
  #include <winsock.h>
#else
  #include <arpa/inet.h>
#endif

#include "Threads.h"   // needed for threading support in sockets
#include <string>      // needed for std::string


namespace Klaus
{

  /*! ConnectionT is an enumeration of currently supported connection types */
  enum ConnectionType
  {
    TCP,                /*!< TCP connection mode */
    UDP,                /*!< UDP connection mode */
    UnknownConnection   /*!< Unknown connection mode */
  };


  /*! DataPorter is class tha encapsulates socket connection operations, like
      opening, reusing, sending and recieving data across sockets. This is the
      base thing for network manipulations. */
  class _DLLExportControl DataPorter
  {
    protected:
      #ifdef WIN32
      SOCKET         mSocketFD;          /*!< Socket file descriptor */
      static int     mWSAInstances;     /*!< WSA Initialized or not */
      #else
      int            mSocketFD;          /*!< Socket file descriptor */
      #endif

      ConnectionType mConnectionType;    /*!< Type of the connection, ie. TCP/UDP */

    public:
                     DataPorter           ( ConnectionType  conn,
                                            int socket_fd = -1         );
                   ~ DataPorter           (                            );

      ConnectionType getConnectionType    (                            ) const;

      int            getSocketFD          (                            ) const;
      bool           setSocketFD          ( int socket_fd,
                                            ConnectionType conn        );

      int            sendMessage          ( const void * msg,
                                            int iLength                ) const;
      int            sendMessageUDP       ( const void * msg,
                                            int iLength,
                                            const sockaddr_in * remote ) const;
      int            recvMessage          ( void * msg,
                                            int iMaxLength,
                                            int time_out = -1          ) const;

      #ifndef WIN32
      int            writeMessage         ( const char * msg           ) const;
      int            writeMessage         ( const void * msg,
                                            int iLength                ) const;
      int            readMessage          ( void * msg,
                                            int iLength                ) const;
      #endif

      bool           active               (                            ) const;
      int            waitForData          ( fd_set &fds,
                                            int iSecs                  ) const;

      bool           open                 (                            );
      bool           reOpen               (                            );
      bool           closeAll             (                            );
  };


  /*! Connection is a class that encapsulates connection handling routines,
      like starting listening, accepting connections, sending and receiving
      data, and making connection to existing listening  servers, currently
      supports to modes of communication: TCP/IP & UDP/IP
  */
  class _DLLExportControl Connection
  {
    protected:
      sockaddr_in   mSelfAddress;       /*!< Self socket information           */
      sockaddr_in   mRemote;            /*!< Remote established connection     */
      DataPorter  * mSocket;            /*!< Socket descriptor of connection   */
      int           mTimeout;           /*!< Timeout on peer data exchange     */
      bool          bContinue;          /*!< Continue receiving messages       */

      /*!< function to start the thread */
      void (*mThreadStarter)(int);

      /*!< Function to execute when receiving data */
      bool (*mSynchRecvCallback)(std::string);

    public:
                     Connection           ( ConnectionType  conn,
                                            const char * strHost = "",
                                            int iPort     = -1,
                                            int iSocketFD = -1 );
                   ~ Connection           (                               );

      bool           connected            (                               ) const;
      ConnectionType getConnectionType    (                               ) const;
      int            getLocalPort         (                               ) const;
      bool           setLocalPort         ( int          iPort            );

      int            getSocketFD          (                               ) const;

      bool           openConnection       ( const char * str,
                                            int iPort                     );
      bool           startServer          ( int port                      );
      bool           waitForConnection    (                               );
      bool           closeConnection      (                               );

      bool           waitForData          ( int time_out  = 0             ) const;
      int            sendMessage          ( const char * strMsg           ) const;
      int            sendData             ( const void * data,
                                            int iLength                   ) const;
      int            recvMessage          ( char * strMsg,
                                            int iMaxLen,
                                            int time_out = -1             ) const;
      void           recvMessage          ( bool (*callback)(std::string),
                                            int time_out = -1             );

      void           synchRecv            (                               );
      void           stopReceiving        (                               );

      #ifndef WIN32
      bool           readMessage          ( void * msg,
                                            int iMaxLength                ) const;
      bool           writeMessage         ( const char * msg              ) const;
      bool           writeMessage         ( const void * msg,
                                            int iLength                   ) const;
      #endif

      void           setConnectionHandler ( void (*starter)(int)          );
  };


  /*! This class is for a thread which operates on network connections
      which are based on TCP/IP protocols */
  class TCPThread : public Thread
  {
    private:
      bool         mInstCreated; /*!< Connection instance created? */

    protected:
      Connection * mConnection;  /*!< Connection instance */

    public:
                   TCPThread     ( int iSocket = -1  );
                   TCPThread     ( Connection * conn );
                   TCPThread     ( DataPorter * port );
      virtual    ~ TCPThread     (                   );

      virtual void execute       (                   );
  };

}; // end namespace Klaus


#endif // CONNECTION

