/*
*********************************************************************************
*          SLangParser.cpp : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 05/01/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: A parser for S-Language                                            *
*                                                                               *
*********************************************************************************
*/

/*! \file SLangParser.cpp
<pre>
<b>File:</b>          SLangParser.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       05/01/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      A parser for S-Language
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
05/01/2008       Mahdi           Initial version created
</pre>
*/

#ifdef WIN32
  #define _CRT_SECURE_NO_WARNINGS
  #include <io.h>
#else
  #include <dirent.h>
#endif

#include "SLangParser.h"

#include <iostream>
#include <cstring>
#include <stdlib.h>
#include <sys/stat.h>
#include <cmath>

/*******************************************************************************/
/*****************************  Class SLangParser  *****************************/
/*******************************************************************************/

/**
 * This is an overloaded function to parse intger from a string
 * @param strMsg String containing integer value
 * @param ind Index to start parsing an integer from
 * @return Integer residing string
 */
int SLangParser::parseFirstInt( const string & strMsg, const int & ind )
{
  // nan will be returned 0 for now
  if( strMsg.compare(0, 3, "nan") == 0 )
    return 0;

  int  iRes  = 0;
  bool bIsMin= false;
  unsigned index = ind;
  const unsigned size = strMsg.size();

  while((!isdigit(strMsg[index])) && (strMsg[index] != '-') && (index < size))
    index++;

  if( index != size && strMsg[index] == '-')       // if it was a minus sign, remember
  {
    bIsMin=true;
    index++;
  }

  while( index!=size && strMsg[index]<='9' && strMsg[index]>='0' )   // for all digits
  {
    iRes = iRes*10 + (strMsg[index]-'0');  // multiply old res with 10
    index++;                               // and add new value
  }

  return (bIsMin) ? -iRes : iRes;
}

/**
 * This method parses the first string token encountered from the index
 * @param str String to be searched for the it's first string
 * @param index index from which the search should be started
 * @return The first string that could be parsed from the given string
 */
string SLangParser::parseFirstWord( const string & str, unsigned * index )
{
  string strResult;
  unsigned i = 0;
  if(!index)
    index = &i;

  while(isspace(str[*index]))
    ++*index;

  unsigned start = *index, end = *index + 1;

  while((str[end] != ' ') && (str[end] != '\t') && (str[end] != '\n'))
    end++;

  *index = end + 1;
  return str.substr( start, end - start );
}

/**
 * This method extracts a string from the given string. The extracted string should
 * reside in cotation pairs.
 * @param str String contining the desired string to extract
 * @param index Index which searching should be started from
 * @return The extracted string
 */
string SLangParser::parseFirstQuote( const string & str, unsigned * index )
{
  string strResult;
  unsigned i = 0;
  if(!index)
    index = &i;

  size_t uStart = str.find( "'", *index );
  if( uStart == str.npos )
    return "";

  size_t uEnd = uStart + 1;
  uEnd = str.find( "'", uEnd );
  if( uEnd == str.npos )
    return "";

  *index = uEnd + 1;

  return str.substr( uStart + 1, uEnd - uStart - 1 );
}

/**
 * This method trims a given string by reducing all the spaces
 * back and front of the string
 * @param str String to trim
 * @return Trimmed string
 */
string SLangParser::trim( const string & str )
{
  string result = str;

  string::iterator it = result.begin();
  while(result.size() != 0 && isspace(result[0]))
    result.erase(0, 1);

  size_t size = result.size();
  while(size != 0 && isspace(result[size - 1]))
  {
    result.erase(size - 1, 1);
    size = result.size();
  }

  return result;
}

/**
 * This method parses the given string into tokens which are splitted by a delimiter character.
 * @param str String to parse
 * @param delim The delimiter charactor
 * @param tokens Tokens extracted from the string
 * @return True if anything was taken out of the string, false otherwise
 */
bool SLangParser::parseByCharDelim( string str, const char & delim, StringList & tokens )
{
  if(str.empty())
    return false;

  str.push_back(delim);
  string token;
  const unsigned size = str.size();
  for(unsigned i = 0; i < size; i++)
    if(str[i] == delim)
    {
      if(!token.empty())
        tokens.push_back(token);
      token.clear();
    }
    else
      token.push_back(str[i]);

  return true;
}

/**
 * This is an overloaded function to parse a double from a string
 * @param strMsg String containing double value
 * @return Value residing in string
 */
double SLangParser::parseFirstDouble( const string & strMsg )
{
  const unsigned size = strMsg.size();
  double dRes=0.0, dFrac=1.0;
  bool bIsMin=false, bInDecimal=false, bCont=true;
  unsigned index = 0;

  // go to first part of double (digit, minus sign or '.')
  while( index < size && !isdigit(strMsg[index]) && strMsg[index]!='-' && strMsg[index]!='.')
  {
    // when NaN or nan is double value, return -1000.0
    if( (strMsg[index]=='N' && (size-index-1)>3 && strMsg[index+1]=='a' && strMsg[index+2]=='N') ||
        (strMsg[index]=='n' && (size-index-1)>3 && strMsg[index+1]=='a' && strMsg[index+2]=='n') )
      return -1000.0;
    else
       index++;
  }

  if( strMsg[index] != '\0' && strMsg[index]=='-')      // if minus sign, remember that
  {
    bIsMin=true;
    index++;
  }

  while( strMsg[index] != '\0' && bCont)                // process the number bit by bit
  {
    if( strMsg[index]=='.' )                            // in decimal part after '.'
      bInDecimal = true;
    else if( bInDecimal && strMsg[index]<='9' && strMsg[index]>='0') // number and in decimal
    {
      dFrac=dFrac*10.0;                                 // shift decimal part to right
      dRes += (double)(strMsg[index]-'0')/dFrac;        // and add number
    }
    else if( strMsg[index]<='9' && strMsg[index]>='0' ) // if number and not decimal
      dRes=dRes*10+(double)(strMsg[index]-'0');         // shift left and add number
    else if( strMsg[index]=='e' ) // 10.6e-08           // if to power e
    {
      if( strMsg[index+1] == '+' )                      // determine sign
        dRes *=  pow(10.0, parseFirstInt(strMsg, index));// and multiply with power
      else if( strMsg[index+1] == '-' )
      {
        index += 2;                                     // skip -
        dRes /=  pow(10.0, parseFirstInt(strMsg, index));// and divide by power
      }
      bCont = false;                                    // after 'e' stop
    }
    else
      bCont = false;                                    // all other cases stop

    if( bCont == true )                                 // update when not stopped yet
      index++;
  }

  return (bIsMin && dRes != 0.0) ? -dRes : dRes;
}

/**
 * This method walks through the string starting at the character of current reading index
   of the string and stops the end of the string is reached or a non space is reached.
   The iterator is updated to this new position.
 * @param it Current iterating position of the string
 * @param end Last position of the string
 * @return character that is at the first non space, charactor of #0 when end of string
    is reached.
 */
char SLangParser::gotoFirstNonSpace( string::const_iterator & it, const string::const_iterator & end )
{
  while(it != end && isspace(*it))
    it++;

  return it != end ? *it : '\0';
}

/**
 * This method extracts a S-comment token from the given position of a string.
 * @param it Current iteration position of the string
 * @param end End of the string
 * @param comments Comments to add the extracted comment
 * @return True if all went without errors, false if any errors occured
 */
bool SLangParser::extractSExpComment( string::const_iterator & it, const string::const_iterator & end, StringList * comments )
{
  if(it == end || *it != ';')
    return false;

  // skip the ';'
  it++;

  if(comments)
  {
    string comment;
    while(it != end && *it != '\n')
      comment.push_back(*it++);
    comments->push_back(comment);
  }
  else
    while(it != end && *it != '\n')
      it++;

  return it != end;
}

/**
 * This method extracts a token from the given position of a string.
 * @param it Current position of the string
 * @param end End of the string
 * @param tokens Tokens to add the extracted token to
 * @return True if no errors occured, false if any errors occured
 */
bool SLangParser::extractSExpToken( string::const_iterator & it, const string::const_iterator & end, StringList & tokens )
{
  if(it == end)
    return false;

  string token;

  // if the token is not begining with a '(' then it can be extracted very easily
  // Just go on in the string till you reach a space or end of the string
  if(*it != '(')
  {
    while(it != end && !isspace(*it) && *it != ')' && *it != '(' && *it != ';')
      token.push_back(*it++);
    tokens.push_back(token);

    return it != end;
  }
  else                  // the begining charactor of the string is a '(', so
  {                     // the token shall be parsed till the matching ')'
    int p = 0;
    do{
      if(*it == '(')
        p++;
      else if(*it == ')')
        p--;

      // record the charactor and advance the iterator
      token.push_back(*it++);
    } while(p > 0 && it != end);

    tokens.push_back(token);
    return it != end;
  }
}

/**
 * This method extracts all elements residing in an S-Expression string
 * @param str String to be parsed for S-Expression
 * @param tokens Everything extracted from string
 * @param comments Comments residing in the S-Expresion
 * @return Number of elements extracted
 */
bool SLangParser::parseSExpression( const string & str, StringList &tokens, StringList * comments )
{
  tokens.clear();
  if( comments )
    comments->clear();

  string::const_iterator it = str.begin();
  const string::const_iterator end = str.end();
  bool expression_started = false;

  while(true)
  {
    if(!gotoFirstNonSpace(it, end))
      return false;

    switch(*it)
    {
      case ';': // parse comments. if comment holder was not specified
                // then just go on with the string
        extractSExpComment(it, end, comments);
        break; // for switch, not for the while

      case ')': // It shall be the end of S-Exp expression. Since the
                // extractSExpToken extracts the tokens which begin
                // with a ( to their last ) then this shall only be
                // the end of the S-Exp expression. If not then the
                // expression was faulty.
        if(!expression_started)
          return false;
        else
          return true;

      case '(': // could be the S-Exp's starting paranthesis or just a paranthesis.
                // if it was the starting paranthesis the skip it
                // if it was a normal one then it is starting an expression
                // which shall be extractes as a whole
        if(!expression_started)
        {
          expression_started = true;
          it++;
          break;
        }
        // else continues to default section right below automatically

      default:  // All other alpha numerical expressions are considered tokens
         if(!extractSExpToken(it, end, tokens))
           return false;

    }; // end of(switch(*it))

  } // end of while(true)

  // end of function. Never reaches here though
  return false;
}

/**
 * This method is used to open a file and store all its contents into a given string
 * @param strFileName FileName to open and store its contents
 * @param strFile String to dump the file into it
 * @return True if operation finished successfull, false otherwise
 */
bool SLangParser::loadFile( const string & strFileName, string & strFile )
{
  ifstream file(strFileName.c_str());
  if( !file )
    return false;

  char strBuf[4096];

  while( !file.eof() )
  {
    file.getline(strBuf, 4096);
    strFile.append(strBuf);
    strFile.push_back('\n');
  }

  file.close();
  return true;
}

/**
 * This method returns the contents of the provided folder
 * @param strFolder Folder to get it's contents
 * @param contents List to write the contents to
 * @return Number of items in the folder
 */
int SLangParser::getFolderContents( const string & strFolder, StringList & contents )
{
  contents.clear();

#ifdef WIN32

  struct _finddata_t file;
  long hFile;

  // Find first file in current directory
  if( (hFile = _findfirst( (strFolder+"\\*").c_str(), &file )) == -1L )
    return 0;

  int number = 0;
  do
  {
    //skip if find . and ..
    if((strcmp(file.name, ".") == 0 ||  strcmp(file.name, "..") == 0))
      continue;

    contents.push_back(file.name);
    number++;
  } while(_findnext( hFile, &file ) == 0);

  _findclose( hFile );

  return number;

#else

  struct dirent **namelist;

  int num = scandir(strFolder.c_str(), &namelist, 0, alphasort), number;
  number = num;

  if(num >= 0)
  {
    while(num--)
    {
      //skip if find . and ..
      if((strcmp(namelist[num]->d_name, ".") == 0 ||  strcmp(namelist[num]->d_name, "..") == 0))
        continue;

      contents.push_back( namelist[num]->d_name );
      free(namelist[num]);
    }

    free(namelist);
  }

  return number;

#endif
}

/**
 * This method checks whether a file with the given name exists or not
 * @param strFileName name of the file to check
 * @return True if the file exists, false otherwise
 */
bool SLangParser::fileExists(string strFileName)
{
  struct stat stFileInfo;

  // Attempt to get the file attributes
  if(stat(strFileName.c_str(), &stFileInfo) == 0)
  {
    // We were able to get the file attributes
    // so the file obviously exists.
    return true;
  }
  else
  {
    // We were not able to get the file attributes.
    // This may mean that we don't have permission to
    // access the folder which contains this file. If you
    // need to do that level of checking, lookup the
    // return values of stat which will give you
    // more details on why stat failed.
    return false;
  }
}

/*******************************************************************************/
/*******************************  GenericData  ********************************/
/*******************************************************************************/

const GenericData GenericData::unknown("NaN");

/**
 * This is the constructor of GenericData, initializes the internals
 */
GenericData::GenericData()
{
  mDataType = DT_ILLEGAL;
}

/**
 * This is the constructor of GenericData, initializes the internals
 * @param value value to initialize tha data with
 */
GenericData::GenericData( const double & value )
{
  setValue(value);
}

/**
 * This is the constructor of GenericData, initializes the internals
 * @param value value to initialize tha data with
 */
GenericData::GenericData( const string & value )
{
  setValue(value);
}

/**
 * This is the destructor of GenericData, deinitializes the storage
 */
GenericData::~GenericData()
{
}

/**
 * This method returns the type of data currently residing in storage
 * @return type of the current data
 */
DataT GenericData::getDataType() const
{
  return mDataType;
}

/**
 * This method sets the data type of the data
 * @param type Type of the data
 */
void GenericData::setDataType( const DataT & type )
{
  mDataType = type;
}

/**
 * This method returns the name of the data stored
 * @return The name of the data stored
 */
string GenericData::getName() const
{
  return mDataName;
}

/**
 * This method sets the name of the data stored in storage
 * @param name The name of the data stored in storage
 */
void GenericData::setName( const string & name )
{
  mDataName = name;
}

/**
 * This method returns the value of the data stored as a string
 * @return Value of the data
 */
string GenericData::getAsString() const
{
  return mDataValue;
}

/**
 * This method sets the value of data as a double
 * @return Value of the data
 */
double GenericData::getAsDouble() const
{
  if( mDataValue == "NaN" )
    return -1000.0;
  else
    return SLangParser::parseFirstDouble( mDataValue );
}

/**
 * This method returns the value of data, this is renamed copy of getAsString() function
 * @return Value of the data
 */
string GenericData::getValue() const
{
  return mDataValue;
}

/**
 * This method sets the value of data stored to the given one
 * @param value New value for the data
 */
void GenericData::setValue( const string & value )
{
  mDataValue = value;
  mDataType = DT_STRING;
}

/**
 * This method sets the value of data to the given one
 * @param value New value for data
 */
void GenericData::setValue( const double & value )
{
  char s[128];
  sprintf(s, "%f", value);
  mDataValue = s;
  mDataType = DT_NUMBER;
}

/**
 * This method sets the value of the data to the given data and type
 * @param data Data to copy the values from
 */
void GenericData::setValue(const GenericData & data)
{
  mDataType = data.mDataType;
  mDataValue = data.mDataValue;
}

/**
 * This operator copies the information on paramter instance given to this instance
 * @param data Data to be copied on self
 * @return GenericData instance to allow furthor copies
 */
GenericData & GenericData::operator = ( const GenericData & data )
{
  mDataName = data.mDataName;
  mDataValue = data.mDataValue;
  mDataType = data.mDataType;

  return *this;
}

/**
 * This method calculates the sum of the value of another storage to this storage and returns self
 * @param data Data for the sum to be calculated to current storage
 * @return current stoage to enable other uses
 */
GenericData GenericData::operator + ( const GenericData & data )
{
  GenericData result = *this;

  if( mDataType == DT_STRING )
    result.setValue(mDataValue + data.mDataValue);
  else if( mDataType == DT_NUMBER )
    result.setValue(getAsDouble() + data.getAsDouble());

  return result;
}

/**
 * This method adds the value of another storage to this storage and returns self
 * @param data Data to be added to current storage
 * @return current stoage to enable other uses
 */
GenericData & GenericData::operator += ( const GenericData & data )
{
  (*this) = (*this) + data;

  return (*this);
}

/**
 * This method calculates the subtract of the value of another storage to this storage and returns self
 * @param data Data for the sum to be calculated to current storage
 * @return current stoage to enable other uses
 */
GenericData GenericData::operator - ( const GenericData & data )
{
  GenericData result;
  result.mDataName = mDataName;
  result.mDataType = mDataType;

  if( mDataType != DT_NUMBER )
    result.mDataValue = "0";
  else
    result.setValue(getAsDouble() - data.getAsDouble());

  return result;
}

/**
 * This method subtracts the value of another storage to this storage and returns self
 * @param data Data to be added to current storage
 * @return current stoage to enable other uses
 */
GenericData & GenericData::operator -= ( const GenericData & data )
{
  return (*this = *this - data);
}

/**
 * This method calculates the multiply of the value of another storage to this storage and returns self
 * @param data Data for the sum to be calculated to current storage
 * @return current stoage to enable other uses
 */
GenericData GenericData::operator * ( const GenericData & data )
{
  GenericData result;
  result.mDataName = mDataName;
  result.mDataType = mDataType;

  if( mDataType != DT_NUMBER )
    result.mDataValue = "0";
  else
    result.setValue(getAsDouble() * data.getAsDouble());

  return result;
}

/**
 * This method multiplies the value of another storage to this storage and returns self
 * @param data Data to be added to current storage
 * @return current stoage to enable other uses
 */
GenericData & GenericData::operator *= ( const GenericData & data )
{
  return (*this = *this * data);
}

/**
 * This method calculates the division of the value of another storage to this storage and returns self
 * @param data Data for the sum to be calculated to current storage
 * @return current stoage to enable other uses
 */
GenericData GenericData::operator / ( const GenericData & data )
{
  GenericData result;
  result.mDataName = mDataName;
  result.mDataType = mDataType;

  if( mDataType != DT_NUMBER )
    result.mDataValue = "0";
  else
    result.setValue(getAsDouble() / data.getAsDouble());

  return result;
}

/**
 * This method divides the value of another storage to this storage and returns self
 * @param data Data to be added to current storage
 * @return current stoage to enable other uses
 */
GenericData & GenericData::operator /= ( const GenericData & data )
{
  return (*this = *this / data);
}

/**
 * Overloaded version of equality operator.
 * @param data Data to check if it's value is equal to this
 * @return True if the values are equal, false otherwise or if the types are not the same
 */
bool GenericData::operator == (const GenericData & data)
{
  if(data.mDataType != mDataType)
    return false;

  return mDataValue == data.mDataValue;
}

/**
 * Overloaded version of equality operator for comparing to a string.
 * @param str String to check if it's value is equal to this
 * @return True if the values are equal, false otherwise or if the data type is not string
 */
bool GenericData::operator == (const string & str)
{
  if(mDataType != DT_STRING)
    return false;

  return mDataValue == str;
}

/**
 * Overloaded version of equality operator for comparing to a double.
 * @param val double value to check if it's value is equal to this
 * @return True if the values are equal, false otherwise or if the data type is not double
 */
bool GenericData::operator == (const double & val)
{
  if(mDataType != DT_NUMBER)
    return false;

  return getAsDouble() == val;
}

/**
 * Overloaded version of the C++ output operator for GenericDatas.
 * This operator makes it possible to use GenericDatas in output
 * statements for output streams. The data name and values are
 * printed in the format '(name,value)'.
 * @param os Output stream which data is being printed
 * @param data Data which is being printed
 * @return Output stream instance to allow furthor prints
 */
ostream& operator <<( ostream &os, const GenericData & data )
{
  os << "('" << data.mDataName << "', " << data.mDataValue << " ";
  if(data.mDataType == DT_NUMBER)
    os << "number";
  else if(data.mDataType == DT_STRING)
    os << "string";
  else
    os << "unknown";

  return (os << ")");
}

/**
 * Overloaded version of the C++ output operator for GenericDatas.
 * This operator makes it possible to use GenericDatas in output
 * statements for output streams. The data name and values are
 * printed in the format '(name,value)'.
 * @param os Output stream which data is being printed
 * @param data Pointer to data which is being printed
 * @return Output stream instance to allow furthor prints
 */
ostream& operator <<( ostream &os, const GenericData * data )
{
  return (os << (*data));
}



/************************************************************************************/
/********************************  Class DataList  ********************************/
/************************************************************************************/

/**
 * Constructor of DataList. Currently does nothing
 */
DataList::DataList()
{
}

/**
 * Destructor of the DataList. Clears the data
 */
DataList::~DataList()
{
  clear();
}

/**
 * This method adds a datum
 * @param datum Datum to be added to the data
 */
void DataList::addData( const GenericData & datum )
{
  GenericData* d = new GenericData;
  *d = datum;
  mData.push_back(d);
}

/**
 * This method clears the data from the holder.
 */
void DataList::clear()
{
  for(size_t i = 0 ; i < mData.size(); i++)
    delete mData[i];

  mData.clear();
}

/**
 * This method searches this node and upper nodes for data by the name given. If the
   data does not exist, then a NULL pointer will be returned.
 * @param strName Data Name
 * @return Data by the name requested
 */
GenericData* DataList::findData( const string & strName )
{
  for(size_t i = 0; i < mData.size(); i++)
    if(mData[i]->getName() == strName)
      return mData[i];

  return NULL;
}

/**
 * This method searches this node and upper nodes for data by the name given. If the
   data does not exist, then a NULL pointer will be returned.
 * @param strName Data Name
 * @return Data by the name requested
 */
const GenericData* DataList::findData( const string & strName ) const
{
  for(size_t i = 0; i < mData.size(); i++)
    if(mData[i]->getName() == strName)
      return mData[i];

  return NULL;
}

/**
 * This method searches this node and upper nodes for data by the name given. If the data
   does not exist, then a data with the specified name will be created and returned.
 * @param strName Data Name
 * @return Data by the name requested
 */
GenericData * DataList::getData( const string & strName )
{
  for(size_t i = 0; i < mData.size(); i++)
    if(mData[i]->getName() == strName)
      return mData[i];

  GenericData* d = new GenericData;
  d->setName(strName);
  mData.push_back(d);
  return d;
}

/**
 * This methods adds a double value to list
 * @param d Double value to addData
 * @param strName Name of the data
 */
void DataList::addValue(const double& d, const std::string & strName)
{
  GenericData * datum = new GenericData;
  datum->setValue(d);
  datum->setName(strName);
  mData.push_back(datum);
}

/**
 * This method adds a string value to list
 * @param str The string value to add
 * @param strName Name of the data
 */
void DataList::addValue(const std::string& str, const std::string & strName)
{
  GenericData * datum = new GenericData;
  datum->setValue(str);
  datum->setName(strName);
  mData.push_back(datum);
}

/**
 * Reference index operator for accessing data
 * @param index Index of the data needed
 * @return Data in the given index
 */
GenericData* DataList::operator[](const size_t& index)
{
  if(index >= mData.size())
    return NULL;
  else
    return mData[index];
}

/**
 * const index operator for accessing data
 * @param index Index of the data needed
 * @return Data in the given index
 */
const GenericData* DataList::operator[](const size_t& index) const
{
  if(index >= mData.size())
    return NULL;
  else
    return mData[index];
}

/**
 * Reference index operator for accessing data
 * @param strIndex Name of the data needed
 * @return Data in the given index
 */
GenericData* DataList::operator[](const string & strIndex)
{
  return findData(strIndex);
}

/**
 * const index operator for accessing data
 * @param strIndex Name of the data needed
 * @return Data in the given index
 */
const GenericData* DataList::operator[](const string & strIndex) const
{
  return findData(strIndex);
}

/**
 * Inline size method for getting number of currently stored data.
 */
size_t DataList::size() const
{
  return mData.size();
}

/**
 * This is overloaded version of assignment operator to copy data a holder to antoher one
 * @param data Data to be copied is in this argument
 * @return self instance to enable rescursion
 */
DataList & DataList::operator = ( const DataList & data)
{
  clear();
  for(size_t i = 0; i < data.mData.size(); i++)
    addData(*(data.mData[i]));

  return *this;
}

