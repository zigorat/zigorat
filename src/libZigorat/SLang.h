/*
*********************************************************************************
*              SLang.h : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 05/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Provides utilities for parsing and using S-Expressions.            *
*                                                                               *
*********************************************************************************
*/

/*! \file SLang.h
<pre>
<b>File:</b>          SLang.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       05/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Provides utilities for parsing and using S-Expressions.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
05/02/2011       Mahdi           Initial version created
</pre>
*/

#ifndef S_LANGUAGE
#define S_LANGUAGE

#include <string>      // needed for string
#include <vector>      // needed for vector
#include <iostream>    // needed for istream, ostream
#include "Exception.h" // needed for Klaus::Exception


#if COMPILER == COMPILER_MSVC
  // needed to disable warning for STL classes export conditions under windows
  #pragma warning (disable : 4251)
#endif 


namespace Klaus
{

  /*! This class manages S-Lang tokens. Tokens are divided into four total groups and
      parsed according to their group. This class also provides methods to efficiently
      find a child node by giving the child's first token name (or in many cases the
      command name of the child)
  */
  class _DLLExportControl SExpression
  {
    /*! This enumeration holds all different catagories of S-Language tokens, which
        includes a comment token, an atom (undividable) token and normal(dividable)
        token.
    */
    public:
      enum TokenType
      {
        Comment,  /*!< A comment token */
        Atom,     /*!< Atom token (undividable token) */
        Normal,   /*!< Normal token (dividable token) */
        Root,     /*!< Root token (top most token) */
        Unknown   /*!< Unknown token catagory */
      };

    private:
      /*! List is a dynamic array of S-Lang tokens */
      typedef std::vector<SExpression> List;

      static SExpression  InvalidExpression; /*!< Invalid expression */

      std::string       * mSExpStr;         /*!< The string containing the S-Expression */
      List                mSubTokens;       /*!< All sub tokens */
      size_t              mBegin;           /*!< start point of data for the node on the string */
      size_t              mEnd;             /*!< end point of data for the node on the string */
      size_t              mSize;            /*!< Total size of the string */
      TokenType           mType;            /*!< Type of the token */

      bool                gotoFirstNonSpace (                              );
      bool                getAsInt          ( size_t & index, int & result ) const;

      size_t              parse             ( std::string * str, int begin );
      size_t              parseString       (                              );
      size_t              parsePureString   (                              );
      size_t              parseComment      (                              );
      size_t              parseHolderNode   (                              );

    public:
                          SExpression       (                              );
                          SExpression       ( const std::string & str      );
                          SExpression       ( std::istream & is,
                                              const size_t & buf_size      );
                          SExpression       ( const std::string & name,
                                              const size_t & buf_size      );
                          SExpression       ( const SExpression & expr     );
                        ~ SExpression       (                              );

      int                 getAsInt          (                              ) const;
      double              getAsDouble       (                              ) const;
      std::string         getAsStdString    (                              ) const;

      TokenType           getType           (                              ) const;
      size_t              getStartingIndex  (                              ) const;

      void                clear             (                              );
      void                parse             ( const std::string & str      );
      void                parse             ( std::istream & is,
                                              const size_t & buf_size      );
      void                parseFile         ( const std::string & name,
                                              const size_t & buf_size      );

      // full name access methods
      size_t              getSubTokenCount  (                              );
      const SExpression & getSubToken       ( const size_t & index         ) const;
      const SExpression & getSubTokenByName ( const std::string & str      ) const;

      // now short name access methods!
      size_t              size              (                              ) const;
      bool                empty             (                              ) const;
      double              number            (                              ) const;
      std::string         str               (                              ) const;
      bool                boolean           (                              ) const;

      // type checking
      bool                isRoot            (                              ) const;
      bool                isAtom            (                              ) const;
      bool                isNormal          (                              ) const;
      bool                isComment         (                              ) const;

      // next item
      int                 getFirstNonComment(                              ) const;
      int                 getNextNonComment ( const int & inum             ) const;

      // operator overloads
      const SExpression & operator []       ( const size_t & index         ) const;
      const SExpression & operator []       ( const std::string & strName  ) const;
      bool                operator !        (                              ) const;
      bool                operator ==       ( const std::string & str      ) const;
      bool                operator ==       ( const double & number        ) const;
      bool                operator !=       ( const std::string & str      ) const;
      bool                operator !=       ( const double & number        ) const;

      operator            std::string       (                              ) const;
      operator            double            (                              ) const;

      /**
        Overloaded stream operator for printing contents of the token into a stream
        @param os The output stream to write token contents
        @param token The token to write it's content
        @return The given output stream instance for recursive streaming
      */
      friend std::ostream & operator <<  ( std::ostream & os,
                                           const SExpression & token      );
      /**
        Overloaded stream operator for printing contents of the token into a stream
        @param os The output stream to write token contents
        @param token The token to write it's content
        @return The given output stream instance for recursive streaming
      */
      friend std::ostream & operator <<  ( std::ostream & os,
                                           const SExpression * token      );

  }; // end class SExpression

  /*!< A very handy type definition for shortening token type name */
  typedef const SExpression & SToken;

};  // end namespace Klaus

#endif // S_LANGUAGE
