/*
*********************************************************************************
*           BinaryStream.h : Robocup 3D Soccer Simulation Team Zigorat          *
*                                                                               *
*  Date: 01/27/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Utilities for parsing binary data from standard data streams.      *
*                                                                               *
*********************************************************************************
*/

/*! \file BinaryStream.h
<pre>
<b>File:</b>          BinaryStream.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/27/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Utilities for parsing binary data from standard data streams.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/27/2011       Mahdi           Initial version created
</pre>
*/

#ifndef BINARY_PARSER
#define BINARY_PARSER


#include <string>
#include <iostream>

namespace Klaus
{

  /*! This class is used to read binary data from standard streams
      and/or save them. The data that can be read and saved are
      standard types including boolean, integer, double, unsigned
      and string.
  */
  class BinaryStream
  {
    public:
        // boolean type
        static bool        readBool     ( std::istream & is       );
        static void        writeBool    ( std::ostream & os,
                                          const bool & var        );

        // integer type
        static int         readInt      ( std::istream & is       );
        static void        writeInt     ( std::ostream & os,
                                          const int & var         );

        // unsigned type
        static unsigned    readUnsigned ( std::istream & is       );
        static void        writeUnsigned( std::ostream & os,
                                          const unsigned & var    );

        // double type
        static double      readDouble   ( std::istream & is       );
        static void        writeDouble  ( std::ostream & os,
                                          const double & var      );

        // string type
        static std::string readString   ( std::istream & is       );
        static void        writeString  ( std::ostream & os,
                                          const std::string & var );

  }; // end class BinaryStream

}; // end namespace Klaus

#endif // BINARY_PARSER
