/*
*********************************************************************************
*           SLangParser.h : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 05/01/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: A parser for S-Language                                            *
*                                                                               *
*********************************************************************************
*/

/*! \file SLangParser.h
<pre>
<b>File:</b>          SLangParser.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       05/01/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      A parser for S-Language
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
05/01/2008       Mahdi           Initial version created
</pre>
*/

#ifndef SLANG_PARSER
#define SLANG_PARSER

#include <string>
#include <vector>
#include <fstream>
#include "PlatformMacros.h"

#if COMPILER == COMPILER_MSVC
#  pragma warning (disable : 4251)
#endif

using namespace std;

/*! StringList represents a list of strings */
typedef std::vector<std::string> StringList;

/*! DataT is an enumeration of all possible types of storages
    that  can be used in  this framework. Currently  numbers and
    strings are supported */
enum DataT
{
  DT_NUMBER,   /*!< Represents a number as data  */
  DT_STRING,   /*!< Represents a text as data    */
  DT_ILLEGAL   /*!< Represents an illegal data   */
};


/*******************************************************************************/
/*****************************  Class SLangParser  *****************************/
/*******************************************************************************/

/*! This class contains several static methods to parse S-Expression modules.
    In the current simulator, S-Expressions are used widely, from the lowest
    server-agent communication to the levels of agent creation modules.
    These contain the RubySceneGraph(RSG) which is used to define agent's
    body, which is greatly needed for agent localization, worldmodel, and
    future state prediction. */
class _DLLExportControl SLangParser
{
  public:
    // methods that deal with S-Expression string parsing
    static double       parseFirstDouble         ( const string & strMsg                         );
    static int          parseFirstInt            ( const string & strMsg, const int & ind = 0    );
    static string       parseFirstWord           ( const string & str, unsigned * index = NULL   );
    static string       parseFirstQuote          ( const string & str, unsigned * index = NULL   );
    static string       trim                     ( const string & str                            );
    static bool         parseByCharDelim         ( string str,
                                                   const char & delim,
                                                   StringList & tokens                           );

    static char         gotoFirstNonSpace        ( string::const_iterator & it,
                                                   const string::const_iterator & end            );
    static bool         extractSExpComment       ( string::const_iterator & it,
                                                   const string::const_iterator & end,
                                                   StringList * comments                         );
    static bool         extractSExpToken         ( string::const_iterator & it,
                                                   const string::const_iterator & end,
                                                   StringList & tokens                           );
    static bool         parseSExpression         ( const string & str,
                                                   StringList & tokens,
                                                   StringList * comments = NULL                  );

    static bool         loadFile                 ( const string & strFileName,
                                                   string & strFile                              );
    static int          getFolderContents        ( const string & strFolder,
                                                   StringList & contents                         );
    static bool         fileExists               ( string strFileName                            );
};




/********************************************************************************************/
/************************************  GenericData  ****************************************/
/********************************************************************************************/

/*! GenericData is a class that enables to have a data with a
    variable data type, so that a data which is saved, can be
    accessed as different types */
class _DLLExportControl GenericData
{
  protected:
    DataT            mDataType;     /*!< Current type of data  */
    string           mDataName;     /*!< Name of the data      */
    string           mDataValue;    /*!< Value of the data     */

  public:
                     GenericData    (                          );
                     GenericData    ( const double & value     );
                     GenericData    ( const string & string    );

    virtual        ~ GenericData    (                          );

    DataT            getDataType    (                          ) const;
    void             setDataType    ( const DataT & type       );

    string           getName        (                          ) const;
    void             setName        ( const string & name      );

    string           getAsString    (                          ) const;
    double           getAsDouble    (                          ) const;
    string           getValue       (                          ) const;

    void             setValue       ( const string & value     );
    void             setValue       ( const double & value     );
    void             setValue       ( const GenericData & data );

    GenericData    & operator =     ( const GenericData & data );

    GenericData      operator +     ( const GenericData & data );
    GenericData    & operator +=    ( const GenericData & data );

    GenericData      operator -     ( const GenericData & data );
    GenericData    & operator -=    ( const GenericData & data );

    GenericData      operator *     ( const GenericData & data );
    GenericData    & operator *=    ( const GenericData & data );

    GenericData      operator /     ( const GenericData & data );
    GenericData    & operator /=    ( const GenericData & data );

    bool             operator ==    ( const GenericData & data );
    bool             operator ==    ( const string & str       );
    bool             operator ==    ( const double & val       );

    /**
     * Overloaded version of the C++ output operator for GenericDatas.
     * This operator makes it possible to use GenericDatas in output
     * statements for output streams. The data name and values are
     * printed in the format '(name,value)'.
     * @param os Output stream which data is being printed
     * @param data Data which is being printed
     * @return Output stream instance to allow furthor prints
     */
    friend ostream & operator <<    ( ostream &os, const GenericData & data );

    /**
     * Overloaded version of the C++ output operator for GenericDatas.
     * This operator makes it possible to use GenericDatas in output
     * statements for output streams. The data name and values are
     * printed in the format '(name,value)'.
     * @param os Output stream which data is being printed
     * @param data Pointer to data which is being printed
     * @return Output stream instance to allow furthor prints
     */
    friend ostream & operator <<    ( ostream &os, const GenericData * data );

    /*!< Decleration of Unknown Data */
    static const GenericData unknown;

    /*!< DataList is a dynamic array of data */
    typedef vector<GenericData*> List;
};




/************************************************************************************/
/********************************  Class DataHolder  ********************************/
/************************************************************************************/

/*! DataHolder class is a storage place for any type of data. Can search for a reference
    to a datum by name and create new data placeholders. */
class _DLLExportControl DataList
{
  private:
    void                addData     ( const GenericData & datum  );

  public:
    GenericData::List   mData;      /*!< data to store and use   */

                        DataList    (                            );
                      ~ DataList    (                            );

    void                clear       (                            );
    GenericData       * findData    ( const string & strName     );
    const GenericData * findData    ( const string & strName     ) const;
    GenericData       * getData     ( const string & strName     );

    void                addValue    ( const double & d,
                                      const string & strName = "");
    void                addValue    ( const string & str,
                                      const string & strName = "");
    GenericData       * operator [] ( const size_t & index       );
    const GenericData * operator [] ( const size_t & index       ) const;

    GenericData       * operator [] ( const string & strIndex    );
    const GenericData * operator [] ( const string & strIndex    ) const;

    size_t              size        (                            ) const;

    DataList          & operator =  ( const DataList & data      );
};


#endif //SLANG_PARSER
