/*
*********************************************************************************
*           NodeFactory.h : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 10/09/2009                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for NodeFactoryCore,          *
*            BaseNodeFactory and MainNodeFactory. BaseNodeFactory is a factory  *
*            class used to create appropriate instance of node parsers.         *
*            MainNodeFactory is the factory which creates parser instances      *
*            for regular simspark nodes and the NodeFactoryCore manages         *
*            registering and using scene node factories.                        *
*                                                                               *
*********************************************************************************
*/

/*! \file NodeFactory.h
<pre>
<b>File:</b>          NodeFactory.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       10/09/2009
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for NodeFactoryCore,
               BaseNodeFactory and MainNodeFactory. BaseNodeFactory is a factory
               class used to create appropriate instance of node parsers.
               MainNodeFactory is the factory which creates parser instances
               for regular simspark nodes and the NodeFactoryCore manages
               registering and using scene node factories.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
10/09/2009       Mahdi           Initial version created
</pre>
*/

#ifndef SCENE_GRAPH_NODE_FACTORY
#define SCENE_GRAPH_NODE_FACTORY

#include "Node.h"  // needed for Node


namespace Klaus
{

  namespace RSG2
  {

    /*! Base of all node factories. Each factory shall only implement
        the createParser method.
    */
    class _DLLExportControl BaseNodeFactory
    {
      private:
        std::string mName; /*!< Name of the factory */

      public:
        /**
          Constructor of the NodeFactoryCore. Sets the factory name
          @param strName Name of the factory
        */
        BaseNodeFactory(const std::string & strName)
        {
          mName = strName;
        }

        /**
          This method returns the name of the factory.
          @return Name of the factory
        */
        const std::string & getName() const
        {
          return mName;
        }

        /**
          This method is reponsible to create a parser for the given node name.
          If the method can not find a suitable parser, NULL shall be returned
          so that NodeFactoryCore can fallback to another NodeFactory.
          @param strNode The node to create a parser for
        */
        virtual Node* createParser(const std::string & strNode) = 0;


        /*! Array is a dynamic array of NodeFactoryCores*/
        typedef std::vector<BaseNodeFactory*> Array;
    };


    /*! The used factory for creating regular simspark node parser. */
    class _DLLExportControl MainNodeFactory : public BaseNodeFactory
    {
      public:
                      MainNodeFactory(                             );
        virtual Node* createParser   ( const std::string & strNode );
    };


    /*! Main point for getting a parser for a specific type of node
    */
    class _DLLExportControl NodeFactoryCore : public Klaus::Singleton<NodeFactoryCore>
    {
      private:
        BaseNodeFactory::Array   mFactories; /*!< Node fctory instances */

                                 NodeFactoryCore   (                             );
                               ~ NodeFactoryCore   (                             );

      public:
        static NodeFactoryCore * getSingleton      (                             );
        static void              freeSingleton     (                             );

        void                     registerFactory   ( BaseNodeFactory * factory   );
        bool                     unregisterFactory ( BaseNodeFactory * factory   );
        void                     unregisterAll     (                             );

        Node                   * createInstanceFor ( const std::string & strNode );
    };


  }; // end namespace RSG2

}; // end namespace Klaus


#endif // SCENE_GRAPH_NODE_FACTORY
