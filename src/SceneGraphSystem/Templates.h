/*
*********************************************************************************
*           Templates.h : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 21/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class definitions for SceneTemplate and         *
*            TemplateFactory which implement templates in simulator's scene     *
*            graphs. Templates ease the process of creating robots by creating  *
*            C-function like implementations of various robot parts             *
*                                                                               *
*********************************************************************************
*/

/*! \file Templates.h
<pre>
<b>File:</b>          Templates.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       21/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class definition for SceneTemplate and
               TemplateFactory which implement templates in simulator's scene
               graphs. Templates ease the process of creating robots by creating
               C-function like implementations of various robot parts
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
21/02/2011       Mahdi           Initial version created
</pre>
*/

#ifndef RSG_SCENE_TEMPLATES
#define RSG_SCENE_TEMPLATES


#include "Node.h"  // needed for Node
#include <map>     // needed for map

namespace Klaus
{

  namespace RSG2
  {

    /*! Template manages templates ruby scene graphs. Templates are more or less like preprocessor
        definitions in C/C++ language with the difference that they are defined in a seperate file.
        When they are called, they will put what ever they have in the calling node regarding their
        assigned parameter values.
        A template command like '(importScene rsg/boxspheres/box.rsg 0.5 0.5 1.0 1.0 matGrey)'
        will put every node and command in the rsg file 'rsg/boxspheres/box.rsg' in the calling
        node, and sets the supplied values as the parameters in the template itself. (Which in
        this case they are `lenX, lenY, lenZ, totalMass, material`)
    */
    class _DLLExportControl Template
    {
      private:
        std::string   mName;           /*!< Name of the template - it's path */
        SExpression   mTokens;         /*!< Commands and nodes of the template */
        int           mMinorVersion;   /*!< Minor version of the template */
        int           mMajorVersion;   /*!< Major version of the template */
        size_t        mBodyStartToken; /*!< First body token's index */
        size_t        mBodyIndex;      /*!< Index of the template's body */
        StringArray   mParameterNames; /*!< Names of the template's parameters */
        NodeConfig  * mConfig;         /*!< Configuration options for nodes */

        void          error            ( const char* str, ...     ) const;

        bool          parseBody        (                          );
        bool          parseVariable    ( const std::string & name,
                                         SToken def,
                                         Node * node              );

      public:
                      Template         (                          );
                    ~ Template         (                          );

        std::string   getName          (                          ) const;
        int           getMajorVersion  (                          ) const;
        int           getMinorVersion  (                          ) const;

        bool          loadTemplate     ( const std::string & name );
        bool          inject           ( Node * nd, SToken cmd    );

        /*! List is a dynamic array of mapping between template names and their instances */
        typedef std::map<std::string, Template*> List;
    };


    /*! TemplateManager is the factory used to create scene graph templates. This
        factory is a singleton (since there is no need (and never should be) to
        have different factories for scene templates). This factory has only one
        useful method: getTemplate. This method returns an instance of a requested
        template. If the template does not exist then it will be created but if it
        was previously created, then only it's instance will be returned.
    */
    class _DLLExportControl TemplateManager : public Singleton<TemplateManager>
    {
      private:
        Template::List           mTemplates;    /*!< The list of templates */

                                 TemplateManager(                          );
                               ~ TemplateManager(                          );
      public:
        static TemplateManager * getSingleton   (                          );
        static void              freeSingleton  (                          );

       Template                * getTemplate    ( const std::string & name );
    };

  }; // end namespace RSG2

}; // end namespace Klaus


#endif // RSG_SCENE_TEMPLATES

