/*
*********************************************************************************
*         PhysicalNodes.h : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 15/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for physical scene nodes.     *
*            These nodes provide various utilities for physic related nodes     *
*            like transform and colliders.                                      *
*                                                                               *
*********************************************************************************
*/

/*! \file PhysicalNodes.h
<pre>
<b>File:</b>          PhysicalNodes.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       15/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for physical scene nodes.
               These nodes provide various utilities for physic related nodes
               like transform and colliders.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
15/02/2011       Mahdi           Initial version created
</pre>
*/

#ifndef PHYSIC_SCENE_NODES
#define PHYSIC_SCENE_NODES

#include "Node.h"                  // needed for GraphicalNode
#include <AgentBase/Kinematics.h>  // needed for Kinematics

namespace Klaus
{

  namespace RSG2
  {
    class Joint;

    /*! This class is the wrapper for for Space nodes. These nodes create
        seperate spaces in physic engines to accelerate physical calculations.
    */
    class _DLLExportControl Space : public TransformableNode
    {
      private:
        BooleanVar mInnerCollision; /*!< Disable inner collisions in the space? */

      protected:
        bool       parseRSGCommand  ( SToken cmd );

      public:
                   Space            (            );
    };


    /*! A pure transformable node. Holds transformational attributes. */
    class _DLLExportControl Transform : public TransformableNode
    {
    };


    /*! Another pure transformable node. */
    class _DLLExportControl AgentAspect : public TransformableNode
    {
    };


    /*! A physical body having its own geometry and mass. It is attached to
        transformable nodes to translate. Forward kinematics is calculated
        using the body node. First all bodies are repositioned and then
        their transformations are moved to their parent transform nodes.
    */
    class _DLLExportControl Body : public Node
    {
      public:
        /*! Array is a dynamic array of bodies. */
        typedef std::vector<Body*> Array;

      private:
        TransformableNode * mBodyTransform;          /*!< Parent transformable node */

        void                setParentTransform       ( const Matrix4 & tr              );
        void                getForwardKinematics     ( const double & time_gap,
                                                       Kinematics & body_sheet         );
        void                getForwardKinematicsFor  ( const InverseKinematics & angles,
                                                       Kinematics & body_sheet         );

      protected:
        double              mMass;                   /*!< Mass of the body */
        double              mTotalMass;              /*!< Mass including the children */
        std::string         mBodyType;               /*!< Type of the body, ie. box sphere or an arbitrary shape */
        VecPosition         mExtents;                /*!< Extents of the body shape */
        Body              * mBodyParent;             /*!< Body parent of this body */
        Array               mBodyChildren;           /*!< Body children of the body */
        Joint             * mJoint;                  /*!< Joint instance to get it's transformation  */
        VecPosition         mPivot;                  /*!< Pivot of the joint, attaching the two bodies. Relative to the first */
        VecPosition         mPosRelPivot;            /*!< Position of this body relative to the pivot point */

        virtual bool        parseRSGCommand          ( SToken cmd                      );
        virtual void        postLoadInternal         (                                 );

      public:
                            Body                     (                                 );
                          ~ Body                     (                                 );

        double              getMass                  ( bool bRecurse = false           );

        std::string         getBodyName              (                                 ) const;
        void                getForwardKinematics     ( const Matrix4 & tr,
                                                       const double & time_gap,
                                                       Kinematics & body_sheet         );
        void                getForwardKinematicsFor  ( const Matrix4 & tr,
                                                       const InverseKinematics & angles,
                                                       Kinematics & body_sheet         );
        void                applyForwardKinematics   ( const Kinematics & sheet        );
        void                applyInverseKinematics   ( const InverseKinematics & sheet );

        void                attachToBody             ( Body * body,
                                                       Joint * joint                   );

        bool                isRootBodyNode           (                                 ) const;
    };


    /*! This node controls drags in physic updates by assigning
        values to linear and angular drags. */
    class _DLLExportControl DragController : public Node
    {
      private:
        RealVar      mLinearDrag;    /*!< Linear drag of the node */
        RealVar      mAngularDrag;   /*!< Angular drag of the node */

      protected:
        virtual bool parseRSGCommand ( SToken cmd );

      public:
                     DragController  (            );
    };


    /*! This node handles contacts between bodies */
    class _DLLExportControl ContactJointHandler : public Node
    {
      private:
        BooleanVar   mBounceMode;       /*!< enables bouncyness of stops */
        RealVar      mBounceValue;      /*!< Bouncyness of stop */
        RealVar      mMinBounceVel;     /*!< Minimum bounce velocity */
        BooleanVar   mSlipMode;         /*!< Slip mode of movement */
        RealVar      mSlip1;            /*!< Force-dependent-slip (FDS) in friction direction 1 */
        RealVar      mSlip2;            /*!< Force-dependent-slip (FDS) in friction direction 2 */
        BooleanVar   mSoftERPMode;      /*!< Soft erp parameter mode. Enabled or disabled */
        RealVar      mSoftERP;          /*!< Error reduction parameter */
        BooleanVar   mSoftCFMMode;      /*!< Soft CFM parameter mode. Enabled or disabled */
        RealVar      mSoftCFM;          /*!< Constraint force mixing parameter */

      protected:
        virtual bool parseRSGCommand    ( SToken cmd );

      public:
                     ContactJointHandler(            );
    };


    /*! Hanlder for touch porceptors */
    class _DLLExportControl TouchPerceptorHandler : public ContactJointHandler
    {
    };


    /*! Base node for colliders. Colliders handle collisions in simulator */
    class _DLLExportControl Collider : public Node
    {
      private:
        PositionVar    mPosition;          /*!< Position of the collider */
        StringArrayVar mDisabledColliders; /*!< List of all disabled colliders for the node */

      protected:
        virtual bool   parseRSGCommand     ( SToken cmd );

      public:
                       Collider            (            );
    };


    /*! A box collider space node. */
    class _DLLExportControl BoxCollider : public Collider
    {
      private:
        PositionVar  mExtents;       /*!< Extents of the box */
        PositionVar  mRotation;      /*!< Rotation of the box */

      protected:
        virtual bool parseRSGCommand ( SToken cmd );

      public:
                     BoxCollider     (            );
    };


    /*! Sphere collider space */
    class _DLLExportControl SphereCollider : public Collider
    {
      private:
        RealVar      mRadius;        /*!< Radius of the sphere space */

      protected:
        virtual bool parseRSGCommand ( SToken cmd );

      public:
                     SphereCollider  (            );
    };


    /*! Capped cylinder collider space node. */
    class _DLLExportControl CCylinderCollider : public Collider
    {
      private:
        RealVar      mRadius;         /*!< Radius of the cylinder */
        RealVar      mHeight;         /*!< Height of the cylinder */

      protected:
        virtual bool parseRSGCommand  ( SToken cmd );

      public:
                     CCylinderCollider(            );
    };


    /*! Capsule collider space node. */
    class _DLLExportControl CapsuleCollider : public Collider
    {
      private:
        RealVar      mRadius;         /*!< Radius of the cylinder */
        RealVar      mHeight;         /*!< Height of the cylinder */

      protected:
        virtual bool parseRSGCommand  ( SToken cmd );

      public:
                     CapsuleCollider  (            );
    };


    /*! A transform collider node */
    class _DLLExportControl TransformCollider : public Collider
    {
    };

  }; // end namespace RSG2


}; // end namespace Klaus


#endif // PHYSIC_SCENE_NODES
