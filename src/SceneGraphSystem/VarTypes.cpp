/*
*********************************************************************************
*            VarTypes.cpp : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 14/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Provides variable definitions for different kinds of variables     *
*                                                                               *
*********************************************************************************
*/

/*! \file VarTypes.cpp
<pre>
<b>File:</b>          VarTypes.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       14/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Provides variable definitions for different kinds of variables
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
14/02/2011       Mahdi           Initial version created
</pre>
*/


#include "VarTypes.h"
#include <libZigorat/Parse.h>


/****************************************************************************/
/****************************  Class BooleanVar  ****************************/
/****************************************************************************/

/**
  Constructor of the BooleanVar. Sets the name and the type to boolean
  @param strName Name of the variable
*/
Klaus::RSG2::BooleanVar::BooleanVar(const std::string & strName) : Var(strName, VarBool)
{
  mValue = false;
}

/**
  This method parses the value for the variable from a given string.
  Throws exception if anything goes wrong.
  @param strValue The string contianing the variable value
*/
void Klaus::RSG2::BooleanVar::setValue(const std::string& strValue)
{
  if(strValue.empty())
    throw VarException("(BooleanVar) given string is empty");

  char c = strValue[0];
  mValue = (c == 'y') || (c == 'Y') || (c == 't') || (c == 'T');
}

/**
  This method returns the value of the variable as a string.
  @return The value of the variable as a string
*/
std::string Klaus::RSG2::BooleanVar::getValue() const
{
  if(mValue)
    return "true";
  else
    return "false";
}




/****************************************************************************/
/*****************************  Class IntVar  ******************************/
/****************************************************************************/

/**
  Constructor of the IntVar. Sets the name and the type to integer
  @param strName Name of the variable
*/
Klaus::RSG2::IntVar::IntVar(const std::string & strName) : Var(strName, VarInt)
{
  mValue = 0;
}

/**
  This method parses the value for the variable from a given string.
  Throws exception if anything goes wrong.
  @param strValue The string contianing the variable value
*/
void Klaus::RSG2::IntVar::setValue(const std::string& strValue)
{
  int index = 0;
  if(Parse::parseFirstInt(strValue, mValue, index))
    throw VarException("(IntVar) can not parse from %s", strValue.c_str());
}

/**
  This method returns the value of the variable as a string.
  @return The value of the variable as a string
*/
std::string Klaus::RSG2::IntVar::getValue() const
{
  char b[20];
  sprintf(b, "%d", mValue);
  return b;
}




/****************************************************************************/
/******************************  Class CharVar  *****************************/
/****************************************************************************/


/**
  Constructor of CharVar. Sets the name and the type to character
  @param strName Name of the variable
*/
Klaus::RSG2::CharVar::CharVar(const std::string & strName) : Var(strName, VarChar)
{
  mValue = '\0';
}

/**
  This method parses the value for the variable from a given string.
  Throws exception if anything goes wrong.
  @param strValue The string contianing the variable value
*/
void Klaus::RSG2::CharVar::setValue(const std::string& strValue)
{
  if(strValue.empty())
    throw VarException("(CharVar) given string is empty");

  mValue = strValue[0];
}

/**
  This method returns the value of the variable as a string.
  @return The value of the variable as a string
*/
std::string Klaus::RSG2::CharVar::getValue() const
{
  std::string s;
  s.append(1, mValue);
  return s;
}




/****************************************************************************/
/*****************************  Class RealVar  ******************************/
/****************************************************************************/

/**
  Constructor of RealVar. Sets the name and the type to Real
  @param strName Name of the variable
*/
Klaus::RSG2::RealVar::RealVar(const std::string & strName) : Var(strName, VarReal)
{
  mValue = 0.0;
}

/**
  This method parses the value for the variable from a given string.
  Throws exception if anything goes wrong.
  @param strValue The string contianing the variable value
*/
void Klaus::RSG2::RealVar::setValue(const std::string& strValue)
{
  int index = 0;
  if(!Parse::parseFirstReal(strValue, mValue, index))
    throw VarException("(RealVar) can not parse from %s", strValue.c_str());
}

/**
  This method returns the value of the variable as a string.
  @return The value of the variable as a string
*/
std::string Klaus::RSG2::RealVar::getValue() const
{
  char b[20];
  sprintf(b, "%f", mValue);
  return b;
}




/****************************************************************************/
/*****************************  Class StringVar  ****************************/
/****************************************************************************/

/**
  Constructor of the StringVar. Sets the name and type to string
  @param strName Name of the variable
*/
Klaus::RSG2::StringVar::StringVar(const std::string & strName) : Var(strName, VarString)
{
}

/**
  This method parses the value for the variable from a given string.
  Throws exception if anything goes wrong.
  @param strValue The string contianing the variable value
*/
void Klaus::RSG2::StringVar::setValue(const std::string& strValue)
{
  mValue = strValue;
}

/**
  This method returns the value of the variable as a string.
  @return The value of the variable as a string
*/
std::string Klaus::RSG2::StringVar::getValue() const
{
  return mValue;
}




/****************************************************************************/
/*************************  Class StringArrayVar  ***************************/
/****************************************************************************/

/**
  Constructor of StringArrayVar. Sets the name and type to array of strings
  @param strName Name of the variable
*/
Klaus::RSG2::StringArrayVar::StringArrayVar(const std::string & strName) : Var(strName, VarStringArray)
{
}

/**
  This method parses the value for the variable from a given string.
  Throws exception if anything goes wrong.
  @param strValue The string contianing the variable value
*/
void Klaus::RSG2::StringArrayVar::setValue(const std::string& strValue)
{
  mValue.clear();
  mValue.push_back(strValue);
}

/**
  This method returns the value of the variable as a string.
  @return The value of the variable as a string
*/
std::string Klaus::RSG2::StringArrayVar::getValue() const
{
  if(mValue.empty())
    return "";
  else
    return mValue[0];
}




/****************************************************************************/
/***************************  Class PositionVar  ****************************/
/****************************************************************************/

/**
  Constructor of the PositionVar. Sets the name and the type to position
  @param strName Name of the variable
*/
Klaus::RSG2::PositionVar::PositionVar(const std::string & strName) : Var(strName, VarPosition)
{
}

/**
  This method parses the value for the variable from a given string.
  Throws exception if anything goes wrong.
  @param strValue The string contianing the variable value
*/
void Klaus::RSG2::PositionVar::setValue(const std::string& strValue)
{
  int index = 0;
  double result;
  for(int i = 0; i < 3; i++)
    if(!Parse::parseFirstReal(strValue, result, index))
      throw VarException("(PositionVar) could not parse %s", strValue.c_str());
    else
      mValue[i] = result;
}

/**
  This method returns the value of the variable as a string.
  @return The value of the variable as a string
*/
std::string Klaus::RSG2::PositionVar::getValue() const
{
  return mValue.str();
}




/****************************************************************************/
/****************************  Class Matrix4Var  ****************************/
/****************************************************************************/

/**
  Constructor of the Matrix4Var. Sets the name and type to a 4x4 matrix
  @param strName Name of the variable
*/
Klaus::RSG2::Matrix4Var::Matrix4Var(const std::string & strName) : Var(strName, VarMatrix4)
{
}

/**
  This method parses the value for the variable from a given string.
  Throws exception if anything goes wrong.
  @param strValue The string contianing the variable value
*/
void Klaus::RSG2::Matrix4Var::setValue(const std::string& strValue)
{
  int index = 0;
  double result;
  for(int i = 0; i < 16; i++)
    if(!Parse::parseFirstReal(strValue, result, index))
      throw VarException("(MatrixVar) could not parse %s", strValue.c_str());
    else
      mValue[i/4][i%4] = result;
}

/**
  This method returns the value of the variable as a string.
  @return The value of the variable as a string
*/
std::string Klaus::RSG2::Matrix4Var::getValue() const
{
  return mValue.str();
}




/****************************************************************************/
/***************************  Class QuaternionVar  **************************/
/****************************************************************************/

/**
  Constructor of the QuaternionVar. Sets the name and type to quaternion
  @param strName Name of the variable
*/
Klaus::RSG2::QuaternionVar::QuaternionVar(const std::string & strName) : Var(strName, VarMatrix4)
{
}

/**
  This method parses the value for the variable from a given string.
  Throws exception if anything goes wrong.
  @param strValue The string contianing the variable value
*/
void Klaus::RSG2::QuaternionVar::setValue(const std::string& strValue)
{
  int index = 0;
  double result;
  for(int i = 0; i < 4; i++)
    if(!Parse::parseFirstReal(strValue, result, index))
      throw VarException("(QauternionVar) could not parse %s", strValue.c_str());
    else
      mValue[i] = result;
}

/**
  This method returns the value of the variable as a string.
  @return The value of the variable as a string
*/
std::string Klaus::RSG2::QuaternionVar::getValue() const
{
  return mValue.str();
}
