/*
*********************************************************************************
*         EffectorNodes.h : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 15/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for effector scene nodes.     *
*            These nodes provide command support for agents. Agent can send     *
*            commands specified by the effectors to the server to execute.      *
*                                                                               *
*********************************************************************************
*/

/*! \file EffectorNodes.h
<pre>
<b>File:</b>          EffectorNodes.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       15/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for effector scene nodes.
               These nodes provide command support for agents. Agent can send
               commands specified by the effectors to the server to execute.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
15/02/2011       Mahdi           Initial version created
</pre>
*/

#ifndef EFFECTOR_SCENE_NODES
#define EFFECTOR_SCENE_NODES

#include "Node.h" // needed for EffectorBase

namespace Klaus
{

  namespace RSG2
  {

    /*! The effector used to create materials for simspark meshes */
    class _DLLExportControl SingleMatInitEffector : public EffectorBase
    {
    };


    /*! This effector is used to place the agent in the needed position */
    class _DLLExportControl BeamEffector : public EffectorBase
    {
    };


    /*! This effector enables the agent to talk with its teammates */
    class _DLLExportControl SayEffector : public EffectorBase
    {
    };


    /*! This effector is used to create meshes for simspark */
    class _DLLExportControl StaticMeshInitEffector : public EffectorBase
    {
    };


    /*! I don't know what this effector does. */
    class _DLLExportControl AgentSyncEffector : public EffectorBase
    {
    };


    /*! Parent of all types of joint effectors */
    class _DLLExportControl JointEffector : public EffectorBase
    {
    };

    /*! The effector used to change joint speeds for hinge joints */
    class _DLLExportControl HingeJointEffector : public JointEffector
    {
    };


    /*! The effector used to change universal joint speeds */
    class _DLLExportControl UniversalJointEffector : public JointEffector
    {
    };

  }; // end namespace RSG2

}; // end namespace Klaus

#endif // EFFECTOR_SCENE_NODES
