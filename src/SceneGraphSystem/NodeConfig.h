/*
*********************************************************************************
*            NodeConfig.h : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 01/08/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Base class for configuration of node system.                       *
*                                                                               *
*********************************************************************************
*/

/*! \file NodeConfig.h
<pre>
<b>File:</b>          NodeConfig.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       01/08/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Base class for configuration of node system.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
01/08/2010       Mahdi           Initial version created
</pre>
*/

#ifndef NODE_CONFIG_2
#define NODE_CONFIG_2

#ifdef IMPLEMENT_GRAPHICS
#include <ConfigSystem/Configurable.h>
#endif
#include <libZigorat/Singleton.h>



namespace Klaus
{

  namespace RSG2
  {

    const unsigned LOG_TO_LOGGER        =  0x01; /*!< outputs be forwarded to logger? */
    const unsigned LOG_TO_COUT          =  0x02; /*!< outputs be forwarded to cout?   */
    const unsigned LOG_TO_CERR          =  0x04; /*!< outputs be forwarded to cerr?   */
    const unsigned LOG_THROW_EXCEPTIONS =  0x08; /*!< outputs throw exceptions?       */

    const unsigned ERRORS_BREAK_PARSE   =  0x10; /*!< exit parse on fail?             */
    const unsigned USE_SIMSPARK_MODELS  =  0x20; /*!< use models of simspark          */


    /*! This class is manages configuration of the node system. */
    class _DLLExportControl NodeConfig :
      #ifdef IMPLEMENT_GRAPHICS
      public Config::Configurable,
      #endif
      public Singleton<NodeConfig>
    {
      private:
        unsigned            mOptions;         /*!< All node options             */

                            NodeConfig        (                                 );
                          ~ NodeConfig        (                                 );

      protected:

        // configuration
        virtual void        _loadDefaultConfig(                                 );

        #ifdef IMPLEMENT_GRAPHICS
        virtual bool        _readConfig       ( const Config::ConfigType & type );
        virtual void        _writeConfig      ( const Config::ConfigType & type );
        #endif

      public:
        static NodeConfig * getSingleton      (                                 );
        static void         freeSingleton     (                                 );

        bool                loggerEnabled     (                                 ) const;
        bool                stdoutEnabled     (                                 ) const;
        bool                stderrEnabled     (                                 ) const;
        bool                exceptionsEnabled (                                 ) const;
        bool                errorsBreakParse  (                                 ) const;

        bool                useSimSparkModels (                                 ) const;
    };


  }; // end namespace RSG2


}; // end namespace Klaus


#endif // NODE_CONFIG_2
