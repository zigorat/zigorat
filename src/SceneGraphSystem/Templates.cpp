/*
*********************************************************************************
*          Templates.cpp : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 21/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class definitions for SceneTemplate and         *
*            TemplateFactory which implement templates in simulator's scene     *
*            graphs. Templates ease the process of creating robots by creating  *
*            C-function like implementations of various robot parts             *
*                                                                               *
*********************************************************************************
*/

/*! \file Templates.cpp
<pre>
<b>File:</b>          Templates.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       21/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class definition for SceneTemplate and
               TemplateFactory which implement templates in simulator's scene
               graphs. Templates ease the process of creating robots by creating
               C-function like implementations of various robot parts
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
21/02/2011       Mahdi           Initial version created
</pre>
*/

#include "Templates.h"
#include <libZigorat/Logger.h>              // needed for Logger
#include <libZigorat/Parse.h>               // needed for Parse
#include <libZigorat/FileSystem.h>          // needed for fileExists
#include <libZigorat/ApplicationSettings.h> // needed for ApplicationSettings


/*! The template manager singleton instance */
template<> Klaus::RSG2::TemplateManager* Klaus::Singleton<Klaus::RSG2::TemplateManager>::mInstance = 0;


/*****************************************************************************/
/*****************************  Class Template  ******************************/
/*****************************************************************************/

/**
  Constructor of Template. Sets versions to unknown
*/
Klaus::RSG2::Template::Template()
{
  mMajorVersion = -1;
  mMinorVersion = -1;
  mConfig = NodeConfig::getSingleton();
}

/**
  Destrutor of Template. Does nothing
*/
Klaus::RSG2::Template::~Template()
{

}

/**
  This method handles errors. All errors will be sent here with their parameters
  and they will be logged, thrown or printed depending on the configuration of
  system.
  @param str Character string with possible format specifiers
  @param ... variables that define the values of the specifiers
*/
void Klaus::RSG2::Template::error(const char* str, ... ) const
{
  va_list ap;
  char buf[1024] = "(Template) ";
  va_start(ap, str);
  vsnprintf(buf + 11, 1023 - 11, str, ap);
  va_end(ap);

  if(mConfig->loggerEnabled())
    Logger::getSingleton()->log(24, buf);
  if(mConfig->stdoutEnabled())
    std::cout << buf << std::endl;
  if(mConfig->stderrEnabled())
    std::cerr << buf << std::endl;
  if(mConfig->exceptionsEnabled())
    throw buf;
}

/**
  This method is responsible to parse the body section of a template
  and find it's template decleration, load all parameter names from
  the decleration and set the injection starting index.
  @return True on successfull parse, false otherwise
*/
bool Klaus::RSG2::Template::parseBody()
{
  SToken body = mTokens[mBodyIndex];
  // find the decleration
  for(size_t i = 0; i < body.size(); ++i)
  {
    if((body[i].isNormal()) && (!body[i].empty()) && (body[i][0] == "templ"))
    {
      for(size_t j = 1; j < body[i].size(); ++j)
        mParameterNames.push_back(body[i][j]);

      mBodyStartToken = i + 1;
      return true;
    }
  }

  return true;
}

/**
  This method parses the value for a definition of template parameter in the template
  call command and adds the resulting variable to the caller node's definitions.
  @param name Name of the variable
  @param def The definition containing the value for parameter
  @param node The caller node to retrieve it's definitions
  @return True on successful parse, false otherwise
*/
bool Klaus::RSG2::Template::parseVariable(const std::string& name, SToken def, Node* node)
{
  // all possibilities:
  // (eval   ---> x
  // (join   ---> x
  // $x
  // 12
  // text

  Var * v = NULL;
  // $x
  // 12
  // text

  if(def.str()[0] == '$')
  {
    // it's a definition
    CVar cv = node->getDefinition(def);
    if(cv->getType() == Var::VarReal)
      v = new RealVar(name);
    else if(cv->getType() == Var::VarString)
      v = new StringVar(name);
    else
    {
      error("unknown definition type in define: '%s'", cv->getName().c_str());
      return false;
    }

    v->setValue(cv->getValue());
  }
  else
  {
    // its a simple number or a simple string
    int index = 0;
    double result;
    if(Parse::parseFirstReal(def, result, index))
      v = new RealVar(name);
    else
      v = new StringVar(name);

    v->setValue(def);
  }

  node->setDefinition(v);
  return true;
}

/**
  This method returns the name of the template
  @return Name of the template
*/
std::string Klaus::RSG2::Template::getName() const
{
  return mName;
}

/**
  This method returns the major version of the template
  @return The major version of the template
*/
int Klaus::RSG2::Template::getMajorVersion() const
{
  return mMajorVersion;
}

/**
  This method returns the minor version of the template
  @return The minor version of the template
*/
int Klaus::RSG2::Template::getMinorVersion() const
{
  return mMinorVersion;
}

/**
  This method initializes the template's main section. It does not create nodes within
  the template or execute it's commands. Instead it parses the template and stores
  all the nodes and commands it finds in a cache. When a node wants to use the template
  it shall call 'loadFromSceneGraph' command and then the template will pump all the
  nodes and commands to the caller node with their appropriate values.
  @param name String which holds the template's main part
  @return True on successful load, false otherwise
*/
bool Klaus::RSG2::Template::loadTemplate(const std::string& name)
{
  try
  {
    mParameterNames.clear();
    mName.clear();
    mTokens.clear();
    mMajorVersion = mMinorVersion = -1;

    // 32 KB seems fairly enough for a scene graph template
    if(FileSystem::fileExists(name))
      mTokens.parseFile(name, 32*1024);
    else
      mTokens.parseFile(ApplicationSettings::getSingleton()->getRSGPath() + name, 32 * 1024);
  }
  catch(SLangException e)
  {
    error("error parsing file template:\n%s",e.str());
    return false;
  }

  bool bParsedHeader = false;
  for(size_t i = 0; i < mTokens.size(); ++i)
  {
    if(!mTokens[i].isNormal())
      continue;

    if(!bParsedHeader)
    {
      SToken t = mTokens[i];
      if(t.empty() || t.size() != 3 || t[0] != "RSG")
      {
        error("expected scene graph header, got '%s' instead", t.str().c_str());
        return false;
      }

      // extract versions
      mMajorVersion = Round(t[1].number());
      mMinorVersion = Round(t[2].number());

      if(mMajorVersion != 0 || mMinorVersion != 1)
      {
        error("unknown version parsed for '%s': %d,%d", name.c_str(), mMajorVersion, mMinorVersion);
        return false;
      }

      bParsedHeader = true;
    }
    else // parse the body
    {
      mBodyIndex = i;
      return parseBody();
    }
  }

  // if control reaches here, it means that header or body is not found
  error("can not find header or body in the template '%s'", name.c_str());
  return false;
}

/**
  This method injects the templates nodes and commands to the caller node. Before pumping
  the information the caller node will be provided with the templates parameters (since
  they are necessary for having correct values) with their respective values. The nall
  the nodes and commands in the template's cache will be pumped up to the caller an the
  caller shall parse them.
  @param nd The node which needs to use the template
  @param cmd The importScene command which contains the values for scene import paramaters
  @return True on successful injection, false otherwise
*/
bool Klaus::RSG2::Template::inject(Node* nd, SToken cmd)
{
  // first add all definitions in the template header to the node
  size_t index = 0;
  for(size_t i = 2; i < cmd.size(); ++i)
    if((cmd[i].isComment() == false) && (parseVariable(mParameterNames[index++], cmd[i], nd) == false))
      return false;

  // now inject all tokens to the node
  SToken body = mTokens[mBodyIndex];
  for(size_t i = mBodyStartToken; i < body.size(); ++i)
    if(!body[i].isComment() && !nd->parseToken(body[i]) && mConfig->errorsBreakParse())
      return false;

  return true;
}





/*****************************************************************************/
/*************************  Class TemplateManager  ***************************/
/*****************************************************************************/

/**
  Constructor of TemplateManager.
*/
Klaus::RSG2::TemplateManager::TemplateManager()
{

}

/**
  Destructor of TemplateManager. Removes all created templates
*/
Klaus::RSG2::TemplateManager::~TemplateManager()
{
  for(Template::List::iterator it = mTemplates.begin(); it != mTemplates.end(); ++it)
    delete it->second;

  mTemplates.clear();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::RSG2::TemplateManager * Klaus::RSG2::TemplateManager::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new TemplateManager;
    Logger::getSingleton()->log(1, "(TemplateManager) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::RSG2::TemplateManager::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(TemplateManager) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(TemplateManager) removed singleton");
  }
}

/**
  This method returns a template associated with the given template file. If the template
  exists then it's instance will be returned. Otherwise the template will be created.
  @param name Name of the template file
  @return Template instance of the given template file name
*/
Klaus::RSG2::Template * Klaus::RSG2::TemplateManager::getTemplate( const std::string & name )
{
  Template::List::iterator it = mTemplates.find(name);
  if(it != mTemplates.end())
    return it->second;

  Template * temp = new Template;
  if(!temp->loadTemplate(name))
  {
    delete temp;
    return NULL;
  }
  else
  {
    mTemplates[name] = temp;
    return temp;
  }
}
