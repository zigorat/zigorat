/*
*********************************************************************************
*         AuxilaryNodes.h : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 15/02/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for auxilary scene nodes.     *
*            These nodes provide various utilities which can not be grouped     *
*            in a single definition.                                            *
*                                                                               *
*********************************************************************************
*/

/*! \file AuxilaryNodes.h
<pre>
<b>File:</b>          AuxilaryNodes.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       15/02/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for auxilary scene nodes. These
               nodes provide various utilities which can not be grouped in a single definition.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
15/02/2011       Mahdi           Initial version created
</pre>
*/

#ifndef AUX_SCENE_NODES
#define AUX_SCENE_NODES


#include "Node.h"     // needed for Klaus::RSG2::Node


namespace Klaus
{

  namespace RSG2
  {

    /*! Root node of graph. This special node does not have a specific type.
        Just serves as a place holder for all the nodes in the scene graph.
    */
    class _DLLExportControl Root : public TransformableNode
    {
      public:
        Root();
    };

    /*! Camera wrapper in the SimSpark framework. */
    class _DLLExportControl Camera: public Node
    {
    };

  }; // end namespace RSG2

}; // end namespace Klaus


#endif // AUX_SCENE_NODES
