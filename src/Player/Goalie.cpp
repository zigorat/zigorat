/*
*********************************************************************************
*            Goalie.cpp : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 03/14/2008                                                             *
*  Author: Amin Mohammadi, Mahdi Hamdarsi                                       *
*  Comments: Actions and skills which apply only to goalie are done here in     *
*            this file                                                          *
*                                                                               *
*********************************************************************************
*/

/*! \file Goalie.cpp
<pre>
<b>File:</b>          Goalie.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Amin Mohammadi, Mahdi Hamdarsi
<b>Created:</b>       03/14/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Actions and skills which apply only to goalie are implemented here
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/14/2008       Amin, Mahdi           Initial version created
</pre>
*/

#include "Player.h"

/**
  This method is used to determine goalies decision. All the goalie's
  decisions should be coded in this method
*/
void Klaus::Player::makeGoalieDecision()
{
  //searchBall();
  walk();
}
