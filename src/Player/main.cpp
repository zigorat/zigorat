/*
*********************************************************************************
*             main.cpp : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This is the core of project. It links every other part to other    *
*            and executes agent.                                                *
*                                                                               *
*********************************************************************************
*/

/*! \file main.cpp
<pre>
<b>File:</b>          main.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This is the main part of project to link and execute agent
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
</pre>
*/


#include <iostream>                         // needed for cout
#include <cstring>                          // needed for str*

#include "Player.h"                         // needed for Player
#include "DefaultConfig.h"                  // needed for default configuration settings
#include <libZigorat/Parse.h>               // needed for Parse
#include <libZigorat/ApplicationSettings.h> // needed to set application settings


using namespace std;
using namespace Klaus;


string   strHost       = "127.0.0.1";       /*!< Host to connect to                       */
int      iPort         = 3100;              /*!< Port to which agent is connecting        */
int      iReqNumber    = 0;                 /*!< Number of the agent                      */
string   strTeamName   = "Zigorat";         /*!< Team name for the agent to register with */
string   strConfDir    = DEFAULT_DATA_ROOT; /*!< Configuration folder                     */
bool     bSuppliedLog  = false;             /*!< Whether should log to output             */
ofstream os;                                /*!< Output stream for logging                */


bool StartAgent( int argc, char *argv[] );
void StopAgent();
bool ConfigureArguments(int argc, char* argv[]);
void PrintOptions();


#define EXEC_MAIN      /*!< For debugging other parts, undef this */
#ifdef EXEC_MAIN
/**
  This is the main function and creates and links all the different classes.
  First it reads in all the parameters from the command prompt and uses
  these values to create the classes. After all the classes are linked,
  the mainLoop in the Agent class is called.
  @param argc Argument count of arguments passed to application
  @param argv[] The argument values
  @return Application exit status
*/
int main(int argc, char* argv[])
{
  Randomize();

  /// Two functions StartAgent and StopAgent are defined to enable the agent start/stop + restart
  /// It means that you can always start and stop the agent within main(), as long as their order
  /// is maintained. This may be handy for testing/learning purposes which needs  agent to be run
  /// Several times in a row.

  if(!StartAgent(argc, argv))
  {
    StopAgent();
    return 1; // return with 1 exit status
  }

  /// Start the agent main loop
  Player::getSingleton()->mainLoop();

  /// Stop the agent now
  StopAgent();

  return 0;
}
#endif // EXEC_MAIN



/**
  This method starts all the needed utilities for the agent like connection
  and then starts  the agent itself, linking everything to each other. Know
  that the order in which classes are initialized are important, cause most
  of classes are singletons
  @param argc Number of arguments passed to application
  @param argv[] Arguments passed to application
  @return Indicates whether agent started successfully or not
*/
bool StartAgent( int argc, char *argv[] )
{
  if(!ConfigureArguments(argc, argv))
    return false;

  /// Initialize application used paths
  ApplicationSettings::getSingleton()->initialize(DEFAULT_APPLICATION_NAME, strConfDir, true);
  strConfDir = ApplicationSettings::getSingleton()->getDataPath();

  /// Set log output file and initialize logger
  if(bSuppliedLog)
    Logger::getSingleton()->setOutputStream(&os);

  /// Load agent thresholds
  AgentSettings::getSingleton()->loadFromFile(strConfDir + "agent.conf");

  /// Load Formations
  if(!FormationSystem::getSingleton()->initialize(strConfDir + "formations.conf", Formation_Initial_Offensive))
  {
    cerr << "(StartAgent) error reading formation configuration file" << endl;
    return false;
  }

  /// Connect to simulator
  if(!SimSpark::getSingleton()->connect(strHost.c_str(), iPort))
  {
    cerr << "(StartAgent) could not connect to simulator" << endl;
    return false;
  }

  /// Initialize the agent
  if(!Player::getSingleton()->initialize(strTeamName, iReqNumber))
  {
    cerr << "(StartAgent) errors occured, agent was not started" << endl;
    return false;
  }

  // All done
  return true;
}

/**
  This method  stops the agent and its utility objects and frees everything from
  the memory, also frees singleton instances of all classes. If you have written
  any singleton, call it's destructor here
*/
void StopAgent()
{
  Player::freeSingleton();
  ActHandler::freeSingleton();
  WorldModel::freeSingleton();
  SimSpark::freeSingleton();
  FormationSystem::freeSingleton();
  AgentSettings::freeSingleton();
  Logger::freeSingleton();

  if(os)
    os.close();
}

/**
  Read in all the command options and change the associated variables
  in case of illegal options the agent doesn't start.
  @param argc Argument count of application
  @param argv Arguments of application
  @return bool indicating wheather command line arguments were parsed successfully
*/
bool ConfigureArguments(int argc, char* argv[])
{
  for(int i = 1 ; i < argc; ++i)
  {
    std::string arg = argv[i];
    // help is only option that does not have to have an argument
    if(arg == "--help")
    {
      PrintOptions();
      return false;
    }

    if(i + 1 == argc)
    {
      cerr << "(ConfigureArguments) need argument for option: " << arg << endl;
      return false;
    }

    std::string param = argv[++i]; // move to the next argument which is the option's value and read it
    int index = 0;

    // read the option name
    if(arg == "-h" || arg == "--host")
      strHost = param;
    else if(arg == "--port" || arg == "-p")
    {
      if(!Parse::parseFirstInt(param, iPort, index))
      {
        cerr << "(ConfigureArguments) could not read port number from the given parameter: " << param << endl;
        return false;
      }
    }
    else if(arg == "--number" || arg == "-n")
    {
      if(!Parse::parseFirstInt(param, iReqNumber, index))
      {
        cerr << "(ConfigureArguments) could not read player number from the given parameter: " << param << endl;
        return false;
      }
    }
    else if(arg == "--team" || arg == "-t")
      strTeamName = param;
    else if(arg == "--dir" || arg == "-d")
      strConfDir = param;
    else if(arg == "--logfile" || arg == "-o")
      os.open(param.c_str(), std::ios::out);
    else if(arg == "--loglevel" || arg == "-l")
    {
      int iMinLogLevel;
      int iMaxLogLevel;

      if(!Parse::parseFirstInt(param, iMinLogLevel, index))
      {
        cerr << "(ConfigureArguments) could not read log level from the given parameter: " << param << endl;
        return false;
      }

      if(index < (int)param.size() && param[index] == '-')
      {
        ++index;

        if(!Parse::parseFirstInt(param, iMaxLogLevel, index))
        {
          cerr << "(ConfigureArguments) could not read max log level from the given parameter: " << param << endl;
          return false;
        }

        Logger::getSingleton()->addLogRange(iMinLogLevel, iMaxLogLevel);
      }
      else
        Logger::getSingleton()->addLogLevel( iMinLogLevel );
    }
    else
    {
      PrintOptions();
      return false;
    }
  }

  return true;
}

/*! Prints any options that can be configured from the command line */
void PrintOptions()
{
  cout << "usage: Player [-param_name param_value]                   " << endl
       << "                                                          " << endl
       << "  --help       : Print this help                          " << endl
       << "  -h, --host   : Specify the server host to connect       " << endl
       << "  -p, --port   : Specify the server port to connect       " << endl
       << "  -n, --number : Specify the agents number in game        " << endl
       << "  -t, --team   : Specify the team name of agent           " << endl
       << "  -d, --dir    : Specify the folder in which player is in " << endl << endl;
}
