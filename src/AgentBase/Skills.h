#ifndef SKILLS
#define SKILLS


#include <libZigorat/Logger.h>       // needed for Logger
#include <SceneGraphSystem/Graph.h>  // needed for RSG2::Graph
#include "ActHandler.h"              // needed for ActHandler
#include "WorldModel.h"              // needed for WorldModel
#include "PIDController.h"           // needed for PIDController

namespace Klaus
{


  /**
    This class is a base for creating skills. When a skill's execution
    is done, The agent's state will be updated and executeInternal will
    be called. Afterwards the newly generated state will be sent to ActHandler.
  */
  class Skill
  {
    private:
      typedef std::map<std::string, PIDController*> PIDMap;

    protected:
      InverseKinematics   mState;           /*!< Evaluated state of the Skill. */
      PIDMap              mPIDs;            /*!< PID Controllers for all joints */
      Logger            * mLog;             /*!< Logger instance */
      RSG2::Graph       * mGraph;           /*!< Agent's scene graph instance */
      ActHandler        * mAct;             /*!< ActionHandler instance to send commands to */
      WorldModel        * mWM;              /*!< WorldModel instance */

      void                sendActions       (                );
      virtual void        initializeInternal(                );
      virtual void        executeInternal   (                ) = 0;

    public:
                          Skill             (                );

      void                initialize        (                );
      void                execute           (                );
  };


  /*!< A very very simple and basic walk gesture animation. */
  class Walk : public Skill
  {
    private:
      VecPosition         mHip;             /*!< Start position of the hip */

      double              mPeriod;          /*!< Gait interval */
      double              mGaitLength;      /*!< Gait length */
      double              mGaitHeight;      /*!< Gait height */
      double              mThighLength;     /*!< Length of the thigh */
      double              mShankLength;     /*!< Length of the shank */

    protected:
      virtual void        initializeInternal(            );
      virtual void        executeInternal   (            );
  };

}; // end namespace Klaus


#endif // SKILS
