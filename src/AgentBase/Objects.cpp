/*
*********************************************************************************
*           Objects.cpp : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains Objects definition which represent soccer       *
*            players, flags, etc.                                               *
*                                                                               *
*********************************************************************************
*/

/** \file Objects.cpp
<pre>
<b>File:</b>          Objects.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains Objects definition which represent soccer
               players, flags, etc.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
</pre>
*/

#include "Objects.h"
#include "AgentSettings.h"
#include <iostream>

/*! Definition of AgentObject singleton */
template<> Klaus::AgentObject* Klaus::Singleton<Klaus::AgentObject>::mInstance = 0;

/*! Definition of Ball singleton*/
template<> Klaus::Ball* Klaus::Singleton<Klaus::Ball>::mInstance = 0;


namespace Klaus
{


/*****************************************************************************/
/*******************************  Class Object  ******************************/
/*****************************************************************************/

/**
  This constructor creates an object, all variables are initialized by
  default (illegal) values
*/
Object::Object()
{
  mTimeLastSeen = -1;
  mTimeGlobalPos = -1;
  mType = UnknownObject;
}

/**
  This destructor destroys an object, all variables are freed and deleted
  by this routine
*/
Object::~Object()
{
}

/**
  This method returns the global position of this object. The time of this
  information is related to the time returned by the last time this object
  was updated from relative see message to global field coordinates.
  @return global position of this object
*/
VecPosition Object::getGlobalPosition() const
{
  return mGlobalPosition;
}

/**
  This method sets the global position of the object upon the raw received
  information from server.
  @param newPos new global position
  @param time Time which the object's global position is updated
*/
void Object::setGlobalPosition(const VecPosition & newPos, const Time & time)
{
  mGlobalPosition = newPos;
  mTimeGlobalPos = time;
}

/**
  This method returns the relative position of this object. The time of this
  information is related to the time returned by the last time this object
  was seen, but is not checked.
  @return global position of this object
*/
VecPosition Object::getRelativePosition() const
{
  return mRelativePosition;
}

/**
  This method sets the global position of the object upon the information
  recieved from server.
  @param newPos new relative position of the object
  @param time Time which the object's relative position is received
*/
void Object::setRelativePosition(const VecPosition & newPos, const Time & time)
{
  mRelativePosition = newPos;
  mTimeLastSeen = time;
}

/**
  This method returns the last time which the object's global position
  was updated from it's relative position
  @return The time of last update of object's global position from relative
*/
Time Object::getTimeLastGlobalPosition() const
{
  return mTimeGlobalPos;
}

/**
  This method returns the last time object was seen
  @return Time last iem object was seen
*/
Time Object::getTimeLastSeen() const
{
  return mTimeLastSeen;
}

/**
  This method returns the confidence of the information of this object.
  The confidence is related to the last time this object was seen and
  the specified time (normally the time of the last received message).
  @param time current time to compare with time object was last seen
  @return confidence factor of this object
*/
double Object::getConfidence( const Time & time ) const
{
  if(mTimeLastSeen == -1)
    return 0.0;
  else
    return SoccerTypes::getConfidence(mTimeLastSeen, time);
}

/**
  This method returns the type of the object.
  @return Object type
*/
ObjectType Object::getType() const
{
  return mType;
}

/**
  This method sets the object type.
  @param obj_type Type of the object
*/
void Object::setType(const Klaus::ObjectType& obj_type)
{
  mType = obj_type;
}




/*****************************************************************************/
/*********************************  Class Flag  ******************************/
/*****************************************************************************/

/**
  This is the constructor for he Flag, Nothing
  important to do here
*/
Flag::Flag():Object()
{
}

/**
  This is destructor of the Flag, Does Nothing.
*/
Flag::~Flag()
{
}




/*****************************************************************************/
/**************************  Class DynamicObject  ****************************/
/*****************************************************************************/

/**
  This is the constructor for DynamicObject. A DynamicObject is created with
  all the variables initialized by default values
*/
DynamicObject::DynamicObject(): Object()
{
}

/**
  This is the destructor for DynamicObject.
*/
DynamicObject::~DynamicObject( )
{
}

/**
  This method returns linear velocity of the object after
  it has updated it's angular properties
  @return Angular velocity of the object
*/
VecPosition DynamicObject::getVelocity() const
{
  return mVelocity;
}

/**
  This method returns linear acceleration of the object after
  it has updated it's angular properties
  @return Angular velocity of the object
*/
VecPosition DynamicObject::getAcceleration() const
{
  return mAccelaration;
}

/**
  This method updates the objects physical data, including
  linear velocity and linear acceleration
*/
void DynamicObject::update()
{
  if(mTimeLastVelocity == mTimeGlobalPos)
    return; // can not calculate velocity and acceleration, no time has passed

  double dTime = mTimeLastVelocity / mTimeGlobalPos;

  // calculate current velocity
  mVelocity = (mLastPosition - mGlobalPosition) / dTime;
  mLastPosition = mGlobalPosition;
  mTimeLastVelocity = mTimeGlobalPos;

  // calculate current acceleration
  mAccelaration = (mLastVelocity - mVelocity) / dTime;
  mLastVelocity = mVelocity;
}




/*****************************************************************************/
/***************************  Class Ball  ******************************/
/*****************************************************************************/

/**
  This is the constructor for a Ball. It does nothing except
  initializing the class.
*/
Ball::Ball() : DynamicObject()
{
}

/**
  This is the destructor for a Ball. It does nothing except
  finalizing the class.
*/
Ball::~Ball()
{
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Ball * Ball::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new Ball;
    Logger::getSingleton()->log(1, "(Ball) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Ball::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(Ball) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(Ball) removed singleton");
  }
}

/**
  This method sets the radius of the ball. The radius can not be
  issued until server sends the information in init process, cause
  the value possibly changes.
  @param value value of the radius of ball
*/
void Ball::setRadius( const double & value )
{
  mRadius = value;
}

/**
  This method return the raius of ball.
  @return double The radius of ball sent by the server
*/
double Ball::getRadius( ) const
{
  return mRadius;
}

/**
  This method sets the mass of the ball. The mass can not be
  issued until server sends the information in init process,
  cause the value possibly changes.
  @param value value of the mass of ball
*/
void Ball::setMass( const double & value )
{
  mMass = value;
}

/**
  This method return the mass of ball.
  @return double The mass of ball sent by the server
*/
double Ball::getMass( ) const
{
  return mMass;
}




/*****************************************************************************/
/***************************  Class PlayerObject  ****************************/
/*****************************************************************************/

/**
  This is the constructor for Player. A Player is created with
  all variables initialized to default values
*/
PlayerObject::PlayerObject( ) :DynamicObject( )
{
  mNumber = -1;
}

/**
  This is the destructor for Player. Frees Player and
  associated objects
*/
PlayerObject::~PlayerObject( )
{
}

/**
  This method gets the players number which is recieved from the server
  within a see message.
  @return int Number of player in game
*/
int PlayerObject::getPlayerNumber( ) const
{
  return mNumber;
}

/**
  This method sets the players number which is recieved from the server
  within a see message.
  @param newNum Number for the player
*/
void PlayerObject::setPlayerNumber( const int & newNum )
{
  mNumber = newNum;
}

/**
  This method returns the current orientation of player
  @return Current orientation of player
*/
Quaternion PlayerObject::getOrientation() const
{
  return mOrientation;
}

/**
  This method sets he current orientation of the player to the given
  value specified
  @param q New orientation of the player
*/
void PlayerObject::setOrientation(const Quaternion & q)
{
  mOrientation = q;
}

/**
  This method gets the team side of player which is recieved from
  the server within a see message.
  @return SideT Side of player in game
*/
SideType PlayerObject::getTeamSide( ) const
{
  return mTeamSide;
}

/**
  This method sets the team side of the player which comes with a
  messge from the server.
  @param newSide The side of player
*/
void PlayerObject::setTeamSide( const SideType & newSide )
{
  mTeamSide = newSide;
}

/**
  This method returns the last visual information about the given body part on the player
  @param strName Name of the requested body part
  @return Last seen position of the part
*/
VecPosition PlayerObject::getBodyPart(const std::string& strName) const
{
  RSG2::ObjectState::Map::const_iterator it = mBodyParts.find(strName);
  if(it != mBodyParts.end())
    return it->second->getSeenPosition();

  Logger::getSingleton()->log(41, "(PlayerObject) no such part in the player's body: '%s'", strName.c_str());
  return VecPosition::Unknown;
}

/**
  This method sets the last seen position of the given body part of the player.
  @param strName Name of the player's body part
  @param pos Position of the part in the player's body
*/
void PlayerObject::setBodyPart(const std::string& strName, const Klaus::VecPosition& pos)
{
  RSG2::ObjectState::Map::iterator it = mBodyParts.find(strName);
  if(it != mBodyParts.end())
    it->second->setSeenPosition(pos);
  else
  {
    RSG2::ObjectState *o = new RSG2::ObjectState;
    o->setSeenPosition(pos);
    mBodyParts[strName] = o;
  }
}




/*****************************************************************************/
/***************************** Class AgentObject *****************************/
/*****************************************************************************/

/**
  This is the constructor for the class AgentObject and initializes the
  variables in the AgentObject. This the class that contains information
  about the agent itself.
*/
AgentObject::AgentObject( )
{
  mLog = Logger::getSingleton();

  mAgentModel = UnknownModel;
  mAgentScene = NULL;
}

/**
  This is the destructor of AgentObject. Clears agent model
*/
AgentObject::~AgentObject( )
{
  clearAgentModel();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
AgentObject * AgentObject::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new AgentObject;
    Logger::getSingleton()->log(1, "(AgentObject) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void AgentObject::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(AgentObject) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(AgentObject) removed singleton");
  }
}

/**
  This method clears agent model related information from world model
*/
void AgentObject::clearAgentModel()
{
  if(mAgentScene)
    delete mAgentScene;

  mAgentScene = NULL;
  mAgentModel = UnknownModel;
}

/**
  This method updates agents node position according to the recved see message
  position of 'seeing' point of agent. This point is the actual center of vision
  of agent and has the name of 'VisionPerceptor' or 'RestrictedVisionPerceptor'
*/
void AgentObject::update()
{
  // First update linear physical attributes
  DynamicObject::update();

  // update the agent scene graph
  mAgentScene->setGlobalTransform(mGlobalPosition, mOrientation);
  mAgentScene->updateForwardKinematics();
}

/**
  This method returns robot name and model of the agent
  @return AgentModel used by agent
*/
AgentModelType AgentObject::getAgentModel() const
{
  return mAgentModel;
}

/**
  This method sets the agent model to be used by agent. It loads agent  information by using
  model's scene graph. Scene graphs are simulators source of robots, this make  them contain
  all physical and graphical representations of robot models. This make them the best source
  to use for agent initialization.

  procedure for adding an agent model is this:

  1) In SoccerTypes.h, under  declearion of  enumeration type   AgentModelT, the  model name
     should be added.
  2) In the SoccerTypes.cpp, in the definition of the AgentModelNames, the path of the agent
     model scene graph should be added in the exact index  of the  array as the index in the
      AgentModelT enumeration
  3) The agent model description  files should be  copied to the agents  directory, provided
     that  the  agent can  access them by adding  the path supplied  in step 2 to the agents
     binary path

  These steps were for adding the  robot model to the sources only. By doing so, you provide
  the source with the structure for  enabling the usage of this  model. To actually use your
  model, the only thing which should be done is to set the agent_model variable in the agent
  configuration file to your desired model. The rest is done automatically by the base. This
  file is normally located in conf folder with name 'agent.conf'.

  @param am AgentModel to be used by the agent
  @return True if agent model setup was loaded successfully, false otherwise
*/
bool AgentObject::setAgentModel(AgentModelType am )
{
  if(mAgentScene)
    clearAgentModel();

  mAgentModel = am;
  std::string strFileName = SoccerTypes::getAgentModelString(am);
  mAgentScene = new RSG2::Graph;
  if(!mAgentScene->loadFromFile(strFileName.c_str()))
    std::cerr << "(AgentObject::setAgentModel) Could not load agent model" << std::endl;
  else
    return true;

  // Otherwise errors have occured, so clear the model and return false
  clearAgentModel();
  return false;
}

/**
  This method returns the scene graph of the agent
  @return RSG2::Graph instance of the agent
*/
RSG2::Graph * AgentObject::getSceneGraph()
{
  return mAgentScene;
}



}; // end namespace Klaus
