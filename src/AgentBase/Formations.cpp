/*
Copyright (c) 2000-2003, Jelle Kok, University of Amsterdam
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the University of Amsterdam nor the names of its
contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/** \file Formations.cpp

<pre>
<b>File:</b>          Formations.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: UvA Trilearn
<b>Authors:</b>       Jelle Kok
<b>Created:</b>       05/02/2001
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Definitions for the different methods in the classes that
               are associated with Formations.                      .
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
05/02/2001       Jelle Kok       Initial version created
</pre>
*/


#include "Formations.h"
#include "AgentSettings.h" // needed for AgentSettings
#include <iomanip>         // needed for std::setiosflags
#include <sstream>         // needed for stringstream


/*! Needed to elminate excess digits */
#define setdecimal(x) std::setprecision(x) << std::setiosflags(std::ios::fixed|std::ios::showpoint)

/*!< Singleton instance for FormationSystem. */
template<> Klaus::FormationSystem* Klaus::Singleton<Klaus::FormationSystem>::mInstance = 0;


/*****************************************************************************/
/*************************  Class PlayerPositioning  *************************/
/*****************************************************************************/

/**
  Constructor of PlayerPositioning. Sets default values for class members.
*/
Klaus::PlayerPositioning::PlayerPositioning()
{
  mAttraction.setPosition(0.5, 0.5);
  mRole = UnknownRole;
  mBehindBall = false;
  mPlayerNumber = -1;

  AgentSettings * s = AgentSettings::getSingleton();
  mConstraints.setRectPoints(VecPosition(-s->getFieldLength() / 2.0, s->getFieldWidth() / 2.0),
                             VecPosition(s->getFieldLength() / 2.0, -s->getFieldWidth() / 2.0));
}

/**
  This method parses a given token and extracts its value for the player positioning structure.
  @param token Token containing the positioning value
  @return True on successful parse, false otherwise
*/
bool Klaus::PlayerPositioning::parse(SToken token)
{
  if(token[0] == "home" && token.size() == 3)
  {
    mHome[0] = token[1].number();
    mHome[1] = token[2].number();
  }
  else if(token[0] == "attraction" && token.size() == 3)
  {
    mAttraction[0] = token[1].number();
    mAttraction[1] = token[2].number();
  }
  else if(token[0] == "x_range" && token.size() == 3)
  {
    VecPosition lt(mConstraints.getPosLeftTop());
    lt[0] = token[1].number();
    VecPosition rb(mConstraints.getPosRightBottom());
    rb[0] = token[2].number();
    mConstraints.setRectPoints(lt, rb);
  }
  else if(token[0] == "y_range" && token.size() == 3)
  {
    VecPosition lt(mConstraints.getPosLeftTop());
    lt[1] = token[1].number();
    VecPosition rb(mConstraints.getPosRightBottom());
    rb[1] = token[2].number();
    mConstraints.setRectPoints(lt, rb);
  }
  else if(token[0] == "x_min" && token.size() == 2)
  {
    VecPosition lt(mConstraints.getPosLeftTop());
    VecPosition rb(mConstraints.getPosRightBottom());
    lt[0] = token[1].number();
    mConstraints.setRectPoints(lt, rb);
  }
  else if(token[0] == "x_max" && token.size() == 2)
  {
    VecPosition lt(mConstraints.getPosLeftTop());
    VecPosition rb(mConstraints.getPosRightBottom());
    rb[0] = token[1].number();
    mConstraints.setRectPoints(lt, rb);
  }
  else if(token[0] == "y_min" && token.size() == 2)
  {
    VecPosition lt(mConstraints.getPosLeftTop());
    VecPosition rb(mConstraints.getPosRightBottom());
    rb[1] = token[1].number();
    mConstraints.setRectPoints(lt, rb);
  }
  else if(token[0] == "y_max" && token.size() == 2)
  {
    VecPosition lt(mConstraints.getPosLeftTop());
    VecPosition rb(mConstraints.getPosRightBottom());
    lt[1] = token[1].number();
    mConstraints.setRectPoints(lt, rb);
  }
  else if(token[0] == "behind_ball")
    mBehindBall = true;
  else
  {
    Logger::getSingleton()->log(33, "(PlayerPositioning) unknown option or threshold '%s'", token.str().c_str());
    return false;
  }

  return true;
}

/**
  This method loads the player information from a given s-expression token.
  @return True on successful load, false otherwise
*/
bool Klaus::PlayerPositioning::load(SToken token)
{
  int index = token.getFirstNonComment();
  if(index == -1 || token[index] != "Player")
  {
    Logger::getSingleton()->log(33, "(PlayerPositioning) unknown player information '%s'", token.str().c_str());
    return false;
  }

  index = token.getNextNonComment(index);
  if(index != -1)
    mPlayerNumber = token[(size_t)index].getAsInt();
  if(index == -1 || mPlayerNumber < 1 || mPlayerNumber > 11)
  {
    Logger::getSingleton()->log(33, "(PlayerPositioning) can not extract player number in '%s'", token.str().c_str());
    return false;
  }

  // will not break parsing in case of an unknown command
  for(size_t i = index + 1; i < token.size(); ++i)
    if(token[i].isComment() == false)
      parse(token[i]);

  return true;
}

/**
  This method returns the player number.
  @return Player's number
*/
int Klaus::PlayerPositioning::getPlayerNumber() const
{
  return mPlayerNumber;
}

/**
  This method returns the player's role in the formation.
  @return Player's role in the formation
*/
Klaus::RoleType Klaus::PlayerPositioning::getPlayerRole() const
{
  return mRole;
}

/**
  This method returns home position of the player. This is the
  position when ball is centered on the field will be returned
  for the player.
  @return Home position of the player
*/
Klaus::VecPosition Klaus::PlayerPositioning::getHomePosition() const
{
  return mHome;
}

/**
  This method returns attraction of the player to the ball
  @return Attraction of the player to the ball
*/
Klaus::VecPosition Klaus::PlayerPositioning::getAttraction() const
{
  return mAttraction;
}

/**
  This method returns constraints on the player position. The positioning
  system will not return values out of the rect specified by this parameter.
  @return Constraints on the player's position
*/
Klaus::Rect Klaus::PlayerPositioning::getConstraints() const
{
  return mConstraints;
}

/**
  This method returns whether the player shall remain
  behind ball at all time or not.
  @return Whether player shall remain behind ball or not
*/
bool Klaus::PlayerPositioning::isBehindBall() const
{
  return mBehindBall;
}

/**
  This method returns the strategic position of the player. It is
  calculated based on the ball position, home position, attraction
  and constraints on his position in the formation.
  @param posBall Position of the ball in field
  @return Strategic position of the player
*/
Klaus::VecPosition Klaus::PlayerPositioning::getStrategicPosition(const Klaus::VecPosition& posBall) const
{
  // apply home position
  VecPosition res = mHome;

  // apply attraction
  res[0] += mAttraction[0] * posBall[0];
  res[1] += mAttraction[1] * posBall[1];

  // apply behind ball
  if(mBehindBall)
    res[0] = Min(res[0], posBall[0]);

  // apply constraints
  res[0] = Max(mConstraints.getPosLeftTop()[0], res[0]);
  res[0] = Min(mConstraints.getPosRightBottom()[0], res[0]);

  res[1] = Min(mConstraints.getPosLeftTop()[1], res[1]);
  res[1] = Max(mConstraints.getPosRightBottom()[1], res[1]);

  return res;
}

/**
  This method returns a string representation of the player information
  @param gap The gap to insert before the information
  @return String representation of the player information
*/
std::string Klaus::PlayerPositioning::str(const std::string& gap) const
{
  VecPosition lt = mConstraints.getPosLeftTop();
  VecPosition rb = mConstraints.getPosRightBottom();

  std::stringstream ss;
  ss << gap;
  ss << setdecimal(2) << "(Player " << mPlayerNumber << " " << "(home " << mHome[0] << " " << mHome[1] <<
                         ") (attraction " << mAttraction[0] << " " << mAttraction[1] <<
                         ") (x_range " << lt[0] << " " << rb[0] << ") (y_range " << rb[1] << " " << lt[1] << ")";

  if(mBehindBall)
    ss << " (behind_ball)";

  ss << ")";

  return ss.str();
}




/*************************************************************************/
/*************************  Class Formation  *****************************/
/*************************************************************************/

/**
  This array contains the string representation of all formation types
*/
std::string FormationNames[] =
{
  "Initial Defensive",
  "Initial Offensive",
  "4-3-3 Offensive",
  "Unknown Formation"
};

/**
  Constructor of Formation. Resets the formation type.
*/
Klaus::Formation::Formation()
{
  mFormation = UnknownFormation;
}

/**
  Destructor of Formation. Removes all registered players.
*/
Klaus::Formation::~Formation()
{
  clear();
}

/**
  This method is used to create instances of PlayerPositioning in the Formation.
  @param iNumber The number of the player which the formation needs its instance
  @return Instance of the player's positioning data
*/
Klaus::PlayerPositioning* Klaus::Formation::getPlayerInstance(const int& iNumber) const
{
  if(iNumber < 1 || iNumber > 11)
    return NULL;

  return mPlayers[(size_t)iNumber - 1];
}

/**
  This method removes all registered players from the formation.
*/
void Klaus::Formation::clear()
{
  for(size_t i = 0; i < mPlayers.size(); ++i)
    if(mPlayers[i])
      delete mPlayers[i];

  mPlayers.clear();
  mFormation = UnknownFormation;
}

/**
  This method parses the given token for a player positioning information
  @param token The token containing the player positioning information
  @return True on successful parse, false otherwise
*/
bool Klaus::Formation::parse(SToken token)
{
  PlayerPositioning * p = new PlayerPositioning;
  if(!p->load(token))
  {
    delete p;
    return false;
  }

  // add the needed players and set them to NULL
  for(size_t i = 0; i < (size_t)p->getPlayerNumber(); ++i)
    mPlayers.push_back(NULL);

  size_t index = (size_t)(p->getPlayerNumber() - 1);
  if(mPlayers[index] == NULL)
    mPlayers[index] = p;
  else
  {
    delete mPlayers[index];
    mPlayers[index] = p;
  }

  return true;
}

/**
  This method loads the formation from the given S-expression token
  @param token Token containing the formation information
  @return True on successful load, false otherwise
*/
bool Klaus::Formation::load(SToken token)
{
  int index = token.getFirstNonComment();
  if(index == -1 || token[index] != "Formation")
  {
    Logger::getSingleton()->log(33, "(Formation) formation command not valid '%s'", token.str().c_str());
    return false;
  }

  index = token.getNextNonComment(index);
  if(index != -1)
    mFormation = getFromationFromStr(token[index].str());
  if(index == -1 || mFormation == UnknownFormation)
  {
    Logger::getSingleton()->log(33, "(Formation) can not set formation type from '%s'", token.str().c_str());
    return false;
  }

  for(size_t i = (size_t)(index+1); i < token.size(); ++i)
    if(token[i].isComment() == false)
      parse(token[i]);

  return true;
}

/**
  This method returns the formation type.
  @return Formation's type
*/
Klaus::FormationType Klaus::Formation::getFormationType() const
{
  return mFormation;
}

/**
  This method returns the strategic position of the given player
  number respected to ball.
  @param posBall Position of the ball
  @param iPlayerNumber Number of the player which his strategic position is needed
  @return Strategic position of the player with the given ball position
*/
Klaus::VecPosition Klaus::Formation::getStrategicPosition(const Klaus::VecPosition& posBall, const int& iPlayerNumber) const
{
  PlayerPositioning * p = getPlayerInstance(iPlayerNumber);
  if(p)
    return p->getStrategicPosition(posBall);

  Logger::getSingleton()->log(33,
                              "(Formation) player #%d does not exist in the formation '%s'",
                              iPlayerNumber,
                              getFormationStr(mFormation).c_str());
  return VecPosition::Unknown;
}

/**
  This method returns the string representation of the formation
  @param gap The gap to insert before the formation string
  @return The string representation of the formation
*/
std::string Klaus::Formation::str(const std::string& gap) const
{
  std::stringstream ss;
  ss << gap << "(\n" << gap << "  Formation '" << getFormationStr(mFormation) << "'\n\n";
  for(size_t i = 0; i < mPlayers.size(); ++i)
    if(mPlayers[i])
      ss << mPlayers[i]->str(gap + "  ") << "\n";
  ss << gap << ")";

  return ss.str();
}

/**
  This method finds out a string representation of the given formation enumeration
  @param t The formation type to convert to string
  @return The string representing the formation type
*/
std::string Klaus::Formation::getFormationStr(const Klaus::FormationType& t)
{
  return FormationNames[t];
}

/**
  This method gets the formation type from the given formation name string
  @param str The formation name string
  @return The formation type extracted from the string
*/
Klaus::FormationType Klaus::Formation::getFromationFromStr(const std::string& str)
{
  for(size_t i = 0; i < UnknownFormation; ++i)
    if(FormationNames[i] == str)
      return (FormationType)i;

  return UnknownFormation;
}



/*****************************************************************************/
/************************* Class FormationSystem  ****************************/
/*****************************************************************************/

/**
  This is the constructor for the FormationSystem class. Resets all data
*/
Klaus::FormationSystem::FormationSystem()
{
  mCurFormation = UnknownFormation;
  mLog = Logger::getSingleton();

  for(size_t i = 0; i < UnknownFormation; ++i)
    mFormations[i] = NULL;
}

/**
  Detructor of FormationSystem. Removes all registered formations.
*/
Klaus::FormationSystem::~FormationSystem()
{
  clear();
}

/**
  This method returns the only instance to this class
  @return singleton instance
*/
Klaus::FormationSystem * Klaus::FormationSystem::getSingleton()
{
  if(!mInstance)
  {
    mInstance = new FormationSystem;
    Logger::getSingleton()->log(1, "(Formations) created singleton");
  }

  return mInstance;
}

/**
  This method frees the only instace of the this class
*/
void Klaus::FormationSystem::freeSingleton()
{
  if(!mInstance)
    Logger::getSingleton()->log(1, "(Formations) no singleton instance");
  else
  {
    delete mInstance;
    mInstance = NULL;
    Logger::getSingleton()->log(1, "(Formations) removed singleton");
  }
}

/**
  This method clears all the registered formations.
*/
void Klaus::FormationSystem::clear()
{
  for(size_t i = 0; i < UnknownFormation; ++i)
  {
    if(mFormations[i])
      delete mFormations[i];

    mFormations[i] = NULL;
  }

  mCurFormation = UnknownFormation;
}

/**
  This method loads all formations from the given s-expression.
  @param token The token containing the S-Expression
  @return True on successful load, false otherwise
*/
bool Klaus::FormationSystem::load(SToken token)
{
  for(size_t i = 0; i < token.size(); ++i)
    if(token[i].isNormal())
    {
      Formation * f = new Formation;
      if(!f->load(token[i]))
      {
        delete f;
        continue;
      }

      size_t index = f->getFormationType();
      if(mFormations[index])
        delete mFormations[index];

      mFormations[index] = f;
    }

  return true;
}

/**
  This method initializes formations by reading all values from a file,
  setting current formation and the player number in formation
  @param strFile File name to read formation values
  @param initForm Initializing formation
  @return True on successfull startup, false otherwise
*/
bool Klaus::FormationSystem::initialize(const std::string & strFile, FormationType initForm)
{
  try
  {
    SExpression file(strFile, 4*1024);
    int index = file.getFirstNonComment();

    if(index == -1)
    {
      mLog->log(33, "(FormationSystem) no useful formation in file '%s'", strFile.c_str());
      return false;
    }

    if(!load(file[index]))
      return false;

    setCurrentFormation(initForm);
  }
  catch(SLangException e)
  {
    Logger::getSingleton()->log(33, "(FormationSystem) error loading s-expression file '%s'", strFile.c_str());
    Logger::getSingleton()->log(33, "%s", e.str());
    return false;
  }

  return true;
}

/**
  This method returns the current formation used by the agent.
  @return Current active formation
*/
Klaus::FormationType Klaus::FormationSystem::getCurrentFormation() const
{
  if(mCurFormation == -1)
    return UnknownFormation;
  else
    return (FormationType)mCurFormation;
}

/**
  This method sets the current formation used by the agent.
  @param formation The new active formation
*/
void Klaus::FormationSystem::setCurrentFormation(FormationType formation)
{
  mCurFormation = formation;
}

/**
  This method returns the strategic position for a player. It calculates this
  information by taking the home position of the current role in the
  current formation and combines this with the position of the ball using the
  attraction values for the current player type. The attraction values
  defines the percentage of the ball coordinate that is added to the home
  position of the current player type. So when the x coordindate of the home
  position is 10.0, x coordinate ball is 20.0 and x attraction is 0.25. The
  x coordinate of the strategic position will become 10.0 + 0.25*20.0 = 15.0.
  When this value is smaller than the minimal x coordinate or larger than
  the maximal x coordinate, the coordinate is changed to this minimal or
  maximal coordinate respectively. Also when the behind ball value is set,
  the x coordinate of the strategic position is set to this ball coordinate.
  Furthermore when the strategic position is in front of the supplied
  argument dMaxXInPlayMode, the x coordinate is adjusted to this value.
  During normal play mode the supplied value is often the offside line.
  @param iPlayer player number in formation of which strategic position should be determined.
  @param posBall position of the ball
  @param dMaxX Maximum x coordinate allowed in the current play mode
  @param ft Formation to check the players strategic position
  @return VecPosition Strategic position of the agent according to current state of the game
*/
Klaus::VecPosition Klaus::FormationSystem::getStrategicPosition(int iPlayer, VecPosition posBall, double dMaxX, FormationType ft)
{
  if(ft == UnknownFormation)
    return VecPosition::Unknown;

  // get the requested formation
  Formation * form = mFormations[mCurFormation];
  if(!form)
  {
    mLog->log(33, "(FormationSystem) no such formation registered: '%s'", Klaus::Formation::getFormationStr(ft).c_str());
    return VecPosition::Unknown;
  }

  // get the strategic position
  VecPosition result = form->getStrategicPosition(posBall, iPlayer);

  // when x coordinate is in front of allowed x value, change it
  result[0] = Min(dMaxX, result[0]);

  // return it.
  return result;
}

/**
  This method returns the whole formation system as a string
  @return String representation of the whole formation system
*/
std::string Klaus::FormationSystem::str() const
{
  std::stringstream ss;
  ss << "\n;\n; Zigorat formation system\n;\n\n(";

  for(size_t i = 0; i < UnknownFormation; ++i)
    if(mFormations[i])
      ss << "\n" << mFormations[i]->str("  ") << "\n";

  ss << ")\n";
  return ss.str();
}
