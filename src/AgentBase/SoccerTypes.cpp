/*
*********************************************************************************
*         SoccerTypes.cpp : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class definition for SoccerCommand              *
*            (Some materials are owned & copyrighted by Jelle Kok)              *
*                                                                               *
*********************************************************************************
*/

/** \file SoccerTypes.cpp
<pre>
<b>File:</b>          SoccerTypes.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class definition for SoccerCommand
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
</pre>
*/

/*
Copyright (c) 2000-2003, Jelle Kok, University of Amsterdam
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the University of Amsterdam nor the names of its
contributors may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "SoccerTypes.h"
#include "Objects.h"        // needed for AgentObject
#include "AgentSettings.h"  // needed for max say size

#include <iostream>       // needed for outputsteam in showcerr
#include <sstream>        // needed for stringstram
#include <iomanip>        // needed for setiosflags for formatting numebrs


/** This macro controls number of decimal digits printed along the floating point values */
#define setdecimal(x) std::setprecision(x) << std::setiosflags(std::ios::fixed|std::ios::showpoint)




/*****************************************************************************/
/**************************  Class SoccerCommand  ****************************/
/*****************************************************************************/

/**
  Constructor of SoccerCommand with no parameters.
*/
Klaus::SoccerCommand::SoccerCommand()
{
  mCommandType = UnknownCommand;
}

/**
  Constructor of SoccerCommand for say commands
  @param strSay Say message
*/
Klaus::SoccerCommand::SoccerCommand(const std::string& strSay)
{
  makeCommand(strSay);
}

/**
  Constructor of SoccerCommand for beaming to a position
  @param pos The position and angle to beam to
*/
Klaus::SoccerCommand::SoccerCommand(const Klaus::VecPosition& pos)
{
  makeCommand(pos);
}

/**
  Constructor of SoccerCommand to load a scene for the player in simspark
  @param model The model type to load in simspark
*/
Klaus::SoccerCommand::SoccerCommand(AgentModelType model)
{
  makeCommand(model);
}

/**
  This is the constructor for SoccerCommand and initilizes the command with
  information of a joint
  @param strEffector Name of the joint effector
  @param d1 Joint's first motor speed
  @param d2 Joint's second motor speed
*/
Klaus::SoccerCommand::SoccerCommand(const std::string & strEffector, double d1, double d2)
{
  makeCommand(strEffector, d1, d2);
}

/**
  Constructor of SoccerCommand with number and team name
  @param iNum requeste number of agent
  @param team_name name of the agent's team
*/
Klaus::SoccerCommand::SoccerCommand(int iNum, const std::string& team_name)
{
  makeCommand(iNum, team_name);
}

/**
  This method creates a text message from the information in soccer command
  that can be digested by server and turn into real actions
  @param strMsg Source to put the text message in
*/
bool Klaus::SoccerCommand::makeCommandString(std::string & strMsg) const
{
  std::stringstream ss;
  ss << setdecimal(2);

  switch(mCommandType)
  {
    case CommandJoint:
      ss << "(" << mEffector << " " << mSpeed1;
      if(mSpeed2 != UnknownDoubleValue)
         ss << " " << mSpeed2;
      ss << ")";
      break;

    case CommandSay:
      if(mSayStr.empty())
        return false;

      ss << "(say " << mSayStr.substr(0, AgentSettings::getSingleton()->getMaxSaySize()) << ")";
      break;

    case CommandBeam:
      if(mBeamPosition == VecPosition::Unknown)
        return false;

      ss << "(beam " << mBeamPosition[0] << " " << mBeamPosition[1] << " " << mBeamPosition[2] << ")";
      break;

    case CommandScene:
      if(mModel == UnknownModel)
        return false;

      ss << "(scene " << Klaus::SoccerTypes::getAgentModelString(mModel) << ")";
      break;

    case CommandInit:
      if(mSayStr.empty())
        return false;

      ss << "(init (unum " << Round(mSpeed1) << ") (teamname " << mSayStr << "))";
      break;

    default:
      return false;
  }

  strMsg = ss.str();
  return true;
}

/**
  This method clears the command.
*/
void Klaus::SoccerCommand::clear()
{
  mCommandType  = UnknownCommand;
  mSpeed1       = UnknownDoubleValue;
  mSpeed2       = UnknownDoubleValue;
  mBeamPosition = VecPosition::Unknown;
  mModel        = UnknownModel;
  mEffector.clear();
  mSayStr.clear();
}

/**
  This method creates a say command
  @param strSay The message to say
*/
void Klaus::SoccerCommand::makeCommand(const std::string& strSay)
{
  clear();
  mCommandType = CommandSay;
  mSayStr = strSay;
}

/**
  This method creates command from the given command type and arguments
  @param strEffector The joint's effector name
  @param d1 First joint angular speed
  @param d2 Second joint angular speed
*/
void Klaus::SoccerCommand::makeCommand(const std::string & strEffector, double d1, double d2)
{
  clear();
  mCommandType = CommandJoint;
  mEffector = strEffector;
  mSpeed1 = d1;
  mSpeed2 = d2;
}

/**
  This method creates a beam command.
  @param pos The x, y and angle to beam to
*/
void Klaus::SoccerCommand::makeCommand(const Klaus::VecPosition& pos)
{
  clear();
  mCommandType = CommandBeam;
  mBeamPosition = pos;
}

/**
  This method creates a scene command. This command will load a scene for the player in simspark.
  @param model The model type to load
*/
void Klaus::SoccerCommand::makeCommand(AgentModelType model)
{
  clear();
  mCommandType = CommandScene;
  mModel = model;
}

/**
  This method creates a init command. This command sets the agent's
  team name and number in the simulator.
  @param iNum Number of the agent
  @param team_name Name of the agent's team
*/
void Klaus::SoccerCommand::makeCommand(int iNum, const std::string& team_name)
{
  clear();
  mCommandType = CommandInit;
  mSayStr = team_name;
  mSpeed1 = iNum;
}

/**
  This method checks whether information in class are valid or not
  @return bool indicating whether information residing in class are valid
*/
bool Klaus::SoccerCommand::isIllegal()
{
  return mCommandType == UnknownCommand;
}

/**
  This operator logs internal information of joint to specified output
  @param os output stream to log information to
  @param cmd command to log information to os
  @return output stream
*/
std::ostream & operator << (std::ostream &os, Klaus::SoccerCommand cmd)
{
  os << cmd.str();
  return os;
}

/**
  This method creates a command string to be show
  @return Command as a readable string
*/
std::string Klaus::SoccerCommand::str() const
{
  std::string str;
  if(makeCommandString(str))
    return "(failed to create command)";
  else
    return str;
}

/**
  This method return the command to as a message that server can understand
  @param strCmd destination to write server message to
  @return bool indicating whether message is valid or not
*/
bool Klaus::SoccerCommand::getCommandString(std::string & strCmd) const
{
  return makeCommandString(strCmd);
}

/**
  Overloaded assignment operator for SoccerCommands.
  @param cmd The instance to copy information from
  @return The self instance
*/
const Klaus::SoccerCommand& Klaus::SoccerCommand::operator=(const Klaus::SoccerCommand& cmd)
{
  mBeamPosition = cmd.mBeamPosition;
  mCommandType  = cmd.mCommandType;
  mEffector     = cmd.mEffector;
  mModel        = cmd.mModel;
  mSayStr       = cmd.mSayStr;
  mSpeed1       = cmd.mSpeed1;
  mSpeed2       = cmd.mSpeed2;

  return *this;
}




/*****************************************************************************/
/****************************  Class SoccerTypes  ****************************/
/*****************************************************************************/

/**
  These constants defines the string names corresponding to the
  ObjectType names, defined in SoccerTypes.h. Players are not added
  since they depend on the team name. Note that the order is
  important, since the names are in the same order as the ObjectType
  enumeration.
*/
const std::string FlagNames[] =
{
  "(goal l t)",     /*!< Left goal flag, top of the field */
  "(goal l b)",     /*!< Left goal flag, bottom of the field */
  "(goal r t)",     /*!< Right goal flag, top of the field */
  "(goal r b)",     /*!< Right goal flag, bottom of the field */
  "(corner l t)",   /*!< Left corner flag, top of the field */
  "(corner l b)",   /*!< Left corner flag, bottom of the field */
  "(corner r t)",   /*!< Right corner flag, top of the field */
  "(corner r b)",   /*!< Right cornder flag, bottom of the field */
  "(flag ?)"        /*!< unknown flag */
};

/**
  These are the string names of agent models
*/
const std::string AgentModelNames[] =
{
  "rsg/agent/leggedsphere.rsg",             /*!< legged sphere model */
  "rsg/agent/soccerbot055.rsg",             /*!< soccerbot 0.5.5 model */
  "rsg/agent/soccerbot056.rsg",             /*!< soccerbot 0.5.6 model */
  "rsg/agent/soccerbot058/soccerbot.rsg",   /*!< soccerbot 0.5.8 model */
  "rsg/agent/nao/nao.rsg",                  /*!< Nao model */
  "?!"                                      /*!< unknown model */
};

/**
  These constants define all of the strings that can mean a playmode
*/
const std::string PlayModeNames[] =
{
  "BeforeKickOff",
  "KickOff_Left",
  "KickOff_Right",
  "PlayOn",
  "KickIn_Left",
  "KickIn_Right",
  "corner_kick_left",
  "corner_kick_right",
  "goal_kick_left",
  "goal_kick_right",
  "offside_left",
  "offside_right",
  "quit",
  "Goal_Left",
  "Goal_Right",
  "free_kick_left",
  "free_kick_right",
  "unknown_play_mode"
};

/**
  These constants represent all of the strings that mean a side info
*/
const std::string SideNames[] =
{
  "our",        /*!< Our side */
  "left",       /*!< Left side */
  "right",      /*!< Right side */
  "illegal"     /*!< Unknown */
};

/**
  These constants represent all fall states enumerations in strings
*/
const std::string BalanceStateNames[] =
{
  "fallen back",      /*!< Fallen back */
  "fallen front",     /*!< Fallen front */
  "fallen left",      /*!< Fallen left */
  "fallen right",     /*!< Fallen right */
  "normal"            /*!< Normal balance state */
};

/**
  This method returns the string that corresponds to a specific object. This
  string name is exactly the same as the (short) name of the RoboCup
  Simulation.
  @param o ObjectType that has to be converted to the string representation
  @return contains the string representation of the specified object
*/
std::string Klaus::SoccerTypes::getObjectStr(ObjectType o)
{
  std::stringstream ss;

  if(o == ObjectBall)
    return "(ball)";
  else if(o <= ObjectFlagUnknown)
    return FlagNames[getIndex(o)];
  else if(isKnownPlayer(o) && isTeammate(o))
    ss << "(teammate #" << getIndex(o) + 1 << ")";
  else if(isKnownPlayer(o) && isOpponent(o))
    ss << "(opponent #" << getIndex(o) + 1 << ")";
  else if(o == ObjectPlayerUnknown)
    return "(player ?)";
  else
    ss << "(? object " << o << ")";

  return ss.str();
}

/**
  This method returns an ObjectType that corresponds to the string passed as
  the first argument. The string representation equals the representation
  used in the Soccer Server. Format is with parenthesis, so possible
  arguments for str are (ball), (p Team_L 1), etc
  @param str pointer to string containing string representation of object
  @return return the corresponding ObjectType, UnknownObject in case of error
*/
Klaus::ObjectType Klaus::SoccerTypes::getObjectFromStr(const std::string & str)
{
  ObjectType o = UnknownObject;
  int size = str.size();
  if(size == 0)
    return UnknownObject;

  switch(str[0])
  {
    case 'b':                       // (ball)
    case 'B':                       // (B) in case of ball very close
       o = ObjectBall; break;
    case 'F':
          if(size < 3)
            return UnknownObject;

          if(str[1] == '1')    // F1L
            o = (str[2] == 'L') ? ObjectFlagCLT : ObjectFlagCRT;
          else
          if(str[1] == '2')    // F2L
            o = (str[2] == 'L') ? ObjectFlagCLB : ObjectFlagCRB;
          break;

    case 'G':
          if(size < 3)
            return UnknownObject;

          if(str[1] == '1')    // G1L
            o = (str[2] == 'L') ? ObjectFlagGLT : ObjectFlagGRT;
          else
          if(str[1] == '2')    // G2L
            o = (str[2] == 'L') ? ObjectFlagGLB : ObjectFlagGRB;
          break;

  case 'T': //
  case 't': //
    {
      if(size < 4)
        return UnknownObject;

      switch(str[3])     // get team number
      {
        case '1': if(str[4] == '0')
                    o = ObjectTeammate10;
                  else if(str[4] == '1')
                    o = ObjectTeammate11;
                  else
                    o = ObjectTeammate1;
                  break;     // and find team member

        case '2':  o = ObjectTeammate2;  break;
        case '3':  o = ObjectTeammate3;  break;
        case '4':  o = ObjectTeammate4;  break;
        case '5':  o = ObjectTeammate5;  break;
        case '6':  o = ObjectTeammate6;  break;
        case '7':  o = ObjectTeammate7;  break;
        case '8':  o = ObjectTeammate8;  break;
        case '9':  o = ObjectTeammate9;  break;
        case 'o':  o = ObjectTeammateUnknown;
        case 'g':  o = ObjectTeammateGoalie;
        default: o = UnknownObject;
      }
    }
    break;
  case 'O': //
  case 'o': //
    {
      if(size < 4)
        return UnknownObject;

      switch(str[3])     // get team number
      {
        case '1': if(str[4] == '0')
                    o = ObjectOpponent10;
                  else if(str[4] == '1')
                    o = ObjectOpponent11;
                  else
                    o = ObjectOpponent1;
                  break;     // and find team member

        case '2':  o = ObjectOpponent2;  break;
        case '3':  o = ObjectOpponent3;  break;
        case '4':  o = ObjectOpponent4;  break;
        case '5':  o = ObjectOpponent5;  break;
        case '6':  o = ObjectOpponent6;  break;
        case '7':  o = ObjectOpponent7;  break;
        case '8':  o = ObjectOpponent8;  break;
        case '9':  o = ObjectOpponent9;  break;
        case 'o':  o = ObjectOpponentUnknown;
        case 'g':  o = ObjectOpponentGoalie;
        default: o = UnknownObject;
      }
    }
  default:
    Logger::getSingleton()->log(39, "(SoccerTypes) Unknown object string: '%s'", str.c_str());
    o = UnknownObject;
    break;
  }

  return o;
}

/**
  This method returns the index of an object relative to the first object
  in that set.
  The index is always 1 smaller than its number, so ObjectOpponent1
  will become 0. This can be used for indexing an array of objects.
  @param o ObjectType type of object of which the index has to be calculated
  @return index of object or -1 when o was not a correct object
*/
int Klaus::SoccerTypes::getIndex(ObjectType o)
{
  if(o == ObjectBall)
    return 0;
  else if(o <= ObjectFlagUnknown)
    return o - 1;
  else if(o >= ObjectOpponent1 && o <= ObjectOpponent11)
    return o - ObjectOpponent1;
  else if(o >= ObjectTeammate1 && o <= ObjectTeammate11)
    return o - ObjectTeammate1;
  else
    return -1;
}

/**
  This method gets the player number from the given player object type
  @param o player object type
  @return Player number
*/
int Klaus::SoccerTypes::getPlayerNumber(ObjectType o)
{
  if(o >= ObjectTeammate1 && o <= ObjectTeammate11)
    return o - ObjectTeammate1 + 1;
  else if(o >= ObjectOpponent1 && o <= ObjectOpponent11)
    return o - ObjectOpponent1 + 1;
  else
    return -1;
}

/**
  This method returns the object type of a teammate with the given player
  number. When iNum equals 4 for example ObjectTeammate4 is returned.
  @param iNum index of teammate range is [1..11]
  @return object type corresponding to this index
*/
Klaus::ObjectType Klaus::SoccerTypes::getTeammateTypeFromNumber(int iNum)
{
  return (ObjectType)(ObjectTeammate1 + iNum - 1);
}

/**
  This method returns the object type of an opponent with the given player
  number. When iNum equals 9 for example ObjectOpponent9 is returned.
  @param iNum index of opponent range is [1..11]
  @return object type corresponding to this index
*/
Klaus::ObjectType Klaus::SoccerTypes::getOpponentTypeFromNumber(int iNum)
{
  return (ObjectType)(ObjectOpponent1 + iNum - 1);
}

/**
  This method returns a boolean indicating whether the object o is
  part of the object set o_s. ObjectTeammate1 as o and
  Teammates will return for example the value true.
  @param o ObjectType of which should be checked whether it is a part of o_s
  @param o_g ObjectSetT to check for object is in
  @param objGoalie ObjectSet in which o should be
  @return true when o is included in set o_g, false otherwise
*/
bool Klaus::SoccerTypes::isInSet(ObjectType o, ObjectSetType o_g, ObjectType objGoalie)
{
  switch(o_g)
  {
    case Flags:
      return isFlag(o);

    case Teammates:
      return isTeammate(o) && isKnownPlayer(o);

    case Opponenents:
      return isOpponent(o) && isKnownPlayer(o);

    case Players:
      return isPlayer  (o) && isKnownPlayer(o);

    case TeammatesNoGoalie:
      return isTeammate(o) && isKnownPlayer(o) && o != objGoalie;

    case OpponentsNoGoalie:
      return isOpponent(o) && isKnownPlayer(o) && o != objGoalie;

    default:
      break;
  }

  return false;
}

/**
  This method determines whether object o is a teammate
  @param o an object type
  @return bool indicating whether o is a teammate (true) or not (false)
*/
bool Klaus::SoccerTypes::isTeammate(ObjectType o)
{
  return (o >= ObjectTeammate1 && o <= ObjectTeammateUnknown);
}

/**
  This method determines whether object o is an opponent
  @param o an object type
  @return bool indicating whether o is an opponent (true) or not (false)
*/
bool Klaus::SoccerTypes::isOpponent(ObjectType o)
{
  return (o >= ObjectOpponent1 && o <= ObjectOpponentUnknown);
}

/**
  This method determines whether object o is a player without checking
  whether its number or side is available.
  @param o an object type
  @return bool indicating whether o is a known player (true) or not (false)
*/
bool Klaus::SoccerTypes::isPlayer(ObjectType o)
{
  return isKnownPlayer(o)           || o == ObjectTeammateUnknown ||
         o == ObjectOpponentUnknown || o == ObjectPlayerUnknown;
}

/**
  This method determines whether object o is a known player, thus
  containing a number
  @param o an object type
  @return bool indicating whether o is a known player (true) or not (false)
*/
bool Klaus::SoccerTypes::isKnownPlayer(ObjectType o)
{
  return (o >= ObjectOpponent1 && o <= ObjectOpponent11) ||
         (o >= ObjectTeammate1 && o <= ObjectTeammate11);
}

/**
  This method determines whether object o is a goalie = teammate number is 1
  (for now)
  @param o an object type
  @return bool indicating whether o is a goalie (true) or not (false)
*/
bool Klaus::SoccerTypes::isGoalie(ObjectType o)
{
  return o == ObjectTeammate1 || o == ObjectOpponent1;
}

/**
  This method determines whether object o is the in set of flags or not
  @param o an object type
  @return bool indicating whether o is the flag (true) or not (false)
*/
bool Klaus::SoccerTypes::isFlag(ObjectType o)
{
  return o > ObjectBall && o < ObjectFlagUnknown;
}

/**
  This method determines whether object o is in set of goals or not
  @param o an object type
  @return bool indicating whether o is the goal (true) or not (false)
*/
bool Klaus::SoccerTypes::isGoal(ObjectType o)
{
  return o >= ObjectFlagGLT && o <= ObjectFlagGRB;
}

/**
  This method determines whether object o is the ball
  @param o an object type
  @return bool indicating whether o is the ball (true) or not (false)
*/
bool Klaus::SoccerTypes::isBall(ObjectType o)
{
  return o == ObjectBall;
}

/**
  This method returns the string representation of a PlayMode as is used
  in the Robocup Soccer Simulation and also said by the referee.
  @param pm PlayModeT which should be converted
  @return  pointer to the string (enough memory has to be allocated)
*/
const std::string Klaus::SoccerTypes::getPlayModeStr(PlayMode pm)
{
  if(pm < UnknownPlayMode)
    return PlayModeNames[(int)pm];
  else
    return PlayModeNames[UnknownPlayMode];
}

/**
  This method parses the string given for a playmode message and extracts
  it to a PlayModeT enumeration
  @param str String that contains playmode information
  @return PlayModeT PlayMode residing in the given string
*/
Klaus::PlayMode Klaus::SoccerTypes::getPlayModeFormStr(const std::string & str)
{
  for(unsigned i = 0; i < UnknownPlayMode; ++i)
    if(str.compare(0, PlayModeNames[i].size(), PlayModeNames[i]) == 0)
      return PlayMode(i);

  return UnknownPlayMode;
}

/**
  This method returns the string representation of a SideT as is used in the
  Robocup Soccer Simulation (r or l).
  @param s SideT which should be converted
  @return pointer to the string
*/
const std::string Klaus::SoccerTypes::getSideStr(SideType s)
{
  return SideNames[s];
}

/**
  This method returns the SideT from the string that is passed as the first
  argument.
  @param str pointer to a string that contains side info string at index 0
  @return SideT of string representation, SIDE_ILLEGAL if it is not known
*/
Klaus::SideType Klaus::SoccerTypes::getSideFromStr(const std::string & str)
{
  if(str.empty())
    return UnknownSide;

  if(str[0] == 'l')
    return SideLeft;
  else if(str[0] == 'r')
    return SideRight;
  else if(str[0] == 'o')
    return SideUs;
  else
    return UnknownSide;
}

/**
  This method converts balance state to a string and return it
  @param state The balance state to get it's string name
  @return String name of the balance state
*/
const std::string Klaus::SoccerTypes::getBalanceStateStr(BalanceState state)
{
  return BalanceStateNames[state];
}

/**
  This method converts a boolean to a string
  @param b boolean value
  @return Converted value of the boolean to string
*/
const std::string Klaus::SoccerTypes::getBoolString(const bool & b)
{
  return b ? "true" : "false";
}

/**
  This method returns the string equivelant of the agent model passed
  @param am AgentModel to get its string equivelant
  @return String containing AgentModel
*/
const std::string Klaus::SoccerTypes::getAgentModelString(const AgentModelType & am)
{
  return AgentModelNames[(int)am];
}

/**
  This method parses the given string for an agent model and returns its enum
  @param str String containing Agent Model
  @return Agent Model parsed from the given string
*/
Klaus::AgentModelType Klaus::SoccerTypes::getAgentModelFromString(const std::string & str)
{
  for(size_t i = 0; i < UnknownModel; ++i)
    if(str == AgentModelNames[i])
      return (AgentModelType)i;

  return UnknownModel;
}

/**
  This method checks if the supplied agent model is one of the soccer bot models
  @param am The model name
  @return True if the model is soccerbot, false otherwise
*/
bool Klaus::SoccerTypes::isModelSoccerBot(const AgentModelType & am)
{
  if(am == SoccerBot055 || am == SoccerBot056 || am == SoccerBot058)
    return true;
  else
    return false;
}

/**
  This method returns the confidence value of an object based on the
  time of it's previous information and current time of simulator
  @param t_old Last time object had it's own information
  @param t_new Current time of play
  @return The confidence value of the object
*/
double Klaus::SoccerTypes::getConfidence(Time t_old, Time t_new)
{
  return 1.0 - Min(t_new - t_old, 1.0);
}
