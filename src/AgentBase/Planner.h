/*
*********************************************************************************
*             Planner.h : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 03/16/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Class decleration for JointPlanner, which is used to                    *
*            run simultanous agent plans                                        *
*                                                                               *
*********************************************************************************
*/

/*! \file Planner.h
<pre>
<b>File:</b>          Planner.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/16/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Class decleration for JointPlanner, which is used to run simultanous
             agent plans
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/16/2008        Mahdi           Initial version created
</pre>
*/

#ifndef _PLANNER_
#define _PLANNER_

#include <deque>
#include <string>

#include "ActHandler.h"
#include "SoccerTypes.h"
#include <libZigorat/Logger.h>
#include "WorldModel.h"

using namespace std;



/***********************************************************************************************/
/************************************  CLASS JointPlanPart  ************************************/
/***********************************************************************************************/

/*! This class represents a joint plan detail for agent. A JointPlan represents a change in a joint
    to the final angles desired within a duration relative to the start time of the joint change.
*/
class JointPlan
{
  protected:
            RSG::SceneJoint*   m_Joint;          /*!< Joint to change                           */
            AngDeg        m_FinalAngle1;    /*!< Final position of first joint angle       */
            AngDeg        m_FinalAngle2;    /*!< Final position of second joint angle      */
            double        m_StartTime;      /*!< Start time, relative to start of the plan */
            double        m_Duration;       /*!< Transformation duration                   */

  public:
    typedef vector<JointPlan> List;         /*!< A dynamic array of joint plans            */

                          JointPlan         (                                              );
    virtual             ~ JointPlan         (                                              );

    const   RSG::SceneJoint*   getJoint          (                                              ) const;
            void          setJoint          ( const string & strPerceptor                  );

            AngDeg        getFinalAngle1    (                                              ) const;
            void          setFinalAngle1    ( AngDeg ang                                   );

            AngDeg        getFinalAngle2    (                                              ) const;
            void          setFinalAngle2    ( AngDeg ang                                   );

            double        getStartTime      (                                              ) const;
            void          setStartTime      ( double st                                    );

            double        getDuration       (                                              ) const;
            void          setDuration       ( double dur                                   );

            bool          shouldBeExecuted  ( Time timeNow                                 ) const;
            SoccerCommand makeCommand       ( Time timeNow                                 ) const;
            void          log               ( int iLogLevel                                );

    friend  ostream& operator <<            ( ostream &os, JointPlan   p                   );
    friend  istream& operator >>            ( istream &is, JointPlan & p                   );
};





/***********************************************************************************************/
/*************************************  CLASS JointPlan  ***************************************/
/***********************************************************************************************/

/*! This class represents a movement plan for agent. A plan is a set of changes in all agent
    joints which if done simulatnously and correctly, will lead to a particular action for
    agent */
class MovementPlan
{
  private:
                           JointPlan::List m_JointPlans;         /*!< A set JointPlanParts to run                       */

  protected:
                           Time          m_StartTime;            /*!< Starting time of the simulator for this plan      */
                           ActHandler  * ACT;                    /*!< ActHandler instance to send commands to simulator */

    virtual  bool          isExecutable            (  Time currentTime = UnknownTime              ) const;

  public:
                           MovementPlan            (                                              );
    virtual              ~ MovementPlan            (                                              );

             void          loadFromFile            ( string filename                              );

             void          addJointPlan            ( const JointPlan & plan                       );
    virtual  void          startExecution          (                                              );
    virtual  bool          execute                 (                                              );
    virtual  bool          executing               (                                              ) const;
    virtual  void          restart                 (                                              );
    virtual  void          show                    ( ostream & os                                 );
};


#endif
