/*
*********************************************************************************
*           ConvexHull.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 03/17/2010                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Class decleration for ConvexHull which handles generating convex   *
*            hull and it's related functions.                                   *
*                                                                               *
*********************************************************************************
*/

/*! \file ConvexHull.h
<pre>
<b>File:</b>          ConvexHull.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/17/2010
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Class decleration for ConvexHull which handles generating convex
            hull and it's related functions.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/17/2010       Mahdi           Initial version created
</pre>
*/

#ifndef CONVEX_HULL
#define CONVEX_HULL


#include <libZigorat/Geometry.h> // needed for VecPosition


namespace Klaus
{

  /*! This class handles convex hulls. It can generate a convex hull from a given set
      of points and checks whether an arbitrary point is in the hull or out of it.
  */
  class ConvexHull
  {
    private:
      VecPosition::Array m_AllPoints;         /*!< The points to create the hull    */
      VecPosition::Array m_ConvexPoints;      /*!< Generated convex points lie here   */

      bool               deriveStartingPoint  ( VecPosition::Array & points,
                                                VecPosition::Array & target ) const;
      bool               deriveNextPoint      ( VecPosition::Array & points,
                                                VecPosition::Array & targer ) const;

    public:
                         ConvexHull           (                             );
	                     ~ ConvexHull           (                             );

      void               clear                (                             );
      void               addPoint             ( const VecPosition & pos     );
      bool               calculateHull        ( VecPosition::Array*target=0 );

      bool               isPointWithinHull    ( const VecPosition & pos     );

      VecPosition::Array::const_iterator begin(                             ) const;
      VecPosition::Array::const_iterator end  (                             ) const;
  };

}; // end namespace Klaus;

#endif // CONVEX_HULL
