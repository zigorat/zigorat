#include "Skills.h"
#include "Objects.h"  // needed for AgentObject


/**
  Constructor of Skill. Initializes singleton instances
  and creates all needed PIDControllers.
*/
Klaus::Skill::Skill()
{
  mLog = Logger::getSingleton();
  mAct = ActHandler::getSingleton();
  mWM = WorldModel::getSingleton();
  mGraph = NULL;


  // create the pid controllers
  mPIDs["hj1"] = new PIDController;
  mPIDs["hj2"] = new PIDController;


  mPIDs["laj1"] = new PIDController;
  mPIDs["laj2"] = new PIDController;
  mPIDs["laj3"] = new PIDController;
  mPIDs["laj4"] = new PIDController;

  mPIDs["raj1"] = new PIDController;
  mPIDs["raj2"] = new PIDController;
  mPIDs["raj3"] = new PIDController;
  mPIDs["raj4"] = new PIDController;


  mPIDs["llj1"] = new PIDController;
  mPIDs["llj2"] = new PIDController;
  mPIDs["llj3"] = new PIDController;
  mPIDs["llj4"] = new PIDController;
  mPIDs["llj5"] = new PIDController;
  mPIDs["llj6"] = new PIDController;

  mPIDs["rlj1"] = new PIDController;
  mPIDs["rlj2"] = new PIDController;
  mPIDs["rlj3"] = new PIDController;
  mPIDs["rlj4"] = new PIDController;
  mPIDs["rlj5"] = new PIDController;
  mPIDs["rlj6"] = new PIDController;
}

/**
  This method evaluates joint speeds from all calculated joint
  angles and sends them via the action handler.
*/
void Klaus::Skill::sendActions()
{
  for(PIDMap::iterator it = mPIDs.begin(); it != mPIDs.end(); ++it)
  {
    const RSG2::Joint * joint = mWM->getJoint(it->first);
    AngDeg angTurn = Max(Min(joint->getMaxAngle(), mState[it->first].mAngles[0]), joint->getMinAngle());
    angTurn = it->second->calculateNeededMV(joint->getAngle(), angTurn, mWM->getTimeStep());
    mAct->addCommand(SoccerCommand(joint->getEffector()->getName(), Deg2Rad(angTurn)));
  }
}

/**
  This method is called to initialize derived skill.
*/
void Klaus::Skill::initializeInternal()
{
}

/**
  This method initializes skill by getting the instance of agent scene
  graph and initializing the agent state. After that the skill's own
  initialization procedure will be called.
*/
void Klaus::Skill::initialize()
{
  mGraph = AgentObject::getSingleton()->getSceneGraph();
  mGraph->getInverseKinematics(mState);

  initializeInternal();
}

/**
  This method executes the skill. First it updates the internal state of robot and
  then the skill's operation will be done. In the end all the joint changes will be
  sent to simulator.
*/
void Klaus::Skill::execute()
{
  mGraph->getInverseKinematics(mState);
  executeInternal();
  sendActions();
}




/**
  Initializes the walk skill by getting the length of shank and thigh.
*/
void Klaus::Walk::initializeInternal()
{
  mPeriod = 0.6;
  mGaitLength = 0.35;
  mGaitHeight = 0.13;

  mThighLength = mState["llj3"].mPivot.getDistanceTo(mState["llj4"].mPivot);
  mShankLength = mState["llj4"].mPivot.getDistanceTo(mState["llj5"].mPivot);
  mHip[2] = mShankLength + mThighLength - 0.05;
}

void Klaus::Walk::executeInternal()
{
  // get the current state of robot
  mState["laj1"].mAngles[0] = -90;
  mState["raj1"].mAngles[0] = -90;

  VecPosition hip, foot, sol;
  AngDeg angKnee, angHip, angFoot, angSol, angPhase;
  int sign;
  Klaus::Plane p;
  Line3 l;
  AngDeg angTorso = 10;

  // left foot
  // find the phase
  angPhase = 2 * M_PI * mWM->getTime() / mPeriod;

  // foot position
  foot[0] = 0.0;
  foot[1] = -mGaitLength / 2.0 * cos(angPhase);
  foot[2] = Max(mGaitHeight / 2.0 * sin(angPhase), 0);
  sign = foot[1] >= 0 ? +1 : -1;

  // hip position
  hip = mHip;

  // knee angle
  angKnee = Triangle::getAngle(mThighLength, mShankLength, hip.getDistanceTo(foot)) - 180.0;

  // hip solution
  p.setPlane(VecPosition(0, 0, 1), foot);
  l.makeLineFromVectorAndPoint(VecPosition(0, 0, -1), hip);
  l.getIntersectionWithPlane(p, sol);
  angSol = Triangle::getAngle(sol.getDistanceTo(hip), hip.getDistanceTo(foot), foot.getDistanceTo(sol));

  // hip angle
  angHip = Triangle::getAngle(mThighLength, hip.getDistanceTo(foot), mShankLength) + sign * angSol + angTorso;

  // ankle angle
  angFoot = -angKnee - angHip;

  // side angles
  mState["llj3"].mAngles[0] = angHip;
  mState["llj4"].mAngles[0] = angKnee;
  mState["llj5"].mAngles[0] = angFoot;



  // right foot
  // find the phase
  angPhase = 2 * M_PI * mWM->getGameTime() / mPeriod - M_PI;

  // foot position
  foot[0] = 0.0;
  foot[1] = -mGaitLength / 2.0 * cos(angPhase);
  foot[2] = Max(mGaitHeight / 2.0 * sin(angPhase), 0);
  sign = foot[1] >= 0 ? +1 : -1;

  // hip position
  hip = mHip;

  // knee angle
  angKnee = Triangle::getAngle(mThighLength, mShankLength, hip.getDistanceTo(foot)) - 180.0;

  // hip solution
  p.setPlane(VecPosition(0, 0, 1), foot);
  l.makeLineFromVectorAndPoint(VecPosition(0, 0, -1), hip);
  l.getIntersectionWithPlane(p, sol);
  angSol = Triangle::getAngle(sol.getDistanceTo(hip), hip.getDistanceTo(foot), foot.getDistanceTo(sol));

  // hip angle
  angHip = Triangle::getAngle(mThighLength, hip.getDistanceTo(foot), mShankLength) + sign * angSol + angTorso;

  // ankle angle
  angFoot = -angKnee - angHip;

  // apply inverse kinematics
  mState["rlj3"].mAngles[0] = angHip;
  mState["rlj4"].mAngles[0] = angKnee;
  mState["rlj5"].mAngles[0] = angFoot;
}
