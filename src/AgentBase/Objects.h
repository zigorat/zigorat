/*
*********************************************************************************
*            Objects.h : Robocup 3D Soccer Simulation Team Zigorat              *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains Objects decleration which represent soccer      *
*            players, flags, etc.                                               *
*                                                                               *
*********************************************************************************
*/

/*! \file Objects.h
<pre>
<b>File:</b>          Objects.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains Objects decleration which represent soccer
               players, flags, etc.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
</pre>
*/

#include <libZigorat/Geometry.h>     // needed for VecPosition
#include <libZigorat/Logger.h>       // needed for Logger
#include <SceneGraphSystem/Graph.h>  // needed for RSG2::SceneGraph
#include "SoccerTypes.h"             // needed for ObjectType

#ifndef SOCCER_OBJECTS
#define SOCCER_OBJECTS


namespace Klaus
{


  /*! Class Object contains RoboCup information that is available for all
      objects in the simulation. Update of an object (or one of the subclasses)
      happens by calling the standard get and set methods available in these
      classes. Calculations on these attributes do not occur in these classes,
      but in the update methods of the WorldModel.
  */
  class Object
  {
    protected:
      VecPosition  mRelativePosition;       /*!< Relative posititon of this object */
      VecPosition  mGlobalPosition;         /*!< Global posititon of this object */
      Time         mTimeLastSeen;           /*!< The last time the object was seen */
      Time         mTimeGlobalPos;          /*!< The time which global position was last set */
      ObjectType   mType;                   /*!< Type of the object */

    public:
                   Object                   (                             );
      virtual    ~ Object                   (                             );

      VecPosition  getGlobalPosition        (                             ) const;
      virtual void setGlobalPosition        ( const VecPosition & newPos,
                                              const Time & time           );

      VecPosition  getRelativePosition      (                             ) const;
      virtual void setRelativePosition      ( const VecPosition & newPos,
                                              const Time & time           );

      Time         getTimeLastGlobalPosition(                             ) const;
      Time         getTimeLastSeen          (                             ) const;
      double       getConfidence            ( const Time & time           ) const;

      ObjectType   getType                  (                             ) const;
      void         setType                  ( const ObjectType & obj_type );
  };


  /*! Class FlagObject contains information for flags. This includes their global
      position and their relative 'seen' positions. Each flag has a unique identifier
      representing its names.
  */
  class Flag:public Object
  {
    public:
                 Flag (                        );
      virtual  ~ Flag (                        );

      /*! Array is a dynamic array of Flags*/
      typedef std::map<ObjectType, Flag*> Map;
  };


  /*! Class DynamicObject contains information for movable objects. Including all
      players and the ball. This class adds position and velocity and the last
      time they had their attributes calculated.
  */
  class DynamicObject: public Object
  {
    private:
      Time         mTimeLastVelocity; /*!< Needed to calculate linear velocity */
      VecPosition  mLastPosition;     /*!< Last seen position of the object. Needed to calculate velocity */
      VecPosition  mLastVelocity;     /*!< Last calculated velocity of the object */

    protected:
      VecPosition  mVelocity;         /*!< Current linear velocity of object */
      VecPosition  mAccelaration;     /*!< Current linear acceleration of object  */

    public:
                   DynamicObject  (                  );
      virtual    ~ DynamicObject  (                  );

      VecPosition  getVelocity    (                  ) const;
      VecPosition  getAcceleration(                  ) const;

      virtual void update         (                  );
  };


  /*! This class represents the ball. Holds the values for ball mass
      and its radius.
  */
  class Ball: public DynamicObject , public Singleton<Ball>
  {
    protected:
      double        mMass;       /*!< Mass of the ball */
      double        mRadius;     /*!< Radius of the ball */

                    Ball         (                      );
      virtual     ~ Ball         (                      );
    public:
      static Ball * getSingleton (                      );
      static void   freeSingleton(                      );

      double        getMass      (                      ) const;
      void          setMass      ( const double & value );

      double        getRadius    (                      ) const;
      void          setRadius    ( const double & value );
  };


  /*! Class Player contains information for all players in field, whether
      teammates or opponents. Includes their team side, uniform number and their
      orientation in field.
  */
  class PlayerObject:public DynamicObject
  {
    protected:
      int                    mNumber;       /*!< Number of the player in his team */
      SideType               mTeamSide;     /*!< Team side of the player */
      Quaternion             mOrientation;  /*!< Player's oritentation */
      RSG2::ObjectState::Map mBodyParts;    /*!< Player's body parts */

    public:
                             PlayerObject   (                             );
                           ~ PlayerObject   (                             );

      int                    getPlayerNumber(                             ) const;
      void                   setPlayerNumber( const int & num             );

      Quaternion             getOrientation (                             ) const;
      void                   setOrientation ( const Quaternion & q        );

      SideType               getTeamSide    (                             ) const;
      void                   setTeamSide    ( const SideType & newSide    );

      VecPosition            getBodyPart    ( const std::string & strName ) const;
      void                   setBodyPart    ( const std::string & strName,
                                              const VecPosition & pos     );

      /*! Map is a dynamic mapping between player numbers and their instances */
      typedef std::map<int, PlayerObject*> Map;
  };


  /*! Class AgentObject contains information of the agent. This includes the agent's
      scene graph and model name.
  */
  class AgentObject: public PlayerObject, public Singleton<AgentObject>
  {
    protected:
      // Attributes of the agent
      AgentModelType       mAgentModel;     /*!< Agent Robot Model */
      RSG2::Graph        * mAgentScene;     /*!< Agent Models SceneGraph */

      // Instance shortcuts
      Logger             * mLog;            /*!< Logger instance to write logs to */

      // Agent model related methods and functions
      void                 clearAgentModel  (                      );

      // Singleton pattern operations
                           AgentObject      (                      );
                         ~ AgentObject      (                      );

    public:
      static AgentObject * getSingleton     (                      );
      static void          freeSingleton    (                      );

      // Update methods
      void                 update           (                      );

      // Agent model
      AgentModelType       getAgentModel    (                      ) const;
      bool                 setAgentModel    ( AgentModelType am    );

      // Nodes
      RSG2::Graph         * getSceneGraph   (                      );
  };


}; // end namespace Klaus

#endif // SOCCER_OBJECTS
