/*
*********************************************************************************
*          Trajectory.cpp : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 03/07/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: class declarations for Trajectories.                               *
*                                                                               *
*********************************************************************************
*/

/*! \file Trajectory.cpp
<pre>
<b>File:</b>          Trajectory.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/07/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      class declarations for Trajectories.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/07/2011       Mahdi           Initial version created
</pre>
*/

#include "Trajectory.h"


/***************************************************************************/
/**************************  Class Trajectory  *****************************/
/***************************************************************************/

/**
  Construtor of the Trajectory. Initializes default values
*/
Klaus::Trajectory::Trajectory()
{
  mRange = 0.0;
}

/**
  Constructor of Trajectory. Creates the trajectory with the given values
  @param p Starting position on of the projectile on the trajectroy
  @param q Orientation of the trajectory
  @param range Maximum range of the trajectory
*/
Klaus::Trajectory::Trajectory(const Klaus::VecPosition& p, const Klaus::Quaternion& q, const double& range)
{
  mStartPos = p;
  mOrientation = q;
  mRange = range;
}

/**
  This method sets the parameters of the trajectory
  @param p Starting position of the trajectory
  @param q Orientation of the trajectory
  @param range Maximum range of the trajectory
*/
void Klaus::Trajectory::setTrajectory(const Klaus::VecPosition& p, const Klaus::Quaternion& q, const double& range)
{
  mStartPos = p;
  mOrientation = q;
  mRange = range;

  postUpdateInternal();
}


/**
  This method returns the range of the trajectory
  @return Range of the trajectory
*/
double Klaus::Trajectory::getRange() const
{
  return mRange;
}

/**
  This method sets the range of the trajectory
  @param range The range of the trajectory
*/
void Klaus::Trajectory::setRange(const double& range)
{
  mRange = range;
  postUpdateInternal();
}

/**
  This method returns the orientation of the trajectory
  @return Orientation of the trajectory
*/
Klaus::Quaternion Klaus::Trajectory::getOrientation() const
{
  return mOrientation;
}

/**
  This method sets the orientation of the trajectory
  @param q The orientation of the trajectory
*/
void Klaus::Trajectory::setOrientation(const Quaternion & q)
{
  mOrientation = q;
  postUpdateInternal();
}

/**
  This method returns the starting position of the projectile
  @return Starting position of the projectile
*/
Klaus::VecPosition Klaus::Trajectory::getStartingPos() const
{
  return mStartPos;
}

/**
  This method sets the starting position of the projectile
  @param pos The starting position of the projectile
*/
void Klaus::Trajectory::setStartingPos(const Klaus::VecPosition& pos)
{
  mStartPos = pos;
  postUpdateInternal();
}




/******************************************************************/
/*******************  Class PeriodicTrajectory  *******************/
/******************************************************************/

/**
  Constructor of PeriodicTrajectory. Initializes period and delta phase
  @param period Time length of a full period movment
  @param delta_phi Delay in starting the phase
  @param min_range Minimum range of the phase
  @param max_range Maximum range of the phase
*/
Klaus::PeriodicTrajectory::PeriodicTrajectory(const double& period, const double& delta_phi,
                                       const double & min_range, const double & max_range)
{
  mPeriod = period;
  mPhaseDelay = delta_phi;
  mMinPhase = min_range;
  mMaxPhase = max_range;
}

/**
  This method calculates the current phase of the period using the given time
  @param time Current time of the projectile
  @return Current phase of the movement
*/
Klaus::AngDeg Klaus::PeriodicTrajectory::getPhase(double time)
{
  // deduce phase delay
  double gap = mMaxPhase - mMinPhase;
  time -= mPhaseDelay / gap * mPeriod;

  // find out time in range [0..mPeriod)
  while(time >= mPeriod)
    time -= mPeriod;

  // calculate phase from the time at hand
  double result = mMinPhase + time / mPeriod * gap;
  if(result > mMaxPhase)
    return result - gap;
  else if(result < mMinPhase)
    return result + gap;
  else
    return result;
}

/**
  This method calculates number of cycles which have passed when in the given time
  @param time Current time of projectile
  @return Number of cycles pass till the given time
*/
int Klaus::PeriodicTrajectory::getNumberOfCycles(double time)
{
  int iCycles = 0;

  // deduce phase delay
  double gap = mMaxPhase - mMinPhase;
  time -= mPhaseDelay / gap * mPeriod;

  while(time >= mPeriod)
  {
    time -= mPeriod;
    ++iCycles;
  }

  while(time < 0)
  {
    time += mPeriod;
    --iCycles;
  }

  return iCycles;
}

/**
  This method returns the period of the trajectory
  @return Period of the trajectory
*/
double Klaus::PeriodicTrajectory::getPeriod() const
{
  return mPeriod;
}

/**
  This method sets the period of the trajectory
  @param period Period of the trajectory
  @param delta_phi Delay in starting the trajectory phase
*/
void Klaus::PeriodicTrajectory::setPeriod(const double& period, const double & delta_phi)
{
  mPeriod = period;
  mPhaseDelay = delta_phi;

  postUpdateInternal();
}

/**
  This method returns the phase delay of the trajectory
  @return Trajectory's phase delay
*/
double Klaus::PeriodicTrajectory::getPhaseDelay() const
{
  return mPhaseDelay;
}

/**
  This method sets the phase delay in trajectory's movement
  @param phase_delay The delay in trajectory's phase calculation
*/
void Klaus::PeriodicTrajectory::setPhaseDelay(const double& phase_delay)
{
  mPhaseDelay = phase_delay;
  postUpdateInternal();
}

/**
  This method returns the minimum phase that the projectile can have
  @return Minimum phase of movement
*/
double Klaus::PeriodicTrajectory::getMinimumPhase() const
{
  return mMinPhase;
}

/**
  This method returns the maximum phase that the projectile can have
  @return Maximum phase of movement
*/
double Klaus::PeriodicTrajectory::getMaximumPhase() const
{
  return mMaxPhase;
}

/**
  This method sets the trajectory phase range
  @param min_range Minimum phase
  @param max_range Maximum phase
*/
void Klaus::PeriodicTrajectory::setPhaseRange(const double& min_range, const double& max_range)
{
  mMinPhase = min_range;
  mMaxPhase = max_range;

  postUpdateInternal();
}




/***************************************************************************/
/***********************  Class EllipsoidTrajectory  ***********************/
/***************************************************************************/

/**
  Constructor of EllipsoidTrajectory. Creates the trajectory and the route
  on which the object moves.
  @param period The period of the movement
  @param range The range of the trajectory in it's period
  @param width The trajectory ellipse width
*/
Klaus::EllipsoidTrajectory::EllipsoidTrajectory(const double& period, const double& range, const double& width)
{
  mPeriod = period;
  mRange = range;
  mWidth = width;
  mMinPhase = 0;
  mMaxPhase = 180.0;
  postUpdateInternal();
}

/**
  This method returns the width of the ellipse
*/
double Klaus::EllipsoidTrajectory::getWidth() const
{
  return mWidth;
}

/**
  This method sets the width of the trajectory ellipse
  @param width New width of the trajectory
*/
void Klaus::EllipsoidTrajectory::setWidth(const double& width)
{
  mWidth = width;
  postUpdateInternal();
}

/**
  This is called after any of the parameters
  of the trajectory is updated.
*/
void Klaus::EllipsoidTrajectory::postUpdateInternal()
{
  mRoute.setEllipse(mRange, mWidth, VecPosition(mRange * 0.5, 0, 0), Quaternion::identity);
}

/**
  Overloaded method to get the position of the object on the trajectory
  @param time Time of the movement
  @return Position of the object on the trajectory
*/
Klaus::VecPosition Klaus::EllipsoidTrajectory::getPosition(double time)
{
  // find out the phase
  double dPhase = getPhase(time);
  int iCycles = getNumberOfCycles(time);

  // begin with the moved distance
  VecPosition result(iCycles * mRange, 0, 0);

  // take in account the position on ellipse
  result += mRoute.getPointWithAngle(180.0 - dPhase);

  // now apply the orientation
  result = mOrientation * result;

  // and add the starting position
  return result + mStartPos;
}
