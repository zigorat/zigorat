/*
*********************************************************************************
*          ActHandler.h : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration for Acthandler which handles  *
*            command sending to simulation server                               *
*                                                                               *
*********************************************************************************
*/

/*! \file ActHandler.h
<pre>
<b>File:</b>          ActHandler.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration for Acthandler
               which handles command sending to server.
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
</pre>
*/

#ifndef ACTHANDLER
#define ACTHANDLER

#include "SoccerTypes.h" // needed for SoccerCommand
#include "SimSpark.h"    // needed for SimSpark connection manager

namespace Klaus
{

  /*! The ActHandler Class is used in the RoboCup Soccer environment to send the
      commands to the soccerserver. The ActHandler contains a queue in which the
      commands are put. When thinkin' finishes the commands that are currently in
      the queue are converted to text strings and send to the server. It is
      possible to send more than one command to the server at each time step,
      Therefore internally different queues are stored. Each time a command is
      put into the queue that is already there, the command is updated with the
      new information. Furthermore it is also possible to directly send commands
      (or text strings) to the server. These methods can be used when an
      initialization or beam command has to be sent to the server and you're sure
      this information is final, i.e. the message will not become better when new
      information arrives from the server. */
  class ActHandler : public Singleton<ActHandler>
  {
    private:
      SimSpark            * mConnection;      /*!< Connection with server */
      SoccerCommand::Array  mJointCommands;   /*!< Commands for the joints*/
      SoccerCommand         mSayCommand;      /*!< Say command            */
      SoccerCommand         mBeamCommand;     /*!< Beam command           */
      SoccerCommand         mInitCommand;     /*!< Init command           */

                            ActHandler        (                           );

    public:

      static ActHandler   * getSingleton      (                           );
      static void           freeSingleton     (                           );

      // methods related to putting and sending messages using the queue
      bool                  addCommand        ( const SoccerCommand &cmd  );
      void                  emptyQueue        (                           );
      bool                  sendCommands      (                           );

      // methods to send commands directly to the server
      bool                  sendCommand       ( const SoccerCommand & soc ) const;
      bool                  sendMessage       ( const std::string & strMsg ) const;
  };


}; // end namespace Klaus


#endif // ACTHANDLER
