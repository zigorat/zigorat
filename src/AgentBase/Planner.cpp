/*
*********************************************************************************
*             Planner.cpp : Robocup 3D Soccer Simulation Team Zigorat           *
*                                                                               *
*  Date: 03/16/2008                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: Class definitions for Planner, which is used to                    *
*            run simultanous agent plans                                        *
*                                                                               *
*********************************************************************************
*/

/*! \file Planner.cpp
<pre>
<b>File:</b>          Planner.cpp
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/16/2008
<b>Last Revision:</b> $ID$
<b>Contents:</b>      Class definition for JointPlanner, which is used to run simultanous
             agent plans
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/16/2008        Mahdi           Initial version created
</pre>
*/
#include "Planner.h"

#include <fstream>
#include <sstream>
#include <math.h>

#include <libZigorat/Parse.h>

using namespace std;

/***********************************************************************************************/
/**************************************  Class JointPlan  **************************************/
/***********************************************************************************************/

/**
 * This is the constructor of JointPlan
 * Initializes JointPlan, and sets joint_id to an illegal value.
 */
JointPlan::JointPlan()
{
  m_Joint       = NULL;
  m_FinalAngle1 = UnknownDoubleValue;
  m_FinalAngle2 = UnknownDoubleValue;
  m_StartTime   = UnknownTime;
  m_Duration    = UnknownDoubleValue;
  m_StartTime   = 0;
}

/**
 * This is the destructor of JointPlan
 * Frees up everything
 */
JointPlan::~JointPlan()
{
}

/**
 * This method returns the joint name which this JointPlan is changing
 * @return Joint name of current JointPlan
 */
const RSG::SceneJoint* JointPlan::getJoint() const
{
  return m_Joint;
}

/**
 * This method sets the joint to change via JointPlan
 * @param strPerceptor Name of the joint's perceptor
 */
void JointPlan::setJoint( const string & strPerceptor )
{
  m_Joint = AgentObject::getSingleton()->getJointInstance(strPerceptor);
}

/**
 * This method returns the final angle which should joint reach
 * @return Final angle of the joint to change
 */
AngDeg JointPlan::getFinalAngle1() const
{
  return m_FinalAngle1;
}

/**
 * Thsis method sets the final angle to apply to the joint
 * @param ang Angle to reach for the current joint
 */
void JointPlan::setFinalAngle1(AngDeg ang)
{
  m_FinalAngle1 = ang;
}

/**
 * This method returns the final angle which should joint reach
 * @return Final angle of the joint to change
 */
AngDeg JointPlan::getFinalAngle2() const
{
  return m_FinalAngle2;
}

/**
 * Thsis method sets the final angle to apply to the joint
 * @param ang Angle to reach for the current joint
 */
void JointPlan::setFinalAngle2(AngDeg ang)
{
  m_FinalAngle2 = ang;
}

/**
 * This method returns the start time for current JointPlan relative
 * to start of the whole JointPlan
 * @return start time relative to the current JointPlan
 */
double JointPlan::getStartTime() const
{
  return m_StartTime;
}

/**
 * This method sets the start time for current JointPlan, relative to
 * the start of whole JointPlan
 * @param st start time of this JointPlan relative to the whole JointPlan
 */
void JointPlan::setStartTime(double st)
{
  m_StartTime = st;
}

/**
 * This method returns the duration needed for current JointPlan to
 * apply angles to the joint
 * @return Duration of action
 */
double JointPlan::getDuration() const
{
  return m_Duration;
}

/**
 * This method sets the duration needed for current JointPlan to
 * apply angles to the joint
 * @param dur Duration of action
 */
void JointPlan::setDuration(double dur)
{
  m_Duration = dur;
}

/**
 * This method checks whether this planpart can be executed just now
 * @param timeNow Current time of general plan execution
 * @return bool indicates whether this planpart can be executed
 */
bool JointPlan::shouldBeExecuted(Time timeNow) const
{
  if( timeNow >= m_StartTime && (timeNow - m_StartTime) < m_Duration)
    return true;
  else
    return false;
}

/**
 * This method creates a command from current JointPlan
 * to be able to be sent to simulator via ActHandler
 * @param timeNow Current JointPlanning Time
 * @return SoccerCommand containing action command to be sent to simulator
 */
SoccerCommand JointPlan::makeCommand( Time timeNow ) const
{
  if( !m_Joint )
  {
    cerr << "(JointPlan::makeCommand) No joint instance exists" << endl;
    return SoccerCommand( CMD_ILLEGAL );
  }

  double delta_time = m_Duration - (timeNow - m_StartTime);
  if( delta_time < WorldModel::getSingleton()->getSimulatorStep() )
  {
    if(m_Joint->getJointType() == RSG::SceneJoint::JOINT_UNIVERSAL)
      return SoccerCommand(CMD_JOINT, m_Joint->getPerceptorName(), m_Joint->getCurrentRate(0), m_Joint->getCurrentRate(1) );
    else
      return SoccerCommand(CMD_JOINT, m_Joint->getPerceptorName(), m_Joint->getCurrentRate(0) );
  }

  double angle1 = Deg2Rad(m_FinalAngle1 - m_Joint->getCurrentAngle());
  double speed1 = angle1 / delta_time;
  if( m_Joint->getJointType() != RSG::SceneJoint::JOINT_UNIVERSAL )
    return SoccerCommand( CMD_JOINT, m_Joint->getPerceptorName(), speed1, 0 );

  double angle2 = Rad2Deg(m_FinalAngle2 - m_Joint->getCurrentAngle(1));
  double speed2 = angle2 / delta_time;

  return SoccerCommand( CMD_JOINT, m_Joint->getPerceptorName(), speed1, speed2 );
}

/*! Overloaded version of the C++ output operator for
    JointPlan. This operator makes it print JointPlan
    to a standard output stream. JointPlan details are printed.

    \param os output stream to which information should be written
    \param p JointPlan which must be printed
    \return output stream containing details */
ostream& operator << ( ostream &os, JointPlan p )
{
  os << "(JointPlan (Joint " << p.m_Joint->getPerceptorName() <<
        ") (DesiredAngle #1 " << p.m_FinalAngle1;
  if(p.m_Joint->getJointType() == RSG::SceneJoint::JOINT_UNIVERSAL)
    os << ") (DesiredAngle #2 " << p.m_FinalAngle2;

  os << ") (Start " << p.m_StartTime << ") (Duration " << p.m_Duration << ") )";

  return os;
}

/*! Overloaded version of the C++ output operator for
    JointPlan. This operator makes it print JointPlan
    to a standard logging stream. JointPlan details are printed.
    \param iLogLevel Log level to put log information to */
void JointPlan::log( int iLogLevel )
{
  stringstream ss;
  ss << *this;
  Logger::getSingleton()->log( iLogLevel, "%s", ss.str().c_str() );
}

/*! Overloaded version of the C++ input operator for JointPlan.
    This operator makes JointPlan to be able to be read from a
    standard input stream.

    \param is input stream from which information should be read
    \param p a JointPlan which must be inputed
    \return input stream */
istream& operator >> ( istream &is, JointPlan & p )
{
  /// Check for # predicate
  p.m_Joint = NULL;
  char str[255];
  is.getline( str, 255 );
  StringList tokens;

  if(!SLangParser::parseByCharDelim(str, ' ', tokens))
    return is;

  StringList::const_iterator it = tokens.begin();
  if( (*it)[0] == '#' )
    return is;

  /// Parse Joint name
  string name = *it;

  /// Parse first Final Angle
  if( ++it == tokens.end() )
    return is;
  p.m_FinalAngle1 = SLangParser::parseFirstDouble(*it);

  RSG::SceneJoint *j = AgentObject::getSingleton()->getJointInstance(name);
  /// Parse second final angle if supplied
  if( j->getJointType() == RSG::SceneJoint::JOINT_UNIVERSAL )
  {
    if( ++it == tokens.end() )
      return is;

    p.m_FinalAngle2 = SLangParser::parseFirstDouble(*it);
  }

  /// Parse start time
  if( ++it == tokens.end() )
    return is;
  p.m_StartTime = SLangParser::parseFirstDouble(*it);

  /// Parse duration
  if( ++it == tokens.end() )
    return is;
  p.m_Duration = SLangParser::parseFirstDouble(*it);

  p.m_Joint = j;

  return is;
}



/***********************************************************************************************/
/***************************************  CLASS PLAN  ******************************************/
/***********************************************************************************************/


/**
 * This is the constrcutor of MovementPlan. MovementPlan is a set of commands to be
   executed each on their own time.
 */
MovementPlan::MovementPlan()
{
  ACT = ActHandler::getSingleton();

  m_StartTime = -1;
}

/**
 * This is the destructor of plan, Clears up memory.
 */
MovementPlan::~MovementPlan()
{
  m_JointPlans.clear();
}

/**
 * This method checks whether there is any JointPlan ready to be executed
 * @param currentTime Current execution time (not current simulator time)
 * @return True if there is a JointPlan awaiting execution, False otherwise
 */
bool MovementPlan::isExecutable( Time currentTime ) const
{
  if( m_StartTime == -1 )
    return false;

  if( currentTime == UnknownTime )
    currentTime = WorldModel::getSingleton()->getTime() - m_StartTime;

  bool bEverythingExecuted = true;
  for( unsigned i = 0; i < m_JointPlans.size(); i++ )
    if( m_JointPlans[i].getStartTime() + m_JointPlans[i].getDuration() > currentTime )
      bEverythingExecuted = false;

  return !bEverythingExecuted;
}

/**
 * This method loads plans from a file into a plan collection.
 * File format is as follows:
 * For joints: JointID Final_Angle1 [FinalAngle2] StartTime (Relative to plan start time) Duration
 * @param filename Filename to load plan data from
 */
void MovementPlan::loadFromFile( string filename )
{
  ifstream file( filename.c_str(), ios::in );
  if( !file )
  {
    cerr << "MovementPlanner: file not opened: " << filename << endl;
    cerr.flush();
    return;
  }

  JointPlan plan;
  while( !file.eof() )
  {
    file >> plan;
    if(plan.getJoint())
      addJointPlan( plan );
  }

  file.close();
}

/**
 * This method adds a plan to the plan colloction
 * @param plan The joint plan to add to the joint lists
 */
void MovementPlan::addJointPlan( const JointPlan & plan )
{
  m_JointPlans.push_back( plan );
}

/**
 * This method starts plan exection. The plan start time is set and all
 * plan collections start from this point on.
 */
void MovementPlan::startExecution()
{
  m_StartTime = WorldModel::getSingleton()->getTime();
}

/**
 * This method executes plan part which is usable for agents currents state.
 * @return Indicates whether this plan has finished its job or not.
 */
bool MovementPlan::execute()
{
  if( m_StartTime == -1 )
    startExecution();

  Time time = WorldModel::getSingleton()->getTime() - m_StartTime;
  for( unsigned i = 0; i < m_JointPlans.size(); i++ )
    if(m_JointPlans[i].shouldBeExecuted(time))
      ACT->putCommandInQueue(m_JointPlans[i].makeCommand(time));

  return !isExecutable( time + WorldModel::getSingleton()->getSimulatorStep() );
}

/**
 * Shows whether any planpart is being executed or not.
 * @return bool indicates if plan is being executed
 */
bool MovementPlan::executing() const
{
  return m_StartTime != -1;
}

/**
 * This method restart plan execution, in case of any reason interrupting plan execution
 */
void MovementPlan::restart()
{
  m_StartTime = -1;
}

/**
 * This method logs plan informaion into an output stream
 * @param os Output stream to print plan information
 */
void MovementPlan::show( ostream &os )
{
  os << "MovementPlans dumping start:\n";

  for( unsigned i = 0; i < m_JointPlans.size(); i++ )
    os << "  " << m_JointPlans[i] << endl;

  os << "MovementPlans dumping ended\n";
}



/*****************************************************************************/
/********************** TESTING PURPOSES *************************************/
/*****************************************************************************/


// #define TEST_PLANNING  // Uncomment this to test planner module
#ifdef TEST_PLANNING

int main()
{
  JointPlan planner(0,0);
  planner.loadFromFile("./Conf/Stance.zpd");
  planner.show(cout);
}

#endif // TEST_PLANNING
