/*
*********************************************************************************
*            SimSpark.h : Robocup 3D Soccer Simulation Team Zigorat             *
*                                                                               *
*  Date: 03/21/2011                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: SimSpark connection manager.                                       *
*                                                                               *
*********************************************************************************
*/

/*! \file SimSpark.h
<pre>
<b>File:</b>          SimSpark.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/21/2011
<b>Last Revision:</b> $ID$
<b>Contents:</b>      SimSpark connection manager
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/21/2011       Mahdi           Initial version created
</pre>
*/

#ifndef SIMSPARK_CONNECTION
#define SIMSPARK_CONNECTION

#include <libZigorat/Connection.h> // needed for Connection
#include <libZigorat/Singleton.h>  // needed for Singleton
#include <libZigorat/Logger.h>     // needed for Logger

namespace Klaus
{

  /*! SimSparkConnection is a class that encapsulates spark simulation
      server connection management; Thus recieving and sending data to
      the simulation server.
  */
  class _DLLExportControl SimSpark : public Connection, public Singleton<SimSpark>
  {
    private:
      Logger          * mLog;        /*!< Logger instance        */

      bool              getSizedData ( char *buf, int size       ) const;

                        SimSpark     (                           );
                      ~ SimSpark     (                           );

    public:
      static SimSpark * getSingleton (                           );
      static void       freeSingleton(                           );

      bool              connect      ( const char * strHost = "",
                                       int          iPort = -1   );
      bool              sendCommand  ( const std::string & msg   ) const;
      bool              getMessage   ( std::string & msg         ) const;
  };

}; // end namespace Klaus

#endif // SIMSPARK_CONNECTION
