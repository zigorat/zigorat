/*
*********************************************************************************
*           BasicAgent.h : Robocup 3D Soccer Simulation Team Zigorat            *
*                                                                               *
*  Date: 03/20/2007                                                             *
*  Author: Mahdi Hamdarsi                                                       *
*  Comments: This file contains class decleration BasicAgent which contains     *
*            several routines needed for thinkin' module                        *
*                                                                               *
*********************************************************************************
*/

/*! \file BasicAgent.h
<pre>
<b>File:</b>          BasicAgent.h
<b>Project:</b>       Robocup Soccer Simulation Team: Zigorat
<b>Authors:</b>       Mahdi Hamdarsi
<b>Created:</b>       03/20/2007
<b>Last Revision:</b> $ID$
<b>Contents:</b>      This file contains class decleration BasicAgent which contains
               several routines needed for thinkin' module
<hr size=2>
<h2><b>Changes</b></h2>
<b>Date</b>             <b>Author</b>          <b>Comment</b>
03/20/2007       Mahdi           Initial version created
</pre>
*/


#ifndef BASICAGENT_H
#define BASICAGENT_H

#include "WorldModel.h"               // needed to get information about game
#include "ActHandler.h"               // needed to send actions to simulator
#include "AgentSettings.h"            // needed to read various agent properties
#include "Formations.h"               // needed for FormationSystem
#include "Skills.h"                   // needed for all skills
#include <libZigorat/Connection.h>    // needed to connect to simulator

namespace Klaus
{

  /*! This class defines the skills that can be used by an agent. No
      functionality is available that chooses when to execute which skill, this
      is done in the Agent class. The WorldModel is used to determine the way
      in which the skills are performed.
  */
  class BasicAgent
  {
    protected:
      bool                mTerminate; /*!< If this variable is true, agent will quit */

      SimSpark          * mConnection;           /*!< Connection instance */
      ActHandler        * mQueue;                /*!< ActHandler instance */
      WorldModel        * mWM;                   /*!< WorldModel instance */
      AgentSettings     * mSettings;             /*!< AgentSettings instance */
      Logger            * mLog;                  /*!< Logger instance */
      FormationSystem   * mFormation;            /*!< FormationSystem instance */
      int                 mRequestedNumber;      /*!< Number to request from server */
      Walk                mWalk;                 /*!< Walk skill */

      // Initialization
      void               sendPlayerInformation   (                           );

      // Finalization
      void               sayGoodbye              (                           );

      // Basic Skills
      void               beamToStrategicPosition ( AngDeg angBody = 0.00     );
      void               beam                    ( const double & dx,
                                                   const double & dy,
                                                   const double & dAng       );
      void               say                     ( const std::string & str   );

      // Joint Utility methods
      void               adjustJoint             ( const std::string & strName,
                                                   const double & dSpeed1    );
      void               adjustJoint             ( const std::string & strName,
                                                   const double & dSpeed1,
                                                   const double & dSpeed2    );
      void               head_up                 ( AngDeg       ang          );
      void               head_side               ( AngDeg       ang          );
      void               arm_rotate_l            ( AngDeg       ang          );
      void               arm_rotate_r            ( AngDeg       ang          );
      void               arm_l                   ( AngDeg       ang          );
      void               arm_r                   ( AngDeg       ang          );
      void               arm_side_l              ( AngDeg       ang          );
      void               arm_side_r              ( AngDeg       ang          );
      void               elbow_l                 ( AngDeg       ang          );
      void               elbow_r                 ( AngDeg       ang          );
      void               hip_l                   ( AngDeg       ang          );
      void               hip_r                   ( AngDeg       ang          );
      void               hip_side_l              ( AngDeg       ang          );
      void               hip_side_r              ( AngDeg       ang          );
      void               hip_rotate_l            ( AngDeg       ang          );
      void               hip_rotate_r            ( AngDeg       ang          );
      void               knee_l                  ( AngDeg       ang          );
      void               knee_r                  ( AngDeg       ang          );
      void               ankle_l                 ( AngDeg       ang          );
      void               ankle_r                 ( AngDeg       ang          );
      void               ankle_side_l            ( AngDeg       ang          );
      void               ankle_side_r            ( AngDeg       ang          );

      // Head related methods
      void               turnNeckToPoint         ( const VecPosition & pos   );
      void               turnNeckToObject        ( const ObjectType & obj    );
      void               searchBall              (                           );

      void               walk                    (                           );

    public:
                         BasicAgent              (                           );
                       ~ BasicAgent              (                           );

      virtual bool       initialize              ( const std::string & name,
                                                   const int & iNum = 0      );
  };

}; // end namespace Klaus


#endif // BASICAGENT_H
